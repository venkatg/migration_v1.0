


--Mysql_dump_commands for all schemas:
--mysqldump -uvenkat -p --routines optionfair_trading > optionfair_trading_data.sql
mysqldump -uvenkat -p --single-transaction   -d --routines backoffice > backoffice_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines binarytrade > binarytrade_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines bonus > bonus_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines casino > casino_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines csm > csm_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines fx > fx_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines loyalty > loyalty_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines mail > mail_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines master > master_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines messaging > messaging_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines mktg > mktg_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines mon > mon_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines oauth > oauth_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines playfund > playfund_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines playpoker > playpoker_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines poker > poker_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines questsoftware > questsoftware_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines recon > recon_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines risk_engage > risk_engage_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines scheduler > scheduler_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines segment > segment_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines trny > trny_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines sportsbook > sportsbook_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines retail > retail_nodata.sql

mysqldump -uvenkat -p --single-transaction   -d --routines bireports > bi_nodata.sql

mysqldump -uvenkat -p --single-transaction   -d --routines cashier > cashier_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines fund > fund_nodata.sql

mysqldump -uvenkat -p --single-transaction   -d --routines vendor > vendor_nodata.sql
mysqldump -uvenkat -p --single-transaction   -d --routines ecr > ecr_nodata.sql

--Mysql dump master data insertion:
mysqldump -uvenkat -p --single-transaction   --no-create-info backoffice tbl_reports tbl_reports_category tbl_reports_module tbl_role_permission tbl_roles tbl_user tbl_user_labels tbl_user_partner tbl_user_role > backoffice_all_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info binarytrade tbl_game_category_mst tbl_game_type_mst > binarytrade_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info bonus tbl_mst_bonus_category tbl_mst_bonus_criteria_type tbl_mst_bonus_pct_ref_type tbl_mst_bonus_pocket tbl_mst_bonus_release_criteria_type tbl_mst_bonus_status tbl_mst_issue_type tbl_mst_player_target_type_list tbl_mst_release_type tbl_mst_user_status tbl_vendor_restricted_bonus > bonus_master.sql


mysqldump -uvenkat -p --single-transaction   --no-create-info casino tbl_casino_game_category_mst tbl_casino_game_template tbl_casino_game_type_mst tbl_casino_partner_template tbl_casino_recommended_games > casino_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info csm tbl_alert_priority tbl_alert_status tbl_alert_types tbl_call_back_comm tbl_comm_channels tbl_event tbl_problem_category tbl_problem_sub_category tbl_queues tbl_rule tbl_rule_event_assoc tbl_rule_queue_assoc tbl_agent_queues tbl_active_allerts_for_partner > csm_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info fx tbl_fx_markup tbl_fx_partner_markup tbl_fx_rates > fx_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info loyalty tbl_tier_periodicity tbl_metric_config tbl_metric_measure tbl_reward_points_expiry_config tbl_reward_points_expiry_config_log tbl_rewards_config tbl_rewards_config_log tbl_tier_config tbl_tier_config_log tbl_tier_levels tbl_tier_levels_log  > loyalty_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info master tbl_active_status_mst tbl_channel_mst_config tbl_crncy_partner_mst_config tbl_gates_action_mst tbl_gates_mst tbl_interceptors_mst tbl_label_address_config tbl_label_mst_config tbl_language tbl_liq_pool_mst_config tbl_mst_channel tbl_mst_crncy tbl_mst_phone_country_code tbl_mst_product_list tbl_mst_sub_channel tbl_mst_vendor_list tbl_partner_currencies tbl_partner_language tbl_partner_mst_config tbl_product_mst_config tbl_sub_channel_mst_config tbl_vendor_mst_config tbl_tracker_mst_config tbl_affiliate_mst_config > master_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info messaging QRTZ_BLOB_TRIGGERS QRTZ_CALENDARS QRTZ_CRON_TRIGGERS QRTZ_FIRED_TRIGGERS QRTZ_JOB_DETAILS QRTZ_JOB_LISTENERS QRTZ_LOCKS QRTZ_PAUSED_TRIGGER_GRPS QRTZ_SCHEDULER_STATE QRTZ_SIMPLE_TRIGGERS QRTZ_TRIGGERS QRTZ_TRIGGER_LISTENERS tbl_message_languages tbl_module_priority > messaging_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info mktg tbl_prm_group_criteria_types_master tbl_prm_group_measure_types_master tbl_prm_types_master > mktg_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info oauth tbl_clients  > oauth_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info playfund tbl_free_chips_events_mst tbl_freechips_fund_txn_type_mst tbl_fund_active_status_mst tbl_fund_type_mst > playfund_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info scheduler tbl_recon_mst_error_code > scheduler_master.sql


mysqldump -uvenkat -p --single-transaction   --no-create-info sportsbook tbl_game_category_mst tbl_game_category_mst_log tbl_game_type_mst tbl_game_type_mst_log > sportsbook_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info retail tbl_retail_scratch_card_txn tbl_retail_shop_account_config tbl_retail_shop_account_details tbl_retail_shop_operating_types tbl_retail_shop_promoters tbl_retail_voucher_card_txn tbl_retail_voucher_config > retail_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info vendor tbl_partner_api_access tbl_vendor_auth_token tbl_vendor_games_mapping_mst tbl_vendor_games_partner_mapping tbl_vendor_mst_config tbl_vendor_mst_service_config tbl_custom_search_criterias_mst> vendor_master.sql


mysqldump -uvenkat -p --single-transaction   --no-create-info ecr tbl_category_config_mapping tbl_category_limits tbl_category_master tbl_category_rule tbl_ecr_cities tbl_ecr_countries tbl_ecr_mst_auth_type tbl_ecr_mst_conversion tbl_ecr_mst_creation_status tbl_ecr_mst_document_types tbl_ecr_mst_email_vrfn tbl_ecr_mst_status tbl_ecr_mst_vrfn_lifecycle tbl_ecr_states tbl_life_line tbl_ecr_partner_hashmap_algorithm tbl_affliate_partner_mapping> ecr_master.sql

mysqldump -uvenkat -p --single-transaction   --no-create-info fund tbl_free_chips_events_mst tbl_freechips_fund_txn_type_mst tbl_fund_active_status_mst tbl_fund_mst_conversion tbl_fund_type_mst tbl_real_fund_txn_type_mst tbl_custom_search_criterias_mst> fund_master.sql


mysqldump -uvenkat -p --single-transaction   --no-create-info cashier tbl_cashier_conversion_deposit_limits tbl_cashier_deposit_limits tbl_cashier_txn_kind tbl_cashier_txn_status tbl_instrument_option tbl_instrument_option_country_mapping tbl_processor_api_config tbl_processor_config tbl_wire_country_config tbl_wire_type_fields_mapping tbl_cashier_instrument_vrfn_source tbl_processors> cashier_master.sql


--Create batabases scripts:

mysql -uvenkat -p < backoffice.sql
mysql -uvenkat -p < binarytrade.sql
mysql -uvenkat -p < bonus.sql
mysql -uvenkat -p < cashier.sql
mysql -uvenkat -p < casino.sql
mysql -uvenkat -p < csm.sql
mysql -uvenkat -p < ecr.sql
mysql -uvenkat -p < fund.sql
mysql -uvenkat -p < fx.sql
mysql -uvenkat -p < loyalty.sql
mysql -uvenkat -p < mail.sql
mysql -uvenkat -p < master.sql
mysql -uvenkat -p < messaging.sql
mysql -uvenkat -p < mktg.sql
mysql -uvenkat -p < mon.sql
mysql -uvenkat -p < oauth.sql
mysql -uvenkat -p < playfund.sql
mysql -uvenkat -p < playpoker.sql
mysql -uvenkat -p < poker.sql
mysql -uvenkat -p < questsoftware.sql
mysql -uvenkat -p < recon.sql
mysql -uvenkat -p < risk_engage.sql
mysql -uvenkat -p < scheduler.sql
mysql -uvenkat -p < segment.sql
mysql -uvenkat -p < trny.sql
mysql -uvenkat -p < vendor.sql
mysql -uvenkat -p < sportsbook.sql
mysql -uvenkat -p < retail.sql

--Mysql_dump_import to new database - commands for all schemas:

mysql -uvenkat -p backoffice < backoffice_nodata.sql
mysql -uvenkat -p binarytrade < binarytrade_nodata.sql
mysql -uvenkat -p bonus < bonus_nodata.sql
mysql -uvenkat -p cashier < cashier_nodata.sql
mysql -uvenkat -p casino < casino_nodata.sql
mysql -uvenkat -p csm < csm_nodata.sql
mysql -uvenkat -p ecr < ecr_nodata.sql
mysql -uvenkat -p fund < fund_nodata.sql
mysql -uvenkat -p fx < fx_nodata.sql
mysql -uvenkat -p loyalty < loyalty_nodata.sql
mysql -uvenkat -p mail < mail_nodata.sql
mysql -uvenkat -p master < master_nodata.sql
mysql -uvenkat -p messaging < messaging_nodata.sql
mysql -uvenkat -p mktg < mktg_nodata.sql
mysql -uvenkat -p mon < mon_nodata.sql
mysql -uvenkat -p oauth < oauth_nodata.sql
mysql -uvenkat -p playfund < playfund_nodata.sql
mysql -uvenkat -p playpoker < playpoker_nodata.sql
mysql -uvenkat -p poker < poker_nodata.sql
mysql -uvenkat -p questsoftware < questsoftware_nodata.sql
mysql -uvenkat -p recon < recon_nodata.sql
mysql -uvenkat -p risk_engage < risk_engage_nodata.sql
mysql -uvenkat -p scheduler < scheduler_nodata.sql
mysql -uvenkat -p segment < segment_nodata.sql
mysql -uvenkat -p trny < trny_nodata.sql
mysql -uvenkat -p vendor < vendor_nodata.sql
mysql -uvenkat -p sportsbook < sportsbook_nodata.sql
mysql -uvenkat -p retail < retail_nodata.sql

--Mysql_dump_insert master data to new database - commands for all schemas:

mysql -uvenkat -p backoffice < backoffice_all_master.sql
mysql -uvenkat -p binarytrade < binarytrade_master.sql
mysql -uvenkat -p bonus < bonus_master.sql
mysql -uvenkat -p cashier < cashier_master.sql
mysql -uvenkat -p casino < casino_master.sql
mysql -uvenkat -p csm < csm_master.sql
mysql -uvenkat -p ecr < ecr_master.sql
mysql -uvenkat -p fund < fund_master.sql
mysql -uvenkat -p fx < fx_master.sql
mysql -uvenkat -p loyalty < loyalty_master.sql
mysql -uvenkat -p master < master_master.sql
mysql -uvenkat -p messaging < messaging_master.sql
mysql -uvenkat -p mktg < mktg_master.sql
mysql -uvenkat -p oauth < oauth_master.sql
mysql -uvenkat -p playfund < playfund_master.sql
mysql -uvenkat -p scheduler < scheduler_master.sql
mysql -uvenkat -p vendor < vendor_master.sql
mysql -uvenkat -p sportsbook <  sportsbook_master.sql
mysql -uvenkat -p retail <  retail_master.sql

