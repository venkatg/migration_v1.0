/****************************************************** clean up database & create scheduler schema START ******************************************************/
DROP SCHEMA IF EXISTS scheduler;
CREATE SCHEMA scheduler;
GRANT USAGE ON *.* TO 'scheduler'@'%';
DROP USER 'scheduler'@'%';
CREATE USER 'scheduler'@'%' IDENTIFIED BY 'scheduler';
GRANT ALL ON scheduler.* TO 'scheduler'@'%';
GRANT USAGE ON *.* TO 'scheduler'@'localhost';
DROP USER 'scheduler'@'localhost';
CREATE USER 'scheduler'@'localhost' IDENTIFIED BY 'scheduler';
GRANT ALL ON scheduler.* TO 'scheduler'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'scheduler'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE scheduler CHARACTER SET utf8 COLLATE utf8_bin;

USE  scheduler;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
