/****************************************************** clean up database & create casino schema START ******************************************************/
DROP SCHEMA IF EXISTS casino;
CREATE SCHEMA casino;
GRANT USAGE ON *.* TO 'casino'@'%';
DROP USER 'casino'@'%';
CREATE USER 'casino'@'%' IDENTIFIED BY 'casino';
GRANT ALL ON casino.* TO 'casino'@'%';
GRANT USAGE ON *.* TO 'casino'@'localhost';
DROP USER 'casino'@'localhost';
CREATE USER 'casino'@'localhost' IDENTIFIED BY 'casino';
GRANT ALL ON casino.* TO 'casino'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'casino'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE casino CHARACTER SET utf8 COLLATE utf8_bin;

USE  casino;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
