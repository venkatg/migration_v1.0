/****************************************************** clean up database & create mail schema START ******************************************************/
DROP SCHEMA IF EXISTS mail;
CREATE SCHEMA mail;
GRANT USAGE ON *.* TO 'mail'@'%';
DROP USER 'mail'@'%';
CREATE USER 'mail'@'%' IDENTIFIED BY 'mail';
GRANT ALL ON mail.* TO 'mail'@'%';
GRANT USAGE ON *.* TO 'mail'@'localhost';
DROP USER 'mail'@'localhost';
CREATE USER 'mail'@'localhost' IDENTIFIED BY 'mail';
GRANT ALL ON mail.* TO 'mail'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'mail'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE mail CHARACTER SET utf8 COLLATE utf8_bin;

USE  mail;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
