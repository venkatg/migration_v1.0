/****************************************************** clean up database & create loyalty schema START ******************************************************/
DROP SCHEMA IF EXISTS loyalty;
CREATE SCHEMA loyalty;
GRANT USAGE ON *.* TO 'loyalty'@'%';
DROP USER 'loyalty'@'%';
CREATE USER 'loyalty'@'%' IDENTIFIED BY 'loyalty';
GRANT ALL ON loyalty.* TO 'loyalty'@'%';
GRANT USAGE ON *.* TO 'loyalty'@'localhost';
DROP USER 'loyalty'@'localhost';
CREATE USER 'loyalty'@'localhost' IDENTIFIED BY 'loyalty';
GRANT ALL ON loyalty.* TO 'loyalty'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'loyalty'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE loyalty CHARACTER SET utf8 COLLATE utf8_bin;

USE  loyalty;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
