/****************************************************** clean up database & create test schema START ******************************************************/
DROP SCHEMA IF EXISTS test;
CREATE SCHEMA test;
GRANT USAGE ON *.* TO 'test'@'%';
DROP USER 'test'@'%';
CREATE USER 'test'@'%' IDENTIFIED BY 'test';
GRANT ALL ON test.* TO 'test'@'%';
GRANT USAGE ON *.* TO 'test'@'localhost';
DROP USER 'test'@'localhost';
CREATE USER 'test'@'localhost' IDENTIFIED BY 'test';
GRANT ALL ON test.* TO 'test'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'test'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE test CHARACTER SET utf8 COLLATE utf8_bin;

USE  test;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
