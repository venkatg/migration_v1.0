/****************************************************** clean up database & create cashier schema START ******************************************************/
DROP SCHEMA IF EXISTS cashier;
CREATE SCHEMA cashier;
GRANT USAGE ON *.* TO 'cashier'@'%';
DROP USER 'cashier'@'%';
CREATE USER 'cashier'@'%' IDENTIFIED BY 'cashier';
GRANT ALL ON cashier.* TO 'cashier'@'%';
GRANT USAGE ON *.* TO 'cashier'@'localhost';
DROP USER 'cashier'@'localhost';
CREATE USER 'cashier'@'localhost' IDENTIFIED BY 'cashier';
GRANT ALL ON cashier.* TO 'cashier'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'cashier'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE cashier CHARACTER SET utf8 COLLATE utf8_bin;

USE  cashier;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
