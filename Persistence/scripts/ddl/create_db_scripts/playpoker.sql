/****************************************************** clean up database & create playpoker schema START ******************************************************/
DROP SCHEMA IF EXISTS playpoker;
CREATE SCHEMA playpoker;
GRANT USAGE ON *.* TO 'playpoker'@'%';
DROP USER 'playpoker'@'%';
CREATE USER 'playpoker'@'%' IDENTIFIED BY 'playpoker';
GRANT ALL ON playpoker.* TO 'playpoker'@'%';
GRANT USAGE ON *.* TO 'playpoker'@'localhost';
DROP USER 'playpoker'@'localhost';
CREATE USER 'playpoker'@'localhost' IDENTIFIED BY 'playpoker';
GRANT ALL ON playpoker.* TO 'playpoker'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'playpoker'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE playpoker CHARACTER SET utf8 COLLATE utf8_bin;

USE  playpoker;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
