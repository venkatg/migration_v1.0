/****************************************************** clean up database & create playfund schema START ******************************************************/
DROP SCHEMA IF EXISTS playfund;
CREATE SCHEMA playfund;
GRANT USAGE ON *.* TO 'playfund'@'%';
DROP USER 'playfund'@'%';
CREATE USER 'playfund'@'%' IDENTIFIED BY 'playfund';
GRANT ALL ON playfund.* TO 'playfund'@'%';
GRANT USAGE ON *.* TO 'playfund'@'localhost';
DROP USER 'playfund'@'localhost';
CREATE USER 'playfund'@'localhost' IDENTIFIED BY 'playfund';
GRANT ALL ON playfund.* TO 'playfund'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'playfund'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE playfund CHARACTER SET utf8 COLLATE utf8_bin;

USE  playfund;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
