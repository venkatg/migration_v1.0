/****************************************************** clean up database & create trny schema START ******************************************************/
DROP SCHEMA IF EXISTS trny;
CREATE SCHEMA trny;
GRANT USAGE ON *.* TO 'trny'@'%';
DROP USER 'trny'@'%';
CREATE USER 'trny'@'%' IDENTIFIED BY 'trny';
GRANT ALL ON trny.* TO 'trny'@'%';
GRANT USAGE ON *.* TO 'trny'@'localhost';
DROP USER 'trny'@'localhost';
CREATE USER 'trny'@'localhost' IDENTIFIED BY 'trny';
GRANT ALL ON trny.* TO 'trny'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'trny'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE trny CHARACTER SET utf8 COLLATE utf8_bin;

USE  trny;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
