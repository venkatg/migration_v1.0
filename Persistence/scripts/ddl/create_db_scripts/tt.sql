/****************************************************** clean up database & create tt schema START ******************************************************/
DROP SCHEMA IF EXISTS tt;
CREATE SCHEMA tt;
GRANT USAGE ON *.* TO 'tt'@'%';
DROP USER 'tt'@'%';
CREATE USER 'tt'@'%' IDENTIFIED BY 'tt';
GRANT ALL ON tt.* TO 'tt'@'%';
GRANT USAGE ON *.* TO 'tt'@'localhost';
DROP USER 'tt'@'localhost';
CREATE USER 'tt'@'localhost' IDENTIFIED BY 'tt';
GRANT ALL ON tt.* TO 'tt'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'tt'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE tt CHARACTER SET utf8 COLLATE utf8_bin;

USE  tt;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
