/****************************************************** clean up database & create questsoftware schema START ******************************************************/
DROP SCHEMA IF EXISTS questsoftware;
CREATE SCHEMA questsoftware;
GRANT USAGE ON *.* TO 'questsoftware'@'%';
DROP USER 'questsoftware'@'%';
CREATE USER 'questsoftware'@'%' IDENTIFIED BY 'questsoftware';
GRANT ALL ON questsoftware.* TO 'questsoftware'@'%';
GRANT USAGE ON *.* TO 'questsoftware'@'localhost';
DROP USER 'questsoftware'@'localhost';
CREATE USER 'questsoftware'@'localhost' IDENTIFIED BY 'questsoftware';
GRANT ALL ON questsoftware.* TO 'questsoftware'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'questsoftware'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE questsoftware CHARACTER SET utf8 COLLATE utf8_bin;

USE  questsoftware;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
