/****************************************************** clean up database & create vendor schema START ******************************************************/
DROP SCHEMA IF EXISTS vendor;
CREATE SCHEMA vendor;
GRANT USAGE ON *.* TO 'vendor'@'%';
DROP USER 'vendor'@'%';
CREATE USER 'vendor'@'%' IDENTIFIED BY 'vendor';
GRANT ALL ON vendor.* TO 'vendor'@'%';
GRANT USAGE ON *.* TO 'vendor'@'localhost';
DROP USER 'vendor'@'localhost';
CREATE USER 'vendor'@'localhost' IDENTIFIED BY 'vendor';
GRANT ALL ON vendor.* TO 'vendor'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'vendor'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE vendor CHARACTER SET utf8 COLLATE utf8_bin;

USE  vendor;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
