/****************************************************** clean up database & create bonus schema START ******************************************************/
DROP SCHEMA IF EXISTS bonus;
CREATE SCHEMA bonus;
GRANT USAGE ON *.* TO 'bonus'@'%';
DROP USER 'bonus'@'%';
CREATE USER 'bonus'@'%' IDENTIFIED BY 'bonus';
GRANT ALL ON bonus.* TO 'bonus'@'%';
GRANT USAGE ON *.* TO 'bonus'@'localhost';
DROP USER 'bonus'@'localhost';
CREATE USER 'bonus'@'localhost' IDENTIFIED BY 'bonus';
GRANT ALL ON bonus.* TO 'bonus'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'bonus'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE bonus CHARACTER SET utf8 COLLATE utf8_bin;

USE  bonus;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
