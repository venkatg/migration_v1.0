/****************************************************** clean up database & create segment schema START ******************************************************/
DROP SCHEMA IF EXISTS segment;
CREATE SCHEMA segment;
GRANT USAGE ON *.* TO 'segment'@'%';
DROP USER 'segment'@'%';
CREATE USER 'segment'@'%' IDENTIFIED BY 'segment';
GRANT ALL ON segment.* TO 'segment'@'%';
GRANT USAGE ON *.* TO 'segment'@'localhost';
DROP USER 'segment'@'localhost';
CREATE USER 'segment'@'localhost' IDENTIFIED BY 'segment';
GRANT ALL ON segment.* TO 'segment'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'segment'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE segment CHARACTER SET utf8 COLLATE utf8_bin;

USE  segment;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
