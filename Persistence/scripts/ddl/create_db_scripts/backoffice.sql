/****************************************************** clean up database & create backoffice schema START ******************************************************/
DROP SCHEMA IF EXISTS backoffice;
CREATE SCHEMA backoffice;
GRANT USAGE ON *.* TO 'backoffice'@'%';
DROP USER 'backoffice'@'%';
CREATE USER 'backoffice'@'%' IDENTIFIED BY 'backoffice';
GRANT ALL ON backoffice.* TO 'backoffice'@'%';
GRANT USAGE ON *.* TO 'backoffice'@'localhost';
DROP USER 'backoffice'@'localhost';
CREATE USER 'backoffice'@'localhost' IDENTIFIED BY 'backoffice';
GRANT ALL ON backoffice.* TO 'backoffice'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'backoffice'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE backoffice CHARACTER SET utf8 COLLATE utf8_bin;

USE  backoffice;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
