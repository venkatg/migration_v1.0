/****************************************************** clean up database & create fx schema START ******************************************************/
DROP SCHEMA IF EXISTS fx;
CREATE SCHEMA fx;
GRANT USAGE ON *.* TO 'fx'@'%';
DROP USER 'fx'@'%';
CREATE USER 'fx'@'%' IDENTIFIED BY 'fx';
GRANT ALL ON fx.* TO 'fx'@'%';
GRANT USAGE ON *.* TO 'fx'@'localhost';
DROP USER 'fx'@'localhost';
CREATE USER 'fx'@'localhost' IDENTIFIED BY 'fx';
GRANT ALL ON fx.* TO 'fx'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'fx'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE fx CHARACTER SET utf8 COLLATE utf8_bin;

USE  fx;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
