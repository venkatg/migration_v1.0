/****************************************************** clean up database & create optionfair_trading schema START ******************************************************/
DROP SCHEMA IF EXISTS optionfair_trading;
CREATE SCHEMA optionfair_trading;
GRANT USAGE ON *.* TO 'optionfair_trading'@'%';
DROP USER 'optionfair_trading'@'%';
CREATE USER 'optionfair_trading'@'%' IDENTIFIED BY 'optionfair_trading';
GRANT ALL ON optionfair_trading.* TO 'optionfair_trading'@'%';
GRANT USAGE ON *.* TO 'optionfair_trading'@'localhost';
DROP USER 'optionfair_trading'@'localhost';
CREATE USER 'optionfair_trading'@'localhost' IDENTIFIED BY 'optionfair_trading';
GRANT ALL ON optionfair_trading.* TO 'optionfair_trading'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'optionfair_trading'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE optionfair_trading CHARACTER SET utf8 COLLATE utf8_bin;

USE  optionfair_trading;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
