/****************************************************** clean up database & create risk_engage schema START ******************************************************/
DROP SCHEMA IF EXISTS risk_engage;
CREATE SCHEMA risk_engage;
GRANT USAGE ON *.* TO 'risk_engage'@'%';
DROP USER 'risk_engage'@'%';
CREATE USER 'risk_engage'@'%' IDENTIFIED BY 'risk_engage';
GRANT ALL ON risk_engage.* TO 'risk_engage'@'%';
GRANT USAGE ON *.* TO 'risk_engage'@'localhost';
DROP USER 'risk_engage'@'localhost';
CREATE USER 'risk_engage'@'localhost' IDENTIFIED BY 'risk_engage';
GRANT ALL ON risk_engage.* TO 'risk_engage'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'risk_engage'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE risk_engage CHARACTER SET utf8 COLLATE utf8_bin;

USE  risk_engage;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
