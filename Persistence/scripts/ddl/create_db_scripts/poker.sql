/****************************************************** clean up database & create poker schema START ******************************************************/
DROP SCHEMA IF EXISTS poker;
CREATE SCHEMA poker;
GRANT USAGE ON *.* TO 'poker'@'%';
DROP USER 'poker'@'%';
CREATE USER 'poker'@'%' IDENTIFIED BY 'poker';
GRANT ALL ON poker.* TO 'poker'@'%';
GRANT USAGE ON *.* TO 'poker'@'localhost';
DROP USER 'poker'@'localhost';
CREATE USER 'poker'@'localhost' IDENTIFIED BY 'poker';
GRANT ALL ON poker.* TO 'poker'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'poker'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE poker CHARACTER SET utf8 COLLATE utf8_bin;

USE  poker;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
