/****************************************************** clean up database & create csm schema START ******************************************************/
DROP SCHEMA IF EXISTS csm;
CREATE SCHEMA csm;
GRANT USAGE ON *.* TO 'csm'@'%';
DROP USER 'csm'@'%';
CREATE USER 'csm'@'%' IDENTIFIED BY 'csm';
GRANT ALL ON csm.* TO 'csm'@'%';
GRANT USAGE ON *.* TO 'csm'@'localhost';
DROP USER 'csm'@'localhost';
CREATE USER 'csm'@'localhost' IDENTIFIED BY 'csm';
GRANT ALL ON csm.* TO 'csm'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'csm'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE csm CHARACTER SET utf8 COLLATE utf8_bin;

USE  csm;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
