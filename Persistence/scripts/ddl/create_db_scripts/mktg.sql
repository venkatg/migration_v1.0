/****************************************************** clean up database & create mktg schema START ******************************************************/
DROP SCHEMA IF EXISTS mktg;
CREATE SCHEMA mktg;
GRANT USAGE ON *.* TO 'mktg'@'%';
DROP USER 'mktg'@'%';
CREATE USER 'mktg'@'%' IDENTIFIED BY 'mktg';
GRANT ALL ON mktg.* TO 'mktg'@'%';
GRANT USAGE ON *.* TO 'mktg'@'localhost';
DROP USER 'mktg'@'localhost';
CREATE USER 'mktg'@'localhost' IDENTIFIED BY 'mktg';
GRANT ALL ON mktg.* TO 'mktg'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'mktg'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE mktg CHARACTER SET utf8 COLLATE utf8_bin;

USE  mktg;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
