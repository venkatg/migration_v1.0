/****************************************************** clean up database & create master schema START ******************************************************/
DROP SCHEMA IF EXISTS master;
CREATE SCHEMA master;
GRANT USAGE ON *.* TO 'master'@'%';
DROP USER 'master'@'%';
CREATE USER 'master'@'%' IDENTIFIED BY 'master';
GRANT ALL ON master.* TO 'master'@'%';
GRANT USAGE ON *.* TO 'master'@'localhost';
DROP USER 'master'@'localhost';
CREATE USER 'master'@'localhost' IDENTIFIED BY 'master';
GRANT ALL ON master.* TO 'master'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'master'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE master CHARACTER SET utf8 COLLATE utf8_bin;

USE  master;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
