/****************************************************** clean up database & create test_engage schema START ******************************************************/
DROP SCHEMA IF EXISTS test_engage;
CREATE SCHEMA test_engage;
GRANT USAGE ON *.* TO 'test_engage'@'%';
DROP USER 'test_engage'@'%';
CREATE USER 'test_engage'@'%' IDENTIFIED BY 'test_engage';
GRANT ALL ON test_engage.* TO 'test_engage'@'%';
GRANT USAGE ON *.* TO 'test_engage'@'localhost';
DROP USER 'test_engage'@'localhost';
CREATE USER 'test_engage'@'localhost' IDENTIFIED BY 'test_engage';
GRANT ALL ON test_engage.* TO 'test_engage'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'test_engage'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE test_engage CHARACTER SET utf8 COLLATE utf8_bin;

USE  test_engage;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
