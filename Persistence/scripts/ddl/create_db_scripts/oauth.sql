/****************************************************** clean up database & create oauth schema START ******************************************************/
DROP SCHEMA IF EXISTS oauth;
CREATE SCHEMA oauth;
GRANT USAGE ON *.* TO 'oauth'@'%';
DROP USER 'oauth'@'%';
CREATE USER 'oauth'@'%' IDENTIFIED BY 'oauth';
GRANT ALL ON oauth.* TO 'oauth'@'%';
GRANT USAGE ON *.* TO 'oauth'@'localhost';
DROP USER 'oauth'@'localhost';
CREATE USER 'oauth'@'localhost' IDENTIFIED BY 'oauth';
GRANT ALL ON oauth.* TO 'oauth'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'oauth'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE oauth CHARACTER SET utf8 COLLATE utf8_bin;

USE  oauth;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
