/****************************************************** clean up database & create recon schema START ******************************************************/
DROP SCHEMA IF EXISTS recon;
CREATE SCHEMA recon;
GRANT USAGE ON *.* TO 'recon'@'%';
DROP USER 'recon'@'%';
CREATE USER 'recon'@'%' IDENTIFIED BY 'recon';
GRANT ALL ON recon.* TO 'recon'@'%';
GRANT USAGE ON *.* TO 'recon'@'localhost';
DROP USER 'recon'@'localhost';
CREATE USER 'recon'@'localhost' IDENTIFIED BY 'recon';
GRANT ALL ON recon.* TO 'recon'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'recon'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE recon CHARACTER SET utf8 COLLATE utf8_bin;

USE  recon;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
