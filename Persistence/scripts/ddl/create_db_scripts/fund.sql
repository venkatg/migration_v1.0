/****************************************************** clean up database & create fund schema START ******************************************************/
DROP SCHEMA IF EXISTS fund;
CREATE SCHEMA fund;
GRANT USAGE ON *.* TO 'fund'@'%';
DROP USER 'fund'@'%';
CREATE USER 'fund'@'%' IDENTIFIED BY 'fund';
GRANT ALL ON fund.* TO 'fund'@'%';
GRANT USAGE ON *.* TO 'fund'@'localhost';
DROP USER 'fund'@'localhost';
CREATE USER 'fund'@'localhost' IDENTIFIED BY 'fund';
GRANT ALL ON fund.* TO 'fund'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'fund'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE fund CHARACTER SET utf8 COLLATE utf8_bin;

USE  fund;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
