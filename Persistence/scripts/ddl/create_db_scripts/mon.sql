/****************************************************** clean up database & create mon schema START ******************************************************/
DROP SCHEMA IF EXISTS mon;
CREATE SCHEMA mon;
GRANT USAGE ON *.* TO 'mon'@'%';
DROP USER 'mon'@'%';
CREATE USER 'mon'@'%' IDENTIFIED BY 'mon';
GRANT ALL ON mon.* TO 'mon'@'%';
GRANT USAGE ON *.* TO 'mon'@'localhost';
DROP USER 'mon'@'localhost';
CREATE USER 'mon'@'localhost' IDENTIFIED BY 'mon';
GRANT ALL ON mon.* TO 'mon'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'mon'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE mon CHARACTER SET utf8 COLLATE utf8_bin;

USE  mon;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
