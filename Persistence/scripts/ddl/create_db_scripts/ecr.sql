/****************************************************** clean up database & create ecr schema START ******************************************************/
DROP SCHEMA IF EXISTS ecr;
CREATE SCHEMA ecr;
GRANT USAGE ON *.* TO 'ecr'@'%';
DROP USER 'ecr'@'%';
CREATE USER 'ecr'@'%' IDENTIFIED BY 'ecr';
GRANT ALL ON ecr.* TO 'ecr'@'%';
GRANT USAGE ON *.* TO 'ecr'@'localhost';
DROP USER 'ecr'@'localhost';
CREATE USER 'ecr'@'localhost' IDENTIFIED BY 'ecr';
GRANT ALL ON ecr.* TO 'ecr'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'ecr'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE ecr CHARACTER SET utf8 COLLATE utf8_bin;

USE  ecr;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
