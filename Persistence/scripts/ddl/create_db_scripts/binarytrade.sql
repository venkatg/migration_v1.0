/****************************************************** clean up database & create binarytrade schema START ******************************************************/
DROP SCHEMA IF EXISTS binarytrade;
CREATE SCHEMA binarytrade;
GRANT USAGE ON *.* TO 'binarytrade'@'%';
DROP USER 'binarytrade'@'%';
CREATE USER 'binarytrade'@'%' IDENTIFIED BY 'binarytrade';
GRANT ALL ON binarytrade.* TO 'binarytrade'@'%';
GRANT USAGE ON *.* TO 'binarytrade'@'localhost';
DROP USER 'binarytrade'@'localhost';
CREATE USER 'binarytrade'@'localhost' IDENTIFIED BY 'binarytrade';
GRANT ALL ON binarytrade.* TO 'binarytrade'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'binarytrade'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE binarytrade CHARACTER SET utf8 COLLATE utf8_bin;

USE  binarytrade;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
