/****************************************************** clean up database & create messaging schema START ******************************************************/
DROP SCHEMA IF EXISTS messaging;
CREATE SCHEMA messaging;
GRANT USAGE ON *.* TO 'messaging'@'%';
DROP USER 'messaging'@'%';
CREATE USER 'messaging'@'%' IDENTIFIED BY 'messaging';
GRANT ALL ON messaging.* TO 'messaging'@'%';
GRANT USAGE ON *.* TO 'messaging'@'localhost';
DROP USER 'messaging'@'localhost';
CREATE USER 'messaging'@'localhost' IDENTIFIED BY 'messaging';
GRANT ALL ON messaging.* TO 'messaging'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'messaging'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE messaging CHARACTER SET utf8 COLLATE utf8_bin;

USE  messaging;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
