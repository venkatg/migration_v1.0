/****************************************************** clean up database & create tungsten_s1 schema START ******************************************************/
DROP SCHEMA IF EXISTS tungsten_s1;
CREATE SCHEMA tungsten_s1;
GRANT USAGE ON *.* TO 'tungsten_s1'@'%';
DROP USER 'tungsten_s1'@'%';
CREATE USER 'tungsten_s1'@'%' IDENTIFIED BY 'tungsten_s1';
GRANT ALL ON tungsten_s1.* TO 'tungsten_s1'@'%';
GRANT USAGE ON *.* TO 'tungsten_s1'@'localhost';
DROP USER 'tungsten_s1'@'localhost';
CREATE USER 'tungsten_s1'@'localhost' IDENTIFIED BY 'tungsten_s1';
GRANT ALL ON tungsten_s1.* TO 'tungsten_s1'@'localhost';
GRANT ALL PRIVILEGES ON *.* TO 'tungsten_s1'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


ALTER DATABASE tungsten_s1 CHARACTER SET utf8 COLLATE utf8_bin;

USE  tungsten_s1;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
