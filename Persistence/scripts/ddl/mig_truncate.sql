select now() starttime;


use ecr
truncate table tbl_migration_temp;
truncate table tbl_migration_map;

use vendor
truncate table tbl_migration_vendor_map;
truncate table tbl_migration_cashier_map;
truncate table tbl_migration_vendor_temp;

 
use ecr
SET FOREIGN_KEY_CHECKS = 0;
truncate table tbl_ecr;
truncate table tbl_ecr_log;
truncate table tbl_ecr_profile_list;
truncate table tbl_ecr_profile;
truncate table tbl_ecr_screen_name;
truncate table tbl_ecr_screen_name_log;
truncate table tbl_ecr_auth;
truncate table tbl_ecr_auth_log;
truncate table tbl_ecr_conversion_info;
truncate table tbl_ecr_secret_question;
truncate table tbl_ecr_secret_question_log;
truncate table tbl_ecr_category;
truncate table tbl_ecr_category_log;
truncate table tbl_ecr_sales_status;
truncate table tbl_ecr_signup_info;
truncate table tbl_ecr_address_vrfn;
truncate table tbl_ecr_address_vrfn_log;
truncate table tbl_ecr_auth_label_info;
truncate table tbl_ecr_auth_label_info_log;
truncate table tbl_ecr_identity_vrfn;
truncate table tbl_ecr_identity_vrfn_log;
truncate table tbl_ecr_age_vrfn;
truncate table tbl_ecr_age_vrfn_log;
truncate table tbl_ecr_labels;
truncate table tbl_ecr_labels_log;
truncate table tbl_ecr_mobile_details;
truncate table tbl_ecr_mobile_vrfn;
truncate table tbl_ecr_mobile_vrfn_log;
truncate table tbl_ecr_aoa;
truncate table tbl_ecr_aoa_log;
truncate table tbl_ecr_account_verified_data;
truncate table tbl_ecr_linked_accounts_logs;
truncate table tbl_ecr_linked_accounts;
SET FOREIGN_KEY_CHECKS = 1;






use vendor
SET FOREIGN_KEY_CHECKS = 0;
truncate table tbl_vendor_ecr_id_mapping;
truncate table tbl_vendor_ecr_id_mapping_log;
truncate table tbl_vendor_fund_txn;
truncate table tbl_vendor_fund_txn_log;
truncate table tbl_vendor_txn_mapping;
truncate table tbl_vendor_txn_mapping_log;
SET FOREIGN_KEY_CHECKS = 1;

use cashier
SET FOREIGN_KEY_CHECKS = 0;
truncate table tbl_instrument;
truncate table tbl_instrument_log;
truncate table tbl_instrument_billing_address;
truncate table tbl_instrument_billing_address_log;
truncate table tbl_deposit_withdrawl_flags;
truncate table tbl_deposit_withdrawl_flags_log;
truncate table tbl_cashier_deposit;
truncate table tbl_cashier_deposit_log;
truncate table tbl_cashier_deposit_summary;
truncate table tbl_cashier_deposit_summary_log;
truncate table tbl_cashier_cashout;
truncate table tbl_cashier_cashout_log;
truncate table tbl_cashier_cashout_summary;
truncate table tbl_cashier_cashout_summary_log;
truncate table tbl_cashier_wallet_txn;
truncate table tbl_cashier_wallet;
truncate table tbl_wire_transaction;
truncate table tbl_wire_transaction_log;
truncate table tbl_card_token_details;
SET FOREIGN_KEY_CHECKS = 1;

use fund
SET FOREIGN_KEY_CHECKS = 0;
truncate table tbl_real_fund;
truncate table tbl_real_fund_log;
truncate table tbl_real_fund_session;
truncate table tbl_real_fund_session_log;
truncate table tbl_real_fund_txn;
truncate table tbl_real_fund_txn_log;
truncate table tbl_realcash_sub_fund_txn;
truncate table tbl_fund_deposit_txn;
truncate table tbl_fund_deposit_txn_log;
truncate table tbl_fund_conversion_info;
SET FOREIGN_KEY_CHECKS = 1;

use messaging
SET FOREIGN_KEY_CHECKS = 0;
truncate table tbl_message_subscription_data;

use csm

truncate table tbl_mst_alert;

SET FOREIGN_KEY_CHECKS = 1;
select now() starttime;