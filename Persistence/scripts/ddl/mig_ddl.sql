use ecr ;

CREATE TABLE IF NOT EXISTS tbl_migration_map(
	c_id bigint NOT NULL auto_increment unique,		
	c_ecr_id bigint unique ,		
	c_external_id bigint unique ,		
	c_partner_account_id varchar(100) NOT NULL,		
	c_partner_id varchar(30) not null,	
	c_source_partner_id varchar(30) not null,	
	c_partner_account_id_2 varchar(100) ,		
	c_partner_external_id varchar(64) DEFAULT NULL,		
	c_status varchar(30) comment 'skipped/initiated/etc..', 		
	c_created_time datetime not null ,		
	c_update_time datetime not null default now(),		
	c_created_by varchar(30),		
	c_updated_by varchar(30),		
	unique (c_partner_account_id)		
);

alter table tbl_migration_map add column c_account_type_id int(11);

CREATE INDEX acc_ecr ON tbl_migration_map (c_partner_account_id,c_ecr_id);

CREATE TABLE IF NOT EXISTS tbl_migration_temp(
	c_partner_account_id varchar(100) unique,
	c_acc_balance 	bigint(20),
	c_bal_for_withdrawal bigint(20),
	c_currency  varchar(5)
);

CREATE TABLE IF NOT EXISTS tbl_ecr_account_verified_data(
	c_id bigint NOT NULL auto_increment unique,
	c_ecr_id bigint NOT NULL,
	c_email_verified bit(1) NOT NULL DEFAULT b'0',
	c_allow_contact bit(1) NOT NULL DEFAULT b'0',
	c_terms_conditions_approved bit(1) NOT NULL DEFAULT b'0',
	c_created_time datetime not null ,
	c_update_time datetime not null default now(),
	c_created_by varchar(30),
	c_updated_by varchar(30)
);

 
use messaging

CREATE TABLE IF NOT EXISTS tbl_message_subscription_data(
	c_id bigint NOT NULL auto_increment unique,
	c_ecr_id bigint NOT NULL,
	c_subscribe_to_newsletter bit(1) NOT NULL,
	c_created_time datetime not null ,
	c_update_time datetime not null default now(),
	c_created_by varchar(30),
	c_updated_by varchar(30)
);

use vendor;

CREATE TABLE `tbl_migration_cashier_map` (
  `c_ecr_id` bigint(20) NOT NULL,
  `c_external_id` bigint(20) DEFAULT NULL,
  `c_partner_account_id` varchar(100) COLLATE utf8_bin NOT NULL,
  `c_cashier_txn_id` bigint(20) NOT NULL,
  `c_fund_txn_id` bigint(20) NOT NULL,
  `c_sub_txn_id` bigint(20) NOT NULL,
  `c_fund_session_id` bigint(20) NOT NULL,
  `c_txn_id_at_bank` bigint(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS tbl_migration_vendor_temp(
	id BIGINT NOT NULL AUTO_INCREMENT unique,
	`c_fund_session_id` bigint(20) NOT NULL,
	`c_position_id` bigint(20) unsigned,
	`c_partner_account_id` varchar(100) COLLATE utf8_bin NOT NULL,
	`c_id` bigint(20)
);

CREATE INDEX position_id ON tbl_migration_vendor_temp (c_position_id);

CREATE INDEX partner_account_id ON tbl_migration_vendor_temp (c_partner_account_id);

CREATE TABLE `tbl_migration_vendor_map` (
  `c_ecr_id` bigint(20) NOT NULL,
  `c_external_id` bigint(20) DEFAULT NULL,
  `c_partner_account_id` varchar(100) COLLATE utf8_bin NOT NULL,
  `c_vendor_txn_id` bigint(20) NOT NULL,
  `c_fund_txn_id` bigint(20) NOT NULL,
  `c_fund_session_id` bigint(20) NOT NULL,
  `c_sub_txn_id` bigint(20) NOT NULL,
  `c_txn_id_at_vendor` bigint(20) NOT NULL unique,
  `c_position_id` bigint(20) unsigned
  );

  
  
use ecr;

