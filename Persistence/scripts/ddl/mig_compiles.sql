select now() starttime;

mysql -uroot -p < proc_migrate_map_data_v1dot0.sql

mysql -uroot -p < proc_migrate_map_cashier_data_v1dot1.sql

mysql -uroot -p < proc_migrate_vendor_temp_data_v1dot1.sql

mysql -uroot -p < proc_migrate_map_vendor_data_v1dot1.sql

mysql -uroot -p < proc_insert_linked_account_data.sql

mysql -uroot -p < fun_mig_ecr_aoa_data_map.sql

mysql -uroot -p < fun_mig_gen_ecr_id.sql

mysql -uroot -p < fun_mig_get_account_balance.sql

mysql -uroot -p < fun_mig_get_bt_op_type.sql

mysql -uroot -p < fun_mig_get_bt_option_value.sql

mysql -uroot -p < fun_mig_get_bt_txn_type.sql

mysql -uroot -p < fun_mig_get_category_data.sql

mysql -uroot -p < fun_mig_get_csm_note_status.sql

mysql -uroot -p < fun_mig_get_csm_prb_cat.sql

mysql -uroot -p < fun_mig_get_csm_prb_sub_cat.sql

mysql -uroot -p < fun_mig_get_currency.sql

mysql -uroot -p < fun_mig_get_ecr_category.sql

mysql -uroot -p < fun_mig_get_ecr_status.sql

mysql -uroot -p < fun_mig_get_label_id.sql

mysql -uroot -p < fun_mig_get_linked_account_count.sql

mysql -uroot -p < fun_mig_get_linked_account_type.sql

mysql -uroot -p < fun_mig_get_mobile_vrfn_status.sql

mysql -uroot -p < fun_mig_get_regulation_questionnaire_status.sql

mysql -uroot -p < fun_mig_get_title_id.sql

mysql -uroot -p < fun_mig_get_txn_status.sql

mysql -uroot -p < fun_mig_get_vrfn_status.sql

mysql -uroot -p < fun_mig_is_deposit_allowed.sql

/*  
mysql -uroot -p

--keep file here  /var/lib/mysql/ecr/account_balances.csv

use ecr


LOAD data infile 'account_balances-4.csv' into table ecr.tbl_migration_temp 
fields terminated by ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;


call proc_migrate_map_data_v1dot0();

use vendor

call proc_migrate_map_cashier_data_v1dot1(50000,20060404);

call proc_migrate_vendor_temp_data_v1dot1(99999,'20100401');

call proc_migrate_map_vendor_data_v1dot1(99999,'20100401');

mysql -uroot -p ecr < function_procedure_exec.sql >compile.log

mysql -uroot -p ecr < migration_init_scripts.sql >init.log

mysql -uroot -p ecr < migrate_data_ecr.sql >ecr.log

mysql -uroot -p cashier < migrate_data_cashier.sql >cashier.log

mysql -uroot -p vendor < migrate_data_vendor.sql >vendor.log

mysql -uroot -p fund < migrate_data_fund.sql >fund.log

mysql -uroot -p messaging < migrate_data_others.sql >others.log



*/

select now() endtime;