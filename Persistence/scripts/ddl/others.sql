
INSERT INTO tbl_migration_temp(c_partner_account_id, c_acc_balance, c_bal_for_withdrawal, c_currency)
SELECT
 c.id as account_id,
 c.balance as balance,
 IF((c.balance_for_calc+c.open_position_amount) < 0, 0, c.balance_for_calc+c.open_position_amount) as balance_for_withdrawal,
 c.symbol as currency_symbol
from (SELECT b.*,
            b.balance-b.total_approved_bonus-b.profit_adjustment_earnings as balance_for_calc /* balance for calculating withdrawal, pnl and net balance */
     from (SELECT a.*,
                  IF(a.total_approved_bonus != 0, IFNULL((SELECT profit_adjustment_earnings
                                                          FROM optionfair_trading.bonus_account_rel b
                                                          where b.wager_req = true
                                                            and b.status = 136
                                                            and b.account_id = a.id
                                                          order by b.first_approve_date asc limit 1), 0), 0) as profit_adjustment_earnings /* for symmetryc withdrawal policy it's bonus earnings */
from 
(SELECT a.id, a.currency_id, a.balance balance, cur.symbol,
(SELECT ifnull(sum(b.amount), 0) FROM optionfair_trading.bonus_account_rel b where b.wager_req = true and b.status = 136 and b.account_id = a.id) total_approved_bonus,
(SELECT ifnull(sum(p.amount), 0) FROM optionfair_trading.position p where p.account_id = a.id and p.status in (18, 41, 42, 234)) open_position_amount
FROM optionfair_trading.account a 
              left join  optionfair_trading.currency cur on a.currency_id = cur.currency_id
order by a.brand_id, a.currency_id) a
) b
) c;

select a.c_ecr_id , a.c_external_id , b.c_partner_account_id as of_id, a.c_email_id as email from tbl_ecr a join tbl_migration_map b on a.c_ecr_id=b.c_ecr_id 
INTO OUTFILE '/tmp/bonus_mig_acc.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT
 c.id as account_id,
 c.balance as balance,
 IF((c.balance_for_calc+c.open_position_amount) < 0, 0, c.balance_for_calc+c.open_position_amount) as balance_for_withdrawal,
 c.symbol as currency_symbol
from (SELECT b.*,
            b.balance-b.total_approved_bonus-b.profit_adjustment_earnings as balance_for_calc /* balance for calculating withdrawal, pnl and net balance */
     from (SELECT a.*,
                  IF(a.total_approved_bonus != 0, IFNULL((SELECT profit_adjustment_earnings
                                                          FROM bonus_account_rel b
                                                          where b.wager_req = true
                                                            and b.status = 136
                                                            and b.account_id = a.id
                                                          order by b.first_approve_date asc limit 1), 0), 0) as profit_adjustment_earnings /* for symmetryc withdrawal policy it's bonus earnings */
from 
(SELECT a.id, a.currency_id, a.balance balance, cur.symbol,
(SELECT ifnull(sum(b.amount), 0) FROM bonus_account_rel b where b.wager_req = true and b.status = 136 and b.account_id = a.id) total_approved_bonus,
(SELECT ifnull(sum(p.amount), 0) FROM position p where p.account_id = a.id and p.status in (18, 41, 42, 234)) open_position_amount
FROM account a 
              left join  currency cur on a.currency_id = cur.currency_id
order by a.brand_id, a.currency_id) a
) b
) c
INTO OUTFILE '/tmp/account_balance.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';


select a.id as "Account ID",
      c.symbol as "Currency Symbol",
      sum(bar.amount) as "Bonus Amount",
      (sum(bar.trading_volume)/sum(bar.amount*wager_base))*100 as "wagering status (in percentage)",
      sum((bar.amount)*wager_base) as "wagering requirement",
      sum(bar.trading_volume) as "wagering status in volume"
from account a inner join bonus_account_rel bar
               on bar.account_id = a.id
              inner join currency c
               on c.currency_id = a.currency_id
where bar.status = 136
 and bar.wager_req = b'1'
group by a.id, c.symbol 
order by a.id
INTO OUTFILE '/tmp/bonus_offer.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

