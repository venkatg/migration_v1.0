


USE vendor;
SELECT DATABASE();

delimiter #
DROP PROCEDURE IF EXISTS proc_migrate_map_cashier_data_v1dot0#

CREATE PROCEDURE proc_migrate_map_cashier_data_v1dot0()
BEGIN
	declare i INT default 1;
	declare j INT default 1;
	declare cash_txn date default '20060312';
	declare fund_txn date default '20070411';
	declare sub_txn date default '20080510';
	declare session_txn date default '20090609';
	declare v_ecr_id bigint(20) ;
    declare v_external_id bigint(20);
	declare v_account_id varchar(100);
    declare v_id bigint(20);
	declare done boolean DEFAULT FALSE;
	
	DECLARE curs CURSOR FOR SELECT emig.c_ecr_id, emig.c_external_id, ban.account_id, ban.id FROM ecr.tbl_migration_map emig join optionfair_trading.banking ban on emig.c_partner_account_id = ban.account_id;
	 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	 
	 
	  OPEN curs; 
	  read_loop: LOOP
 
	 FETCH curs INTO v_ecr_id,v_external_id,v_account_id, v_id;
	  IF done THEN
		  LEAVE read_loop;
	  END IF;
    
		INSERT INTO tbl_migration_cashier_map (c_ecr_id,c_external_id, c_partner_account_id, c_cashier_txn_id, c_fund_txn_id, c_sub_txn_id, c_fund_session_id, c_txn_id_at_bank) 
		SELECT v_ecr_id, v_external_id, v_account_id,cast(concat(DATE_FORMAT(cash_txn, '%Y%m%d'),i) AS UNSIGNED) , cast(concat(DATE_FORMAT(fund_txn, '%Y%m%d'),i) AS UNSIGNED), cast(concat(DATE_FORMAT(sub_txn, '%Y%m%d'),i) AS UNSIGNED),cast(concat(DATE_FORMAT(session_txn, '%Y%m%d'),i) AS UNSIGNED), v_id;
		
		
		
		set i=i+1;
		set j=j+1;
		
		if(i=50000) then
			set i=1;
			set cash_txn = DATE_ADD(cash_txn ,INTERVAL 5 DAY);
			set fund_txn = DATE_ADD(fund_txn ,INTERVAL 5 DAY);
			set sub_txn = DATE_ADD(sub_txn ,INTERVAL 5 DAY);
			set session_txn = DATE_ADD(session_txn ,INTERVAL 5 DAY);
		end if;
		

		
		END LOOP;	
		CLOSE curs;	

END #
delimiter ;


		




