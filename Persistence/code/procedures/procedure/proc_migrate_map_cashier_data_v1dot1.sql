


USE vendor;
SELECT DATABASE();

delimiter #
DROP PROCEDURE IF EXISTS proc_migrate_map_cashier_data_v1dot1#

CREATE PROCEDURE proc_migrate_map_cashier_data_v1dot1(in lmt int, in datein date)
BEGIN
	
	declare e INT default 0;
	declare cash_txn varchar(30);
	declare fund_txn varchar(30);
	declare sub_txn varchar(30);
	declare session_txn varchar(30);
	declare v_row_count int;
	declare v_ttl_count int;
	declare test bigint(20);
	declare done boolean DEFAULT FALSE;
	declare cnt int default 0;
	
	set cash_txn = DATE_FORMAT(datein, '%Y%m%d');
	set fund_txn = DATE_FORMAT(DATE_ADD(datein ,INTERVAL 1 YEAR), '%Y%m%d');
	set sub_txn = DATE_FORMAT(DATE_ADD(datein ,INTERVAL 2 YEAR), '%Y%m%d');
	set session_txn = DATE_FORMAT(DATE_ADD(datein ,INTERVAL 3 YEAR), '%Y%m%d');
	
    
		INSERT INTO tbl_migration_cashier_map (c_ecr_id,c_external_id, c_partner_account_id, c_cashier_txn_id, c_fund_txn_id, c_sub_txn_id, c_fund_session_id, c_txn_id_at_bank) SELECT emig.c_ecr_id, emig.c_external_id, ban.account_id,cash_txn,fund_txn,sub_txn,session_txn, ban.id FROM ecr.tbl_migration_map emig join optionfair_trading.banking ban on emig.c_partner_account_id = ban.account_id where ban.type in (102,103);
		select row_count() into v_ttl_count;
		select v_ttl_count;
		
		set @i=0, @j=0, @k=0,@l=0;
		set test = cash_txn;
		v_loop : while(v_ttl_count-e > 0) do
			update 	tbl_migration_cashier_map 
				set c_cashier_txn_id =cast(concat(cash_txn,@i:=@i+1) AS UNSIGNED),
					c_fund_txn_id = cast(concat(fund_txn,@j:=@j+1) AS UNSIGNED),
					c_sub_txn_id = cast(concat(sub_txn,@k:=@k+1) AS UNSIGNED),
					c_fund_session_id = cast(concat(session_txn,@l:=@l+1) AS UNSIGNED) 
				where c_cashier_txn_id=test 
					order by c_txn_id_at_bank 
						limit lmt;
			
			select row_count() into v_row_count;
			select cnt+1, v_row_count;
			if(v_row_count = 0) then
				leave v_loop;
			end if;
			set e=e+v_row_count;
			set cash_txn = DATE_FORMAT(DATE_ADD(cash_txn ,INTERVAL 5 DAY), '%Y%m%d');
			set fund_txn = DATE_FORMAT(DATE_ADD(fund_txn ,INTERVAL 5 DAY), '%Y%m%d');
			set sub_txn = DATE_FORMAT(DATE_ADD(sub_txn ,INTERVAL 5 DAY), '%Y%m%d');
			set session_txn = DATE_FORMAT(DATE_ADD(session_txn ,INTERVAL 5 DAY), '%Y%m%d');
			set @i=0, @j=0, @k=0,@l=0;
		
		end while;
	

END #
delimiter ;


		




