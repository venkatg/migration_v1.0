

		USE ecr;
		SELECT DATABASE();

		delimiter #
		DROP PROCEDURE IF EXISTS proc_migrate_map_data_v1dot0#

		CREATE PROCEDURE proc_migrate_map_data_v1dot0()
		BEGIN
		
		DECLARE v_ttl_count int;
		
		set @i=1,@j=1;
		INSERT INTO tbl_migration_map (c_ecr_id,c_external_id,c_partner_account_id,c_partner_id,c_source_partner_id,c_partner_account_id_2,c_partner_external_id,c_status,c_created_time, c_created_by,c_updated_by,c_account_type_id) SELECT fun_mig_gen_ecr_id(@i := @i+1,20040401000000,10000,89999), fun_mig_gen_ecr_id(@j := @j+1,20040401000000,100,899), acc.id,'optionfair','optionfair',0,acc.external_id,'Initial Data',now(),'system',null,acc.account_type_id FROM optionfair_trading.account acc;
			

		END #
		delimiter ;


		