


USE vendor;
SELECT DATABASE();

delimiter #
DROP PROCEDURE IF EXISTS proc_migrate_map_vendor_data_v1dot1#

CREATE PROCEDURE proc_migrate_map_vendor_data_v1dot1(in lmt int, in datein date)
BEGIN
	
	declare e INT default 0;
	declare vend_txn varchar(30);
	declare fund_txn varchar(30);
	declare sub_txn varchar(30);
	declare v_row_count int;
	declare v_ttl_count int;
	declare v_cnt int;
	declare test bigint(20);
	
	
	set vend_txn = DATE_FORMAT(datein, '%Y%m%d');
	set fund_txn = DATE_FORMAT(DATE_ADD(datein ,INTERVAL 1 YEAR), '%Y%m%d');
	set sub_txn = DATE_FORMAT(DATE_ADD(datein ,INTERVAL 3 YEAR), '%Y%m%d');
		
				 
		INSERT INTO tbl_migration_vendor_map (c_ecr_id,c_external_id, c_partner_account_id, c_vendor_txn_id, c_fund_txn_id, c_fund_session_id, c_sub_txn_id, c_txn_id_at_vendor, c_position_id) 
		SELECT emig.c_ecr_id, emig.c_external_id, fin.account_id,vend_txn,fund_txn,migtemp.c_fund_session_id,sub_txn, fin.id, fin.position_id 
		FROM ecr.tbl_migration_map emig  join optionfair_trading.finaincial_ledger fin on emig.c_partner_account_id = fin.account_id join tbl_migration_vendor_temp migtemp on migtemp.c_position_id = fin.position_id and migtemp.c_partner_account_id = fin.account_id where  fin.position_id <> 0;
				
		
		select row_count() into v_cnt;
		select v_cnt;
	
		INSERT INTO tbl_migration_vendor_map (c_ecr_id,c_external_id, c_partner_account_id, c_vendor_txn_id, c_fund_txn_id, c_fund_session_id, c_sub_txn_id, c_txn_id_at_vendor, c_position_id) 
		SELECT emig.c_ecr_id, emig.c_external_id, migtemp.c_partner_account_id,vend_txn,fund_txn,migtemp.c_fund_session_id,sub_txn, migtemp.c_id, migtemp.c_position_id 
		FROM ecr.tbl_migration_map emig  join tbl_migration_vendor_temp migtemp on emig.c_partner_account_id = migtemp.c_partner_account_id where migtemp.c_position_id = 0;
		
		select row_count() into v_ttl_count;
		select v_ttl_count;
		
		set v_cnt := v_cnt+v_ttl_count;
		select v_cnt as Total_count;
		
		
		set e=0;
		set @i=0, @j=0, @l=0, @cnt=0;
		set test = vend_txn;
		v1_loop : while(v_cnt-e > 0) do
			update 	tbl_migration_vendor_map 
				set c_vendor_txn_id =cast(concat(vend_txn,@i:=@i+1) AS UNSIGNED),
					c_fund_txn_id = cast(concat(fund_txn,@j:=@j+1) AS UNSIGNED),
					c_sub_txn_id = cast(concat(sub_txn,@l:=@l+1) AS UNSIGNED) 
				where c_vendor_txn_id=test 
						limit lmt;
			
			select row_count() into v_row_count;
			select @cnt :=@cnt+1, v_row_count;
			if(v_row_count = 0) then
				leave v1_loop;
			end if;
			set e=e+v_row_count;
			set vend_txn = DATE_FORMAT(DATE_ADD(vend_txn ,INTERVAL 1 DAY), '%Y%m%d');
			set fund_txn = DATE_FORMAT(DATE_ADD(fund_txn ,INTERVAL 1 DAY), '%Y%m%d');
			set sub_txn = DATE_FORMAT(DATE_ADD(sub_txn ,INTERVAL 1 DAY), '%Y%m%d');
			set @i=0, @j=0, @l=0;
		end while;
		
END #
delimiter ;


		





