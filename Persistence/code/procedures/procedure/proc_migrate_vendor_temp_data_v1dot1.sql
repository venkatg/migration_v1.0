


USE vendor;
SELECT DATABASE();

delimiter #
DROP PROCEDURE IF EXISTS proc_migrate_vendor_temp_data_v1dot1#

CREATE PROCEDURE proc_migrate_vendor_temp_data_v1dot1(in lmt int, in datein date)
BEGIN
	
	declare e INT default 0;
	declare session_txn varchar(30);
	declare v_row_count int;
	declare v_cnt int;
	declare v_ttl_count int;
	declare test bigint(20);
	
	
	
	set session_txn = DATE_FORMAT(DATE_ADD(datein ,INTERVAL 2 YEAR), '%Y%m%d');
	
		
		INSERT INTO tbl_migration_vendor_temp (c_fund_session_id, c_position_id,c_partner_account_id,c_id) 
		SELECT session_txn, fin.position_id, fin.account_id,fin.id
		FROM optionfair_trading.finaincial_ledger fin group by fin.position_id having fin.position_id <> 0;
		
		select row_count() into v_cnt;
		select v_cnt;
	
		INSERT INTO tbl_migration_vendor_temp (c_fund_session_id, c_position_id,c_partner_account_id,c_id) 
		SELECT session_txn, fin.position_id, fin.account_id, fin.id
		FROM optionfair_trading.finaincial_ledger fin where fin.position_id = 0;
		
		select row_count() into v_ttl_count;
		select v_ttl_count;
		
		set v_cnt := v_cnt+v_ttl_count;
		select v_cnt as Total_count;
		
		set @i=0, @cnt=0;
		set test = session_txn;
		v_loop : while(v_cnt-e > 0) do
			update 	tbl_migration_vendor_temp 
				set c_fund_session_id = cast(concat(session_txn,@i:=@i+1) AS UNSIGNED)
				where c_fund_session_id=test 
						limit lmt;
			
			select row_count() into v_row_count;
			select @cnt :=@cnt+1, v_row_count;
			if(v_row_count = 0) then
				leave v_loop;
			end if;
			set e=e+v_row_count;
			set session_txn = DATE_FORMAT(DATE_ADD(session_txn ,INTERVAL 1 DAY), '%Y%m%d');
			set @i=0;
		end while;
		
				
END #
delimiter ;


		





