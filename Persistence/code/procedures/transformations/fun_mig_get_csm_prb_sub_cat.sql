use csm;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_csm_prb_sub_cat;
CREATE FUNCTION fun_mig_get_csm_prb_sub_cat(in_subject_id INT(10))
    RETURNS varchar(500)
    DETERMINISTIC
    BEGIN
        
		DECLARE out_name varchar(500);
		DECLARE v_in_name varchar(500);

		select name into v_in_name from optionfair_trading.bo_note_subject where subject_id = in_subject_id  and brand_id<>3; 

        IF (v_in_name = 'CustomerComplaint') THEN
                SET out_name = 'CustomerComplaint';
        ELSEIF (v_in_name = 'ManualBonusToCustomer') THEN
                SET out_name = 'ManualBonus';
		ELSEIF (v_in_name = 'RequestForCashOut') THEN
                SET out_name = 'RequestForCashOut';         
        ELSEIF (v_in_name = 'RequestForTechnicalSupport') THEN
                SET out_name = 'RequestForTechnicalSupport';
	    ELSEIF (v_in_name = 'RequestForTradingSupport') THEN
                SET out_name = 'RequestForTradingSupport  ';
		ELSEIF (v_in_name = 'RequestForDepositSupport') THEN
                SET out_name = 'RequestForDepositSupport  ';
		ELSEIF (v_in_name = 'General') THEN
                SET out_name = 'GeneralQuery';
		ELSEIF (v_in_name = 'Deposit') THEN
                SET out_name = 'DepositQuery';
		ELSEIF (v_in_name = 'FirstCall') THEN
                SET out_name = 'VerificationCall';
		ELSEIF (v_in_name = 'FDPCall') THEN
                SET out_name = 'FDPCall';
		ELSEIF (v_in_name = 'RetentionCall') THEN
                SET out_name = 'RetentionCall';
		ELSEIF (v_in_name = 'ReAssignedLead') THEN
                SET out_name = 'ReAssignedLead';
		ELSEIF (v_in_name = 'NewLead') THEN
                SET out_name = 'SalesCall';
		ELSEIF (v_in_name = 'NewAccount') THEN
                SET out_name = 'SalesCall';
		ELSEIF (v_in_name = 'FirstDeposit') THEN
                SET out_name = 'FirstDeposit';
		ELSEIF (v_in_name = 'RetentionDeposit') THEN
                SET out_name = 'RetentionDeposit';
		ELSEIF (v_in_name = 'FailedDeposit') THEN
                SET out_name = 'FailedDeposit';
		ELSEIF (v_in_name = 'WithdrawalRequest') THEN
                SET out_name = 'RequestForCashOut';
		ELSEIF (v_in_name = 'AccountInformationUpdated') THEN
                SET out_name = 'ProfileUpdate';
		ELSEIF (v_in_name = 'NoMoreMoney') THEN
                SET out_name = 'NoMoreMoney';
		ELSEIF (v_in_name = 'AlmostNoMoreMoney') THEN
                SET out_name = 'AlmostNoMoreMoney';
        END IF;
        RETURN out_name;
    END//

