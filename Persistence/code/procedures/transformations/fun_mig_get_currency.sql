use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_currency;
CREATE FUNCTION fun_mig_get_currency(in_currency_id INT(10))
    RETURNS VARCHAR(3)
    DETERMINISTIC
    BEGIN
        DECLARE v_ecr_currency VARCHAR(3);

        SELECT symbol INTO v_ecr_currency
        FROM   optionfair_trading.currency
        WHERE  currency_id = in_currency_id;

        RETURN v_ecr_currency;
    END//

