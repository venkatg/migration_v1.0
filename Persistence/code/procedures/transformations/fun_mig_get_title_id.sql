use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_title_id;
CREATE FUNCTION fun_mig_get_title_id(in_title_id INT(11))
    RETURNS VARCHAR(5)
    DETERMINISTIC
    BEGIN
        DECLARE v_title VARCHAR(5);

        IF (in_title_id = 1) THEN
                SET v_title = 'Mr.';
        ELSEIF (in_title_id = 2) THEN
                SET v_title = 'Mrs.';
        ELSEIF (in_title_id = 3) THEN
                SET v_title = 'Miss.';
        ELSEIF (in_title_id = 4) THEN
                SET v_title = 'Ms.';
        ELSE
                SET v_title = NULL;
        END IF;
        RETURN v_title;
    END//

