use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_linked_account_type;
CREATE FUNCTION fun_mig_get_linked_account_type(in_type INT(10))
    RETURNS VARCHAR(100)
    DETERMINISTIC
    BEGIN
        DECLARE v_linked_account_type VARCHAR(100);


        SELECT lut_name INTO v_linked_account_type 
        FROM   optionfair_trading.lut 
        WHERE  id    = in_type;

        RETURN v_linked_account_type;
    END//

