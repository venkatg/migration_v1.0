use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_is_deposit_allowed;
CREATE FUNCTION fun_mig_is_deposit_allowed(in_status INT(10))
    RETURNS BOOLEAN
    DETERMINISTIC
    BEGIN
        DECLARE v_status BOOLEAN;

        IF (in_status in (400,410,500, 510))  THEN
                SET v_status = TRUE;
        ELSEIF (in_status in (600,650))  THEN
                SET v_status = FALSE;
        ELSE
                SET v_status = NULL;
        END IF;
        RETURN v_status;
    END//

