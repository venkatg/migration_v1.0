use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_ecr_category;
CREATE FUNCTION fun_mig_get_ecr_category(in_status INT(10))
    RETURNS VARCHAR(100)
    DETERMINISTIC
    BEGIN
        DECLARE v_ecr_category VARCHAR(100);

        IF (in_status in (100,200,300)) THEN
                SET v_ecr_category = 'play_user';
        ELSEIF (in_status in (410, 600, 650))  THEN
                SET v_ecr_category = 'real_user';
		ELSEIF (in_status in (400))  THEN
				SET v_ecr_category = 'c1';
        ELSEIF (in_status in (500))  THEN
				SET v_ecr_category = 'c3';
        ELSEIF (in_status in (510))  THEN
				SET v_ecr_category = 'c2';
        ELSEIF (in_status = 700)  THEN
                SET v_ecr_category = 'suspicious';
        ELSEIF (in_status = 800)  THEN
                SET v_ecr_category = 'closed';
        ELSEIF (in_status = 900)  THEN
                SET v_ecr_category = 'fraud';
        ELSE
                SET v_ecr_category = NULL;
        END IF;
        RETURN v_ecr_category;
    END//

