use fund;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_account_balance;
CREATE FUNCTION fun_mig_get_account_balance(in_acc_id bigint(20))
    RETURNS bigint(20)
    DETERMINISTIC
    BEGIN
        DECLARE v_balance bigint(20);

		select p.bal -  ifnull(q.pae,0) into v_balance
		from 
		(
			select a.id, (ifnull(a.balance,0) - sum(ifnull(b.amount,0)) ) bal
			from optionfair_trading.account a 
			left join optionfair_trading.bonus_account_rel b on  ( a.id = b.account_id and  b.status = 136 and b.wager_req = 1) 
			WHERE 
			a.id =in_acc_id
		) p left join (
			select c.account_id, ifnull(c.profit_adjustment_earnings,0) pae from 
				(
				select * from optionfair_trading.bonus_account_rel r 
				where status = 136 and wager_req=1 and account_id = in_acc_id order by first_approve_date asc 
				) c
			group by account_id
		) q on   q.account_id  = p.id ;

        RETURN v_balance;
    END//

