use cashier;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_bt_option_value;
CREATE FUNCTION fun_mig_get_bt_option_value(type int(10), card_type int(10))
    RETURNS varchar(100)
    DETERMINISTIC
    BEGIN
        DECLARE v_option_value VARCHAR(100);
		if(type = 93)  then
			SET v_option_value = 'WIRE';
		elseif(type = 95)  then
			SET v_option_value = 'SKRILL';
		elseif(type = 96)  then
			select lut_name into v_option_value from optionfair_trading.lut where id in (card_type);
		elseif(type = 141)  then
			select lut_name into v_option_value from optionfair_trading.lut where id in (card_type);
		elseif(type = 199)  then
			SET v_option_value = 'WESTERN_UNION';
		elseif(type = 200)  then
			SET v_option_value = 'LIBERTY_RESERVE';
		elseif(type = 201)  then
			SET v_option_value = 'MONEYGRAM';
		elseif(type = 271)  then
			SET v_option_value = 'CASHU';
		elseif(type = 272)  then
			SET v_option_value = 'CHINA_UNION';
		elseif(type = 273)  then
				SET v_option_value = 'ELECTRONIC_WIRE';
		elseif(type = 300)  then
			select lut_name into v_option_value from optionfair_trading.lut where id in (card_type);
		elseif(type = 304)  then
				SET v_option_value = 'SKRILL';
		elseif(type = 306)  then
			SET v_option_value = 'SAFECHARGE';
		elseif(type = 308)  then
			SET v_option_value = 'NETELLER';
		elseif(type = 314)  then
			select lut_name into v_option_value from optionfair_trading.lut where id in (card_type);
		elseif(type = 2665)  then
			SET v_option_value = 'WIRE';
		elseif(type = 2672)  then
				SET v_option_value ='WESTERN_UNION';
		elseif(type = 2674)  then
				SET v_option_value = 'MONEYGRAM';
		elseif(type = 2683)  then
				select lut_name into v_option_value from optionfair_trading.lut where id in (card_type);
		elseif(type = 2688)  then
				SET v_option_value = 'ALGOCHARGE';
		elseif(type = 2689)  then
				SET v_option_value = 'SAFECHARGE';	
		elseif(type = 2693)  then
				select lut_name into v_option_value from optionfair_trading.lut where id in (card_type);	
		end if;	
		
		if(v_option_value is null) then
			SET v_option_value = 'OTHER';	
		end if;
		
        RETURN v_option_value;
    END//

