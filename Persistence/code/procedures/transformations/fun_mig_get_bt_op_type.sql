use fund;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_bt_op_type;
CREATE FUNCTION fun_mig_get_bt_op_type(type int(11), order_amount bigint(20))
    RETURNS VARCHAR(10)
    DETERMINISTIC
    BEGIN
        DECLARE v_txn_type VARCHAR(10);
		if(type = 24)  then
			SET v_txn_type = 'CR';
		elseif(type = 25)  then
			SET v_txn_type = 'DB';
		elseif(type = 26)  then
			SET v_txn_type = 'DB';
		elseif(type = 27)  then
			SET v_txn_type = 'CR';
		elseif(type = 135)  then
			SET v_txn_type = 'CR';
		elseif(type = 148)  then
			SET v_txn_type = 'CR';
		elseif(type = 149)  then
			SET v_txn_type = 'CR';
		elseif(type = 181)  then
			SET v_txn_type = 'DB';
		elseif(type = 196)  then
			SET v_txn_type = 'DB';
		elseif(type = 198)  then
			if(order_amount < 0) then
				SET v_txn_type = 'DB';
			ELSE 
				SET v_txn_type = 'CR';
			end if;		
		elseif(type = 204)  then
			SET v_txn_type = 'DB';
		elseif(type = 230)  then
			if(order_amount < 0) then
				SET v_txn_type = 'DB';
			ELSE 
				SET v_txn_type = 'CR';
			end if;	
		elseif(type = 231)  then
			SET v_txn_type = 'CR';
		elseif(type = 232)  then
			SET v_txn_type = 'CR';
		elseif(type = 235)  then
			SET v_txn_type = 'CR';
		elseif(type = 236)  then
			SET v_txn_type = 'DB';
		elseif(type = 288)  then
			if(order_amount < 0) then
				SET v_txn_type ='DB';
			ELSE 
				SET v_txn_type = 'CR';
			end if;	
		elseif(type = 289)  then
			if(order_amount < 0) then
				SET v_txn_type = 'DB';
			ELSE 
				SET v_txn_type = 'CR';
			end if;	
		elseif(type = 386)  then
				SET v_txn_type = 'DB';
		elseif(type = 2697)  then
				SET v_txn_type = 'DB';
		elseif(type = 2810)  then
				SET v_txn_type = 'CR';			
		end if;	
		
        RETURN v_txn_type;
    END//

