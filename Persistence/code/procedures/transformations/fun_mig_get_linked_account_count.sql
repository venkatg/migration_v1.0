use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_linked_account_count;
CREATE FUNCTION fun_mig_get_linked_account_count(in_source_id INT(10),in_relation_type INT(10))
    RETURNS INT(11)
    DETERMINISTIC
    BEGIN
        DECLARE v_linked_account_count INT(11);

        SET  v_linked_account_count = 0;

        SELECT COUNT(1) INTO v_linked_account_count
        FROM   optionfair_trading.related_account
        WHERE  source_id  = in_source_id
        AND    relation_type = in_relation_type
        GROUP  BY source_id,relation_type;

        RETURN v_linked_account_count;
    END//

