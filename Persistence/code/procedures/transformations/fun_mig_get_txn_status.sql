use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_txn_status;
CREATE FUNCTION fun_mig_get_txn_status(in_status INT(10))
    RETURNS VARCHAR(30)
    DETERMINISTIC
    BEGIN
        DECLARE v_txn_status VARCHAR(30);

        IF (in_status = 98) THEN
                SET v_txn_status = 'TBD';
        ELSEIF (in_status  = 99) THEN
                SET v_txn_status = 'txn_initiated';
        ELSEIF (in_status  = 100) THEN
                SET v_txn_status = 'txn_confirmed_success';
        ELSEIF (in_status in (101,108)) THEN
                SET v_txn_status = 'txn_confirmed_failure';
        ELSE
                SET v_txn_status = NULL;
        END IF;
        RETURN v_txn_status;
    END//

