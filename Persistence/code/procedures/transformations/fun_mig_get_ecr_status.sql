use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_ecr_status;
CREATE FUNCTION fun_mig_get_ecr_status(in_status INT(10))
    RETURNS VARCHAR(30)
    DETERMINISTIC
    BEGIN
        DECLARE v_ecr_status VARCHAR(30);

        IF (in_status in (100,200,300)) THEN
                SET v_ecr_status = 'play';
        ELSEIF (in_status in (400,410,500, 510, 600, 650, 700, 800,900))  THEN
                SET v_ecr_status = 'real';
        ELSE
                SET v_ecr_status = NULL;
        END IF;
        RETURN v_ecr_status;
    END//

