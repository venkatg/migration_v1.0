use cashier;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_processor_name;
CREATE FUNCTION fun_mig_get_processor_name(type int(10))
    RETURNS varchar(100)
    DETERMINISTIC
    BEGIN
        DECLARE v_processor_name VARCHAR(100);
		if(type = 104)  then 				/* Visa */
			SET v_processor_name = 'direct_cc_001';
		elseif(type = 105)  then			/* MasterCard  */
			SET v_processor_name = 'direct_cc_mc_001';
		elseif(type = 106)  then			/* DinersClub */
			SET v_processor_name = 'direct_cc_dinersclub_001';
		elseif(type = 107)  then			/* VisaElectron */
			SET v_processor_name = 'direct_cc_001';
		elseif(type = 160)  then			/* VisaDelta */
			SET v_processor_name = 'direct_cc_001';
		elseif(type = 161)  then			/* VisaCoBrandedCards */
			SET v_processor_name = 'direct_cc_001';
		elseif(type = 162)  then			/* MasterCardDebit */
			SET v_processor_name = 'direct_cc_mc_001';
		elseif(type = 163)  then			/* Maestro */
			SET v_processor_name = 'direct_cc_maestro_001';
		elseif(type = 164)  then			/* JCB */
			SET v_processor_name = 'direct_cc_jcb_001';
		elseif(type = 166)  then			/* Dankort */
				SET v_processor_name = 'direct_cc_dankortb_001';
		elseif(type = 167)  then			/* CartaSi */
			SET v_processor_name = 'direct_cc_cartasi_001';
		elseif(type = 168)  then			/* Laser */
				SET v_processor_name = 'direct_cc_laser_001';	
		end if;	
		
        RETURN v_processor_name;
    END//

