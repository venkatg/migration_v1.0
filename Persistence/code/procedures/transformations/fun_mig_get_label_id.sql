use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_label_id;
CREATE FUNCTION fun_mig_get_label_id(in_label_id INT(10))
    RETURNS VARCHAR(30)
    DETERMINISTIC
    BEGIN
        DECLARE v_label VARCHAR(30);

        IF (in_label_id = 1) THEN
                SET v_label = 'optionfair';
        ELSEIF (in_label_id = 83) THEN
                SET v_label = 'optionlondon';
        ELSEIF (in_label_id = 153) THEN
                SET v_label = 'options500';
        ELSEIF (in_label_id = 166) THEN
                SET v_label = 'marketsunlimited';
        ELSE
                SET v_label = NULL;
        END IF;
        RETURN v_label;
    END//

