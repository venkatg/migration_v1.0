use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_regulation_questionnaire_status;
CREATE FUNCTION fun_mig_get_regulation_questionnaire_status(in_status INT(10))
    RETURNS VARCHAR(30)
    DETERMINISTIC
    BEGIN
        DECLARE v_regulation_questionnaire_status VARCHAR(30);

        IF (in_status = 1671) THEN
                SET v_regulation_questionnaire_status= 'notSubmitted';
        ELSEIF (in_status  in (1672,1673)) THEN
                SET v_regulation_questionnaire_status = 'submitted';
        ELSE
                SET v_regulation_questionnaire_status = 'notSubmitted';
        END IF;
        RETURN v_regulation_questionnaire_status;
    END//

