USE  ecr;
SELECT DATABASE();

delimiter #
DROP function IF EXISTS fun_mig_ecr_aoa_data_map#
create function fun_mig_ecr_aoa_data_map()
  returns INT
  DETERMINISTIC
  begin
  declare resp INT default 0;
  declare arr_str varchar(2048);
  declare v_arr_str varchar(2048);
  declare v_ecr_id bigint(20);
  declare v_regulation_questionnaire_status varchar(30);
  declare v_reg_date datetime;
  declare arr_data varchar(500);
  declare colon_delim varchar(5) default ':';
  declare camma_delim varchar(5) default ',"';
  declare lhn_data varchar(100);
  declare rhs_data varchar(100);
  declare v_agent_id varchar(30) default 'system';
  declare v_employment_status varchar(30);
  declare v_nature_of_business varchar(50);
  declare v_currency varchar(5);
  declare v_estimated_annual_income varchar(200);
  declare v_estimated_annual_income_to_char varchar(200);
  declare v_estimated_annual_income_from bigint(20);
  declare v_estimated_annual_income_to bigint(20);
  declare v_estimated_net_worth varchar(200);
  declare v_estimated_net_worth_from bigint(20);
  declare v_estimated_net_worth_to_char varchar(200);
  declare v_estimated_net_worth_to bigint(20);
  declare v_estimated_total_turnover varchar(200);
  declare v_estimated_total_turnover_from bigint(20);
  declare v_estimated_total_turnover_to_char varchar(200);
  declare v_estimated_total_turnover_to bigint(20);
  declare v_trading_exeperience_char varchar(10);
  declare v_trading_exeperience tinyint(1);
  declare v_investment_knowledge_by_seminar_char varchar(10);
  declare v_investment_knowledge_by_seminar tinyint(1);
  declare v_investment_knowledge_by_work_exp_char varchar(10);
  declare v_investment_knowledge_by_work_exp tinyint(1);
  declare v_usa_reporting_char varchar(10);
  declare v_usa_reporting tinyint(1);
  declare v_custom_field3 varchar(250);
  declare v_custom_field4 varchar(250);
  declare done boolean DEFAULT FALSE;
  declare done1 boolean DEFAULT FALSE;
  declare arr_str1 varchar(2048);
  declare v_arr_str1 varchar(2048);
  declare v_ecr_id1 bigint(20);
  declare v_regulation_questionnaire_status1 varchar(30);
  declare v_reg_date1 datetime;
  declare arr_data1 varchar(500);
  
  DECLARE curs CURSOR FOR select accque.questionnaire_details,mig.c_ecr_id,acc.regulation_questionnaire_status, acc.registration_date from tbl_migration_map mig,optionfair_trading.account_regulation_questionnaire accque, optionfair_trading.account acc where acc.id=mig.c_partner_account_id and accque.account_id = acc.id and accque.questionnaire_details LIKE '%Employment status%';
	 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
	 
  OPEN curs; 
  read_loop: LOOP
  FETCH curs INTO arr_str,v_ecr_id,v_regulation_questionnaire_status ,v_reg_date;
  IF done THEN
      LEAVE read_loop;
  END IF;
  set v_arr_str = arr_str;
  
  WHILE( length( arr_str ) > 0  && arr_str LIKE '%Employment status%') do
	set   arr_str = replace(arr_str,'{','');
	set   arr_str = replace(arr_str,'}','');
	set   v_employment_status = NULL;
	set	  v_nature_of_business = NULL;
	set   v_currency = NULL;
	set   v_estimated_annual_income_from = 0;
	set   v_estimated_annual_income_to = 0;
	set   v_estimated_net_worth_from = 0;
	set   v_estimated_net_worth_to = 0;
	set   v_estimated_total_turnover_from = 0;
	set   v_estimated_total_turnover_to = 0;
	set   v_trading_exeperience = 0;
	set   v_investment_knowledge_by_seminar = 0;
	set   v_investment_knowledge_by_work_exp = 0;
	set   v_usa_reporting = 0;
	set   v_custom_field3 = NULL;
	set   v_custom_field4 = NULL;	
	
  IF( locate( camma_delim, arr_str ) ) then

    set arr_data = ( select substring_index(arr_str, camma_delim, -1) );

    set arr_str = ( select
      replace(arr_str,
        concat(camma_delim,
          substring_index(arr_str, camma_delim, -1)
        )
      ,'')
    );
  ELSE
    set arr_data = arr_str;
    set arr_str = '';
  END IF;
	
  IF arr_data IS NOT NULL  then
	 IF( locate( colon_delim, arr_data ) ) then
	    set rhs_data = (select substring_index(arr_data, colon_delim, -1));
	    set lhn_data = ( select replace(arr_data, concat(colon_delim, substring_index(arr_data, colon_delim, -1)),''));
		set rhs_data = replace(rhs_data,'"','');
		set lhn_data = replace(lhn_data,'"','');
		
		IF lhn_data LIKE '%Employment status%' THEN 
			SET v_employment_status = rhs_data;
		ELSEIF lhn_data LIKE '%Nature of Business%' THEN 
			SET v_nature_of_business = rhs_data;
		ELSEIF lhn_data LIKE '%Estimated Annual Income%' THEN 
			SET v_estimated_annual_income = replace(rhs_data,',','');
			SET v_currency = SUBSTRING_INDEX((SUBSTRING_INDEX(lhn_data,'(', -1)), ')', 1); 
		ELSEIF lhn_data LIKE '%Estimated Net Worth%' THEN 
			SET v_estimated_net_worth = replace(rhs_data,',','');
			SET	v_currency = SUBSTRING_INDEX((SUBSTRING_INDEX(lhn_data, '(', -1)), ')', 1);
		ELSEIF lhn_data LIKE '%Estimated Account Turnover %' THEN 
			SET v_estimated_total_turnover = replace(rhs_data,',','');
			SET v_currency =SUBSTRING_INDEX((SUBSTRING_INDEX(lhn_data, '(', -1)), ')', 1); 
		ELSEIF lhn_data LIKE '%Last 2 years traded binary%' THEN 
			SET v_trading_exeperience_char = rhs_data;
		ELSEIF lhn_data LIKE '%Seminar attendance%' THEN 
			SET v_investment_knowledge_by_seminar_char = rhs_data;
		ELSEIF lhn_data LIKE '%Work experience%' THEN 
			SET v_investment_knowledge_by_work_exp_char = rhs_data;
		ELSEIF lhn_data LIKE '%agreementCheckbox%' THEN 
			SET v_usa_reporting_char = rhs_data;
		ELSEIF lhn_data LIKE '%Frequency of transactions per week%' THEN 
			SET v_custom_field3 = replace(rhs_data,',','');
		ELSEIF lhn_data LIKE '%Average volume size per transaction%' THEN 
			SET v_custom_field4 = replace(rhs_data,',','');
		END IF;
		
		IF (length( v_estimated_annual_income ) > 0 ) THEN
			IF (v_estimated_annual_income like '%Select...%') THEN
				SET v_estimated_annual_income_from = 0;
				SET v_estimated_annual_income_to = 0;
			ELSEIF (v_estimated_annual_income like '%Less than $%') THEN
				SET v_estimated_annual_income_from = 0;
				SET v_estimated_annual_income_to = CONVERT ( TRIM(SUBSTRING_INDEX(v_estimated_annual_income,'$',-1)) , UNSIGNED);
			ELSEIF v_estimated_annual_income like '%or more' THEN
				SET v_estimated_annual_income_from = CONVERT (TRIM(SUBSTRING_INDEX((SUBSTRING_INDEX(v_estimated_annual_income,'$',-1)),' ',1)) , UNSIGNED);
				SET v_estimated_annual_income_to = 0;
			ELSE
				SET v_estimated_annual_income_to_char =  (select SUBSTRING_INDEX(v_estimated_annual_income, '–', -1));
				SET v_estimated_annual_income_from = CONVERT ( TRIM(SUBSTRING_INDEX((replace(v_estimated_annual_income, concat('–', substring_index(v_estimated_annual_income, '–', -1)) ,'')),'$',-1)) , UNSIGNED);
				SET v_estimated_annual_income_to = CONVERT ( TRIM(SUBSTRING_INDEX(v_estimated_annual_income_to_char,'$',-1)) , UNSIGNED);
			END IF;
		END IF;
		
		IF (length( v_estimated_net_worth ) > 0 ) THEN
			IF (v_estimated_net_worth like '%Select...%') THEN
				SET v_estimated_net_worth_from = 0;
				SET v_estimated_net_worth_to = 0;
			ELSEIF (v_estimated_net_worth like '%Less than $%') THEN
				SET v_estimated_net_worth_from = 0;
				SET v_estimated_net_worth_to = CONVERT ( TRIM(SUBSTRING_INDEX(v_estimated_net_worth,'$',-1)) , UNSIGNED);
			ELSEIF v_estimated_net_worth like '%or more' THEN
				SET v_estimated_annual_income_from = CONVERT (TRIM(SUBSTRING_INDEX((SUBSTRING_INDEX(v_estimated_net_worth,'$',-1)),' ',1)) , UNSIGNED);
				SET v_estimated_annual_income_to = 0;
			ELSE
				SET v_estimated_net_worth_to_char = SUBSTRING_INDEX(v_estimated_net_worth, '–', -1);
				SET v_estimated_net_worth_from = CONVERT ( TRIM(SUBSTRING_INDEX((replace(v_estimated_net_worth, concat('–', substring_index(v_estimated_net_worth, '–', -1)) ,'')),'$',-1)) , UNSIGNED);
				SET v_estimated_net_worth_to = CONVERT ( TRIM(SUBSTRING_INDEX(v_estimated_net_worth_to_char,'$',-1)) , UNSIGNED);
			END IF;
		END IF;
		
		IF (length( v_estimated_total_turnover ) > 0 ) THEN
			IF (v_estimated_total_turnover like '%Select...%') THEN
				SET v_estimated_total_turnover_from = 0;
				SET v_estimated_total_turnover_to = 0;
			ELSEIF (v_estimated_total_turnover like '%Less than $%') THEN
				SET v_estimated_total_turnover_from = 0;
				SET v_estimated_total_turnover_to = CONVERT ( TRIM(SUBSTRING_INDEX(v_estimated_total_turnover,'$',-1)) , UNSIGNED);
			ELSEIF v_estimated_total_turnover like '%or more' THEN
				SET v_estimated_annual_income_from = CONVERT (TRIM(SUBSTRING_INDEX((SUBSTRING_INDEX(v_estimated_total_turnover,'$',-1)),' ',1)) , UNSIGNED);
				SET v_estimated_annual_income_to = 0;
			ELSE
				SET v_estimated_total_turnover_to_char =  SUBSTRING_INDEX(v_estimated_total_turnover, '–', -1);
				SET v_estimated_total_turnover_from = CONVERT ( TRIM(SUBSTRING_INDEX((replace(v_estimated_total_turnover, concat('–', substring_index(v_estimated_total_turnover, '–', -1)) ,'')),'$',-1)) , UNSIGNED);
				SET v_estimated_total_turnover_to = CONVERT ( TRIM(SUBSTRING_INDEX(v_estimated_total_turnover_to_char,'$',-1)) , UNSIGNED);
			END IF;
			
		END IF;
		
		IF (length( v_trading_exeperience_char ) > 0  ) THEN
			IF(replace(v_trading_exeperience_char,'"','') = 'YES') THEN
				SET v_trading_exeperience = 1;
			ELSE 
				SET v_trading_exeperience = 0;
			END IF;
		END IF;
		
		IF (length( v_investment_knowledge_by_seminar_char ) > 0  ) THEN
			IF(replace(v_investment_knowledge_by_seminar_char,'"','') = 'YES') THEN
				SET v_investment_knowledge_by_seminar = 1;
			ELSE 
				SET v_investment_knowledge_by_seminar = 0;
			END IF;
		END IF;
		
		IF (length( v_investment_knowledge_by_work_exp_char ) > 0  ) THEN
			IF(replace(v_investment_knowledge_by_work_exp_char,'"','') = 'YES') THEN
				SET v_investment_knowledge_by_work_exp = 1;
			ELSE 
				SET v_investment_knowledge_by_work_exp = 0;
			END IF;
		END IF;
		
		IF (length( v_usa_reporting_char ) > 0  ) THEN
			IF(replace(v_usa_reporting_char,'"','') = 'YES' || 'true') THEN
				SET v_usa_reporting = 1;
			ELSE 
				SET v_usa_reporting = 0;
			END IF;
		END IF;
		
	  END IF;
  END IF;

 END WHILE;
		IF(v_arr_str LIKE '%Employment status%') THEN
			INSERT INTO tbl_ecr_aoa (c_ecr_id,c_aoa_status,c_date_of_submission,c_date_of_updation,c_agent_id,c_employment_status,c_nature_of_business,c_currency,c_estimated_annual_income_from,c_estimated_annual_income_to,c_estimated_net_worth_from,c_estimated_net_worth_to,c_estimated_total_turnover_from,c_estimated_total_turnover_to,c_trading_exeperience,c_investment_knowledge_by_seminar,c_investment_knowledge_by_work_exp,c_usa_reporting,c_custom_field3,c_custom_field4,c_tax_id,c_reason_for_establishment_business_relationship,c_nature_of_transactions) SELECT v_ecr_id, fun_mig_get_regulation_questionnaire_status(v_regulation_questionnaire_status), v_reg_date,v_reg_date,v_agent_id, v_employment_status, v_nature_of_business, v_currency, IFNULL(v_estimated_annual_income_from,0), IFNULL(v_estimated_annual_income_to,0),IFNULL(v_estimated_net_worth_from,0), IFNULL(v_estimated_net_worth_to,0), IFNULL(v_estimated_total_turnover_from,0), IFNULL(v_estimated_total_turnover_to,0),IFNULL(v_trading_exeperience,0), IFNULL(v_investment_knowledge_by_seminar,0), IFNULL(v_investment_knowledge_by_work_exp,0), v_usa_reporting, v_custom_field3, v_custom_field4,"","","";
			
			INSERT INTO tbl_ecr_aoa_log (c_ecr_id,c_aoa_status,c_date_of_submission,c_date_of_updation,c_agent_id,c_employment_status,c_nature_of_business,c_currency,c_estimated_annual_income_from,c_estimated_annual_income_to,c_estimated_net_worth_from,c_estimated_net_worth_to,c_estimated_total_turnover_from,c_estimated_total_turnover_to,c_trading_exeperience,c_investment_knowledge_by_seminar,c_investment_knowledge_by_work_exp,c_usa_reporting,c_custom_field3,c_custom_field4,c_tax_id,c_reason_for_establishment_business_relationship,c_nature_of_transactions) SELECT v_ecr_id, fun_mig_get_regulation_questionnaire_status(v_regulation_questionnaire_status), v_reg_date,v_reg_date,v_agent_id, v_employment_status, v_nature_of_business, v_currency, IFNULL(v_estimated_annual_income_from,0), IFNULL(v_estimated_annual_income_to,0),IFNULL(v_estimated_net_worth_from,0), IFNULL(v_estimated_net_worth_to,0), IFNULL(v_estimated_total_turnover_from,0), IFNULL(v_estimated_total_turnover_to,0),IFNULL(v_trading_exeperience,0), IFNULL(v_investment_knowledge_by_seminar,0), IFNULL(v_investment_knowledge_by_work_exp,0), v_usa_reporting, v_custom_field3, v_custom_field4,"","","";
			set resp = resp + 1;	
			
		END IF;
		
		END LOOP;	
		CLOSE curs;	
		
	begin
	DECLARE curs1 CURSOR FOR select accque.questionnaire_details,mig.c_ecr_id,acc.regulation_questionnaire_status, acc.registration_date from tbl_migration_map mig,optionfair_trading.account_regulation_questionnaire accque, optionfair_trading.account acc where acc.id=mig.c_partner_account_id and accque.account_id = acc.id and accque.questionnaire_details NOT LIKE '%Employment status%';
	 
	 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;
	 
	OPEN curs1; 
	read_loop1: LOOP
	FETCH curs1 INTO arr_str1,v_ecr_id1,v_regulation_questionnaire_status1 ,v_reg_date1;
	IF done1 THEN
		LEAVE read_loop1;
	END IF;
	set v_arr_str1 = arr_str1;
  
	IF(v_arr_str1 NOT LIKE '%Employment status%') THEN
			INSERT INTO tbl_ecr_aoa (c_ecr_id,c_aoa_status,c_date_of_submission,c_date_of_updation,c_agent_id,c_employment_status,c_nature_of_business,c_currency,c_estimated_annual_income_from,c_estimated_annual_income_to,c_estimated_net_worth_from,c_estimated_net_worth_to,c_estimated_total_turnover_from,c_estimated_total_turnover_to,c_trading_exeperience,c_investment_knowledge_by_seminar,c_investment_knowledge_by_work_exp,c_usa_reporting,c_custom_field3,c_custom_field4,c_tax_id,c_reason_for_establishment_business_relationship,c_nature_of_transactions) 
			SELECT v_ecr_id1, fun_mig_get_regulation_questionnaire_status(v_regulation_questionnaire_status1), v_reg_date1,v_reg_date1,'system', '', '', '', '', '','', '', '', '','', '', '', '', arr_str1, '',"","","";
			
			INSERT INTO tbl_ecr_aoa_log (c_ecr_id,c_aoa_status,c_date_of_submission,c_date_of_updation,c_agent_id,c_employment_status,c_nature_of_business,c_currency,c_estimated_annual_income_from,c_estimated_annual_income_to,c_estimated_net_worth_from,c_estimated_net_worth_to,c_estimated_total_turnover_from,c_estimated_total_turnover_to,c_trading_exeperience,c_investment_knowledge_by_seminar,c_investment_knowledge_by_work_exp,c_usa_reporting,c_custom_field3,c_custom_field4,c_tax_id,c_reason_for_establishment_business_relationship,c_nature_of_transactions) 
			SELECT v_ecr_id1, fun_mig_get_regulation_questionnaire_status(v_regulation_questionnaire_status1), v_reg_date1,v_reg_date1,'system', '', '', '', '', '','', '', '', '','', '', '', '', arr_str1, '',"","","";
			set resp = resp + 1;	
			
		END IF;
		
		END LOOP;	
		CLOSE curs1;
	end;

return resp;

 END #

delimiter ;

