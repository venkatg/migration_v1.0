use csm;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_csm_note_status;
CREATE FUNCTION fun_mig_get_csm_note_status(in_status_id INT(11))
    RETURNS varchar(50)
    DETERMINISTIC
    BEGIN
        DECLARE v_in_name varchar(50);
		DECLARE out_name varchar(50);
		
		select name into v_in_name from optionfair_trading.bo_lut where id = in_status_id; 

        IF (v_in_name = 'Open') THEN
                SET out_name = 'closed';
		ELSEIF (v_in_name = 'InProcess') THEN
                SET out_name = 'closed';
        ELSEIF (v_in_name = 'Close') THEN
                SET out_name = 'closed';
        END IF;
        RETURN out_name;
    END//

