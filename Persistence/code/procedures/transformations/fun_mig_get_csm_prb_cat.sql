use csm;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_csm_prb_cat;
CREATE FUNCTION fun_mig_get_csm_prb_cat(in_subject_id INT(10))
    RETURNS VARCHAR(500)
    DETERMINISTIC
    BEGIN
        DECLARE v_in_name varchar(500);
		DECLARE out_name varchar(500);
		
		select name into v_in_name from optionfair_trading.bo_note_subject where subject_id = in_subject_id and brand_id <>3; 

        IF (v_in_name = 'CustomerComplaint') THEN
                SET out_name = 'CustomerComplaint';
        ELSEIF (v_in_name = 'ManualBonusToCustomer') THEN
                SET out_name = 'Bonus';
		ELSEIF (v_in_name = 'RequestForCashOut') THEN
                SET out_name = 'CashOut';         
        ELSEIF (v_in_name = 'RequestForTechnicalSupport') THEN
                SET out_name = 'TechnicalSupport';
	    ELSEIF (v_in_name = 'RequestForTradingSupport') THEN
                SET out_name = 'ProductSupport';
		ELSEIF (v_in_name = 'RequestForDepositSupport') THEN
                SET out_name = 'Deposit';
		ELSEIF (v_in_name = 'General') THEN
                SET out_name = 'General';
		ELSEIF (v_in_name = 'Deposit') THEN
                SET out_name = 'Deposit';
		ELSEIF (v_in_name = 'FirstCall') THEN
                SET out_name = 'Verification';
		ELSEIF (v_in_name = 'FDPCall') THEN
                SET out_name = 'Verification';
		ELSEIF (v_in_name = 'RetentionCall') THEN
                SET out_name = 'Retention';
		ELSEIF (v_in_name = 'ReAssignedLead') THEN
                SET out_name = 'General';
		ELSEIF (v_in_name = 'NewLead') THEN
                SET out_name = 'Sales';
		ELSEIF (v_in_name = 'NewAccount') THEN
                SET out_name = 'Sales';
		ELSEIF (v_in_name = 'FirstDeposit') THEN
                SET out_name = 'Deposit';
		ELSEIF (v_in_name = 'RetentionDeposit') THEN
                SET out_name = 'Deposit';
		ELSEIF (v_in_name = 'FailedDeposit') THEN
                SET out_name = 'Deposit';
		ELSEIF (v_in_name = 'WithdrawalRequest') THEN
                SET out_name = 'CashOut';
		ELSEIF (v_in_name = 'AccountInformationUpdated') THEN
                SET out_name = 'General';
		ELSEIF (v_in_name = 'NoMoreMoney') THEN
                SET out_name = 'Deposit';
		ELSEIF (v_in_name = 'AlmostNoMoreMoney') THEN
                SET out_name = 'Deposit';
        END IF;
        RETURN out_name;
    END//

             