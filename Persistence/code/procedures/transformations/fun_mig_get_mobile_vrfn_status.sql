use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_mobile_vrfn_status;
CREATE FUNCTION fun_mig_get_mobile_vrfn_status(in_phone_verified bit(1),in_phone_2_verified bit(1))
    RETURNS VARCHAR(30)
    DETERMINISTIC
    BEGIN
        DECLARE v_mobile_vrfn_status VARCHAR(30);

        IF (in_phone_verified  = 1 ||  in_phone_2_verified = 1) THEN
                SET v_mobile_vrfn_status= 'vrfn_verified';
        ELSE
                SET v_mobile_vrfn_status = 'vrfn_new';
        END IF;
        RETURN v_mobile_vrfn_status;
    END//

