use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_category_data;
CREATE FUNCTION fun_mig_get_category_data(status INT(10), blockList bit(1), blocked bit(1), can_login bit(1))
    RETURNS VARCHAR(100)
    DETERMINISTIC
    BEGIN
        DECLARE v_category VARCHAR(100);
		if(blockList = 1 || blocked = 1 || can_login = 0)  then
			SET status = 700;
		end if;

               SET v_category = fun_mig_get_ecr_category(status);
        RETURN v_category;
    END//

