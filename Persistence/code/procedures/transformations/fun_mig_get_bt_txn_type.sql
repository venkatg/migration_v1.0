use fund;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_bt_txn_type;
CREATE FUNCTION fun_mig_get_bt_txn_type(type int(11), order_amount bigint(20))
    RETURNS int(11)
    DETERMINISTIC
    BEGIN
        DECLARE v_txn_type int(11);
		if(type = 24)  then
			SET v_txn_type = 1;
		elseif(type = 25)  then
			SET v_txn_type = 2;
		elseif(type = 26)  then
			SET v_txn_type = 46;
		elseif(type = 27)  then
			SET v_txn_type = 47;
		elseif(type = 135)  then
			SET v_txn_type = 19;
		elseif(type = 148)  then
			SET v_txn_type = 48;
		elseif(type = 149)  then
			SET v_txn_type = 36;
		elseif(type = 181)  then
			SET v_txn_type = 37;
		elseif(type = 196)  then
			SET v_txn_type = 35;
		elseif(type = 198)  then
			if(order_amount < 0) then
				SET v_txn_type = 50;
			ELSE 
				SET v_txn_type = 49;
			end if;	
		elseif(type = 204)  then
			SET v_txn_type = 40;
		elseif(type = 230)  then
			if(order_amount < 0) then
				SET v_txn_type = 52;
			ELSE 
				SET v_txn_type = 51;
			end if;	
		elseif(type = 231)  then
			SET v_txn_type = 19;
		elseif(type = 232)  then
			SET v_txn_type = 53;
		elseif(type = 235)  then
			SET v_txn_type = 57;
		elseif(type = 236)  then
			SET v_txn_type = 54;
		elseif(type = 288)  then
			if(order_amount < 0) then
				SET v_txn_type =4;
			ELSE 
				SET v_txn_type = 3;
			end if;	
		elseif(type = 289)  then
			if(order_amount < 0) then
				SET v_txn_type = 4;
			ELSE 
				SET v_txn_type = 3;
			end if;	
		elseif(type = 386)  then
				SET v_txn_type = 58;
		elseif(type = 2697)  then
				SET v_txn_type = 55;
		elseif(type = 2810)  then
				SET v_txn_type = 56;			
		end if;	
		
        RETURN v_txn_type;
    END//

