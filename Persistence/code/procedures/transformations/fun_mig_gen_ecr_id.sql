use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_gen_ecr_id;
CREATE FUNCTION fun_mig_gen_ecr_id(i INT, indate DATETIME, minrange INT, maxrange INT)
    RETURNS BIGINT(20)
    DETERMINISTIC
    BEGIN
        DECLARE v_ecr_id BIGINT(20);
        DECLARE  v_date_part VARCHAR(18);
        DECLARE v_no_part VARCHAR(5); 
        DECLARE v_new_date DATETIME ;
    
        SET v_new_date = DATE_ADD(indate, INTERVAL i minute);  

	SELECT  CAST((minrange + (RAND() * maxrange)) AS UNSIGNED)  INTO v_no_part; 
        SELECT  CAST((UNIX_TIMESTAMP(v_new_date) * 1000) AS UNSIGNED) INTO v_date_part;
        SELECT  CAST(CONCAT(v_no_part,v_date_part)  AS UNSIGNED) INTO v_ecr_id;
        RETURN v_ecr_id;
        
    END//

