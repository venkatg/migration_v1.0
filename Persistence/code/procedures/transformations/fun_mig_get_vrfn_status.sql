use ecr;
SELECT database();
DELIMITER //
DROP FUNCTION  IF EXISTS fun_mig_get_vrfn_status;
CREATE FUNCTION fun_mig_get_vrfn_status(in_status INT(10))
    RETURNS VARCHAR(30)
    DETERMINISTIC
    BEGIN
        DECLARE v_vrfn_status VARCHAR(30);

        IF (in_status in (100,200,300,400,410,600,650,700,800,900)) THEN
                SET v_vrfn_status = 'vrfn_pending';
        ELSEIF (in_status in (500, 510))  THEN
                SET v_vrfn_status = 'vrfn_success';
        ELSE
                SET v_vrfn_status = NULL;
        END IF;
        RETURN v_vrfn_status;
    END//

