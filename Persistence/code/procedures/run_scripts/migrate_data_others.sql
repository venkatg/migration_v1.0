use messaging

select now() starttime;

INSERT INTO tbl_message_subscription_data(c_ecr_id,c_subscribe_to_newsletter,c_created_time,c_created_by,c_update_time,c_updated_by)
SELECT mig.c_ecr_id, acc.subscribe_to_newsletter,acc.registration_date ,'system',acc.last_record_update,'system'
FROM optionfair_trading.account acc join ecr.tbl_migration_map  mig on acc.id=mig.c_partner_account_id ;

select row_count() tbl_message_subscription_data;

use csm

 
INSERT INTO tbl_mst_alert (c_reference_id,c_sub_reference_id,c_alert_type,c_problem_cat,c_problem_sub_cat,c_complaint_details,c_ecr_id,c_partner_id,c_label_id,c_root_cause,c_queue_name,c_followup_time,c_reported_time,c_status,c_comments,c_resolve_comments,c_created_by,c_updated_by,c_updated_time) 
SELECT note.id, 'cs_contact', 'contact', fun_mig_get_csm_prb_cat(note.note_subject_id), fun_mig_get_csm_prb_sub_cat(note.note_subject_id), fun_mig_get_csm_prb_cat(note.note_subject_id), mig.c_ecr_id,'optionfair','optionfair', fun_mig_get_csm_prb_sub_cat(note.note_subject_id), 'cs_contact', note.follow_up_on, note.open_date, fun_mig_get_csm_note_status(note.note_status_id), note.comment, note.change_status_comment, 'system', usr.username, note.update_date
FROM optionfair_trading.bo_note note join ecr.tbl_migration_map mig on note.object_id=mig.c_partner_account_id left join optionfair_trading.bo_user usr on usr.id=note.assigned_to where note.note_object=73; 

select row_count() tbl_mst_alert;



select now() endtime;