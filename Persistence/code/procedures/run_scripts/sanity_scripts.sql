use ecr
select now() starttime;

/*
1.	Balance report
*/

SELECT * FROM (
    SELECT 'pw_id', 'of_id', 'total_balnce', 'balance_for_withdrawal', 'currency_symbol'
    UNION ALL
    (
        select a.c_external_id as pw_id, a.c_partner_account_id as of_id,  b.c_realcash_balance + b.c_crp as total_balnce, b.c_realcash_balance as balance_for_withdrawal, b.c_ecr_crncy from fund.tbl_real_fund b join ecr.tbl_migration_map a on a.c_ecr_id=b.c_ecr_id
    )
) resulting_set
INTO OUTFILE '/tmp/acc_balance.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

/*
2.	Bonus report
*/

SELECT * FROM (
    SELECT 'of_id', 'bonus_amount', 'wagering requirment'
    UNION ALL
    (
        select mig.c_partner_account_id,multiplier.c_offer_value bonus_amount,multiplier.c_offer_value*multiplier.c_issue_multiplier as wager_amount
from ecr.tbl_migration_map mig 
JOIN bonus.tbl_user_bonus_multiplier_info multiplier ON multiplier.c_ecr_id = mig.c_ecr_id
where multiplier.c_bonus_id = 201605311
    )
) resulting_set
INTO OUTFILE '/tmp/acc_bonus.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

/*
3.	This is to generate our id(external ecr id) and their account id and status of user file
*/

SELECT * FROM (
    SELECT 'pw_id', 'of_id', 'status'
    UNION ALL
    (
        select a.c_external_id as pw_id, b.c_partner_account_id as of_id, a.c_ecr_status as status from tbl_ecr a join tbl_migration_map b on a.c_ecr_id=b.c_ecr_id 
    )
) resulting_set
INTO OUTFILE '/tmp/acc_migration.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

/*
4.	Summary of status, deposit allowed and can trade(id verified)
*/

select count(*) from ecr.tbl_ecr where c_ecr_status ='real';

select count(*) from ecr.tbl_ecr where c_ecr_status ='play';

--can't deposit count
select count(*) from cashier.tbl_deposit_withdrawl_flags where c_deposit_allowed=0;
357771
-- can't trade count
select count(*) from ecr.tbl_ecr_identity_vrfn where c_vrfn_status <> 'vrfn_verified';
366718
-- can trade and deposit count
select count(*) from ecr.tbl_ecr_identity_vrfn ecr, cashier.tbl_deposit_withdrawl_flags  flag where ecr.c_ecr_id = flag.c_ecr_id and ecr.c_vrfn_status = 'vrfn_verified' and flag.c_deposit_allowed=1;
18589

/*
5.	summary of AOA status

select count(*) from tbl_ecr_aoa aoa, tbl_migration_map mig where aoa.c_aoa_status = 'submitted' and aoa.c_ecr_id = mig.c_ecr_id;
--28492
*/

SELECT (select count(*) from  tbl_migration_map mig) AS mig_cnt,
    (select count(*) from tbl_ecr_aoa aoa, tbl_migration_map mig where aoa.c_aoa_status = 'submitted' and aoa.c_ecr_id = mig.c_ecr_id) AS aoa_submitted_cnt,
    (SELECT mig_cnt - aoa_submitted_cnt) AS aoa_not_submit_cnt;

+---------+-------------------+--------------------+
| mig_cnt | aoa_submitted_cnt | aoa_not_submit_cnt |
+---------+-------------------+--------------------+
|  385306 |             28610 |             356696 |
+---------+-------------------+--------------------+

	
	SELECT * FROM (
    SELECT 'pw_id', 'of_id', 'FNS_status'
    UNION ALL
    (
        select mig.c_external_id as pw_id, mig.c_partner_account_id as of_id, aoa.c_aoa_status as FNS_status  from tbl_ecr_aoa aoa join tbl_migration_map mig on aoa.c_ecr_id=mig.c_ecr_id
    )
) resulting_set
INTO OUTFILE '/tmp/acc_FNS_status.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';


/*
6.	count of closed position real users
*/
select count(*) from fund.tbl_real_fund_txn where c_txn_type=47 and c_ecr_id in (select c_ecr_id from tbl_migration_map);
--6370908
select count(*) from fund.tbl_real_fund_txn b join ecr.tbl_ecr a on a.c_ecr_id=b.c_ecr_id where a.c_ecr_status='real' and b.c_txn_type=47 and b.c_ecr_id in (select c_ecr_id from tbl_migration_map);
--

/*
7. card token 
*/
SELECT * FROM (
    SELECT 'pw_id', 'of_id', 'card_token'
    UNION ALL
    (
        select b.c_external_id as pw_id, b.c_partner_account_id as of_id, a.c_card_token as card_token  from cashier.tbl_card_token_details a join tbl_migration_map b on a.c_ecr_id=b.c_ecr_id
    )
) resulting_set
INTO OUTFILE '/tmp/acc_card_token.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';


/*
all the positions. Fields are: tf_position_id, ibid_position_id, open_datetime, asset_name, inveseted_amount(in base customer's currency), balance(in base customer's currency).
*/
SELECT * FROM (
    SELECT 'tf_id', 'ibid_id', 'tf_position_id', 'ibid_position_id', 'ledger_time', 'credit_or_debit', 'inveseted_amount', 'balance'
    UNION ALL
    (
        select mig.c_partner_account_id as tf_id, mig.c_external_id as ibid_id, mig.c_position_id as tf_position_id, fund.c_txn_id as ibid_position_id, fund.c_start_time as ledger_time, fund.c_op_type as credit_or_debit, fund.c_amount_in_ecr_ccy as inveseted_amount, fund.c_current_balance as balance from fund.tbl_real_fund_txn fund join vendor.tbl_migration_vendor_map mig on mig.c_fund_txn_id = fund.c_txn_id
    )
) resulting_set
INTO OUTFILE '/tmp/acc_led_data.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';


SELECT * FROM (
    SELECT 'tf_position_id'
    UNION ALL
    (
        select distinct mig.c_position_id as tf_position_id from vendor.tbl_migration_vendor_map mig
    )
) resulting_set
INTO OUTFILE '/tmp/acc_position_id.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';



SELECT * FROM (
    SELECT 'token', 'proc_ref_id', 'ext_id_8', 'currency' ,'card_first_part','card_last_part'
    UNION ALL
    (
        select abc.token, abc.ref_id, abc.ext_id, abc.curr,abc.card_first_part,abc.card_last_part from (select token.c_card_token as token , dep.c_processor_ref_id as ref_id, SUBSTRING(mig.c_external_id,1,8) as ext_id, dep.c_currency as curr, instru.c_first_part as card_first_part, instru.c_last_part as card_last_part,dep.c_instrument_id,dep.c_ecr_id,dep.c_processor_name from tbl_cashier_deposit dep join tbl_card_token_details token on dep.c_ecr_id = token.c_ecr_id and dep.c_instrument_id = token.c_instrument_id and dep.c_txn_status= 'txn_confirmed_success' join ecr.tbl_migration_map mig on mig.c_ecr_id = token.c_ecr_id join tbl_instrument instru on instru.c_id= dep.c_instrument_id order by dep.c_created_time desc) abc group by c_instrument_id,c_ecr_id,c_processor_name
    )
) resulting_set
INTO OUTFILE '/tmp/token.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT * FROM (
    SELECT 'cust_txn_id', 'proc_ref_id', 'amount', 'currency', 'ext_id_8' , 'auth_code', 'token','processor','card_first_part','card_last_part', 'email'
    UNION ALL
    (
        select abc.cust_txn_id, abc.ref_id, abc.amount, abc.curr, abc.ext_id, abc.auth_code, abc.token, case when abc.processor_name like '%SafeCharge%' then 'SAFECHARGE' when abc.processor_name like '%Inatec%' then 'INATEC' end, abc.card_first_part, abc.card_last_part ,abc.email  from (select dep.c_id as cust_txn_id, token.c_card_token as token , dep.c_processor_ref_id as ref_id, SUBSTRING(mig.c_external_id,1,8) as ext_id, dep.c_external_error_code as auth_code, dep.c_currency as curr, dep.c_instrument_id,dep.c_ecr_id,dep.c_processor_name as processor_name, dep.c_initial_amount-dep.c_refund_amount as amount,instru.c_first_part as card_first_part, instru.c_last_part as card_last_part, tecr.c_email_id as email from tbl_cashier_deposit dep join tbl_card_token_details token on dep.c_ecr_id = token.c_ecr_id and dep.c_instrument_id = token.c_instrument_id and dep.c_txn_status= 'txn_confirmed_success' and (dep.c_initial_amount-dep.c_refund_amount) >0 and dep.	c_processor_name like 'direct_mg_%' join ecr.tbl_migration_map mig on mig.c_ecr_id = token.c_ecr_id join tbl_instrument instru on instru.c_id= dep.c_instrument_id join ecr.tbl_ecr tecr on mig.c_ecr_id = tecr.c_ecr_id) abc
    )
) resulting_set
INTO OUTFILE '/tmp/token_data.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT * FROM (
    SELECT 'cust_txn_id', 'proc_ref_id', 'amount', 'currency', 'ext_id_8' , 'auth_code', 'token','processor','card_first_part','card_last_part', 'email'
    UNION ALL
    (
        select abc.cust_txn_id, abc.ref_id, abc.amount, abc.curr, abc.ext_id, abc.auth_code, abc.token, case when abc.processor_name like '%SafeCharge%' then 'SAFECHARGE' when abc.processor_name like '%Inatec%' then 'INATEC' end, abc.card_first_part, abc.card_last_part ,abc.email from (select dep.c_id as cust_txn_id, bank.card_token as token , dep.c_processor_ref_id as ref_id, SUBSTRING(mig.c_external_id,1,8) as ext_id, dep.c_external_error_code as auth_code, dep.c_currency as curr, dep.c_ecr_id,dep.c_processor_name as processor_name, dep.c_initial_amount-dep.c_refund_amount as amount,bank.card_num_start as card_first_part, bank.card_num as card_last_part, tecr.c_email_id as email from optionfair_trading.banking bank join ecr.tbl_migration_map mig on mig.c_partner_account_id = cast(bank.account_id as char(100)) and bank.type=102 join tbl_cashier_deposit dep on  dep.c_ecr_id=mig.c_ecr_id and bank.external_transaction_id = dep.c_processor_ref_id COLLATE utf8_unicode_ci and bank.external_auth_code = dep.c_external_error_code  COLLATE utf8_unicode_ci and dep.c_txn_status= 'txn_confirmed_success' and (dep.c_initial_amount-dep.c_refund_amount) >0 and dep.c_processor_name like 'direct_mg_%' join ecr.tbl_ecr tecr on mig.c_ecr_id = tecr.c_ecr_id) abc
    )
) resulting_set
INTO OUTFILE '/tmp/OF_token_data.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT * FROM (
    SELECT 'cust_txn_id', 'proc_ref_id', 'amount', 'currency', 'ext_id_8' , 'auth_code', 'token','processor','card_first_part','card_last_part', 'email'
    UNION ALL
    (
        select abc.cust_txn_id, abc.ref_id, abc.amount, abc.curr, abc.ext_id, abc.auth_code, abc.token, case when abc.processor_name like '%SafeCharge%' then 'SAFECHARGE' when abc.processor_name like '%Inatec%' then 'INATEC' end, abc.card_first_part, abc.card_last_part ,abc.email from (select dep.c_id as cust_txn_id, bank.card_token as token , dep.c_processor_ref_id as ref_id, SUBSTRING(mig.c_external_id,1,8) as ext_id, dep.c_external_error_code as auth_code, dep.c_currency as curr, dep.c_ecr_id,dep.c_processor_name as processor_name, dep.c_initial_amount-dep.c_refund_amount as amount,bank.card_num_start as card_first_part, bank.card_num as card_last_part, tecr.c_email_id as email from optionfair_trading.banking bank join ecr.tbl_migration_map mig on mig.c_partner_account_id = bank.account_id and bank.type=102 and bank.status in (97,98,100) join tbl_cashier_deposit dep on  dep.c_ecr_id=mig.c_ecr_id and (bank.external_transaction_id = dep.c_processor_ref_id COLLATE utf8_unicode_ci or (bank.external_transaction_id is null and dep.c_processor_ref_id is null)) and  (bank.external_auth_code = dep.c_external_error_code  COLLATE utf8_unicode_ci or (bank.external_auth_code is null and dep.c_external_error_code is null)) and bank.request_time = dep.c_created_time and dep.c_txn_status= 'txn_confirmed_success' and (dep.c_initial_amount-dep.c_refund_amount) >0 and dep.c_processor_name like 'direct_mg_%' join ecr.tbl_ecr tecr on mig.c_ecr_id = tecr.c_ecr_id) abc where abc.processor_name like '%SafeCharge%' or abc.processor_name like '%Inatec%'
    )
) resulting_set
INTO OUTFILE '/tmp/epay_OF_data.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';


 SELECT * FROM (
    SELECT 'cust_txn_id', 'proc_ref_id', 'amount', 'currency', 'ext_id_8' , 'auth_code', 'token','processor','card_first_part','card_last_part', 'email'
    UNION ALL
    (
        select abc.cust_txn_id, abc.ref_id, abc.amount, abc.curr, abc.ext_id, abc.auth_code, abc.token, case when abc.processor_name like '%SafeCharge%' then 'SAFECHARGE' when abc.processor_name like '%Inatec%' then 'INATEC' end, abc.card_first_part, abc.card_last_part ,abc.email from (select dep.c_id as cust_txn_id, bank.card_token as token , dep.c_processor_ref_id as ref_id, SUBSTRING(mig.c_external_id,1,8) as ext_id, dep.c_external_error_code as auth_code, dep.c_currency as curr, dep.c_ecr_id,dep.c_processor_name as processor_name, dep.c_initial_amount-dep.c_refund_amount as amount,bank.card_num_start as card_first_part, bank.card_num as card_last_part, tecr.c_email_id as email from optionfair_trading.banking bank join ecr.tbl_migration_map mig on mig.c_partner_account_id = bank.account_id and bank.type=102 and bank.status in (97,98,100) join tbl_cashier_deposit dep on  dep.c_ecr_id=mig.c_ecr_id and (bank.external_transaction_id = dep.c_processor_ref_id COLLATE utf8_unicode_ci or (bank.external_transaction_id is null and dep.c_processor_ref_id is null)) and  (bank.external_auth_code = dep.c_external_error_code  COLLATE utf8_unicode_ci or (bank.external_auth_code is null and dep.c_external_error_code is null)) and bank.request_time = dep.c_created_time and dep.c_txn_status= 'txn_confirmed_success' and (dep.c_initial_amount-dep.c_refund_amount) =0 and dep.c_processor_name like 'direct_mg_%'  and dep.c_update_time > DATE_SUB(NOW(),INTERVAL 1 YEAR) join ecr.tbl_ecr tecr on mig.c_ecr_id = tecr.c_ecr_id) abc where abc.processor_name like '%SafeCharge%' or abc.processor_name like '%Inatec%'
    )
) resulting_set
INTO OUTFILE '/tmp/epay_OF_data_redeposit.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';


SELECT * FROM (
    SELECT 'ext_id_8',   'auth_code', 'proc_ref_id'
    UNION ALL
    (
        select  abc.ext_id, abc.auth_code,abc.ref_id from (select dep.c_processor_ref_id as ref_id, SUBSTRING(mig.c_external_id,1,8) as ext_id, dep.c_external_error_code as auth_code,dep.c_instrument_id,dep.c_ecr_id,dep.c_processor_name from tbl_cashier_deposit dep join tbl_card_token_details token on dep.c_ecr_id = token.c_ecr_id and dep.c_instrument_id = token.c_instrument_id and dep.c_txn_status= 'txn_confirmed_success' and  dep.c_external_error_code is not null and dep.c_external_error_code <> '' join ecr.tbl_migration_map mig on mig.c_ecr_id = token.c_ecr_id order by dep.c_created_time desc) abc group by c_instrument_id,c_ecr_id,c_processor_name
    )
) resulting_set
INTO OUTFILE '/tmp/authcode.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

SELECT * FROM (
    SELECT 'of_id'
    UNION ALL
    (
        select distinct c_ecr_id from tbl_ecr_auth where c_login_time > '2016-05-02 00:00:00'
    )
) resulting_set
INTO OUTFILE '/tmp/login_ext_id.csv'
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n';

/*
To generate player balance for a particular month
*/
select * from (select c_ecr_id, c_realcash_balance,c_ecr_crncy from tbl_real_fund_log where c_created_time  < '2016-07-01' and c_ecr_id in (select distinct c_ecr_id from tbl_real_fund_log where c_created_time  > '2016-07-01') and c_realcash_balance is not null order by c_created_time desc)abc group by c_ecr_id
union all
select c_ecr_id, c_realcash_balance,c_ecr_crncy from tbl_real_fund where c_updated_time < '2016-07-01' and c_created_time<'2016-07-01' order by c_realcash_balance desc
INTO OUTFILE '/tmp/june_player_balances.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

