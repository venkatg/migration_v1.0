use ecr;
select now() starttime;
/*  
call proc_migrate_map_data_v1dot0();
*/




INSERT INTO tbl_ecr (c_ecr_id,c_external_id ,c_email_id,c_partner_id,c_registration_status ,c_ecr_status,c_passwd_hash ,c_signup_time ,c_updated_by,c_updated_time,c_language,c_affiliate_id) 
select mig.c_ecr_id, mig.c_external_id, lower(acc.email),mig.c_partner_id,'ecr_regn_completed',fun_mig_get_ecr_status(acc.status),acc.password ,acc.registration_date,'system',acc.last_record_update, lan.lut_name ,cast(acc.affiliate_id as char) 
from optionfair_trading.account acc join tbl_migration_map  mig on acc.id=mig.c_partner_account_id join optionfair_trading.lut lan on acc.site_language=lan.id and lan.lut_type_id=14 
group by email having count(email)=1;

select row_count() tbl_ecr; 

INSERT INTO tbl_ecr (c_ecr_id,c_external_id ,c_email_id,c_partner_id,c_registration_status ,c_ecr_status,c_passwd_hash ,c_signup_time ,c_updated_by,c_updated_time,c_language,c_affiliate_id) 
select mig.c_ecr_id, mig.c_external_id, lower(concat(concat(acc.id,'_'),acc.email)),mig.c_partner_id,'ecr_regn_completed',fun_mig_get_ecr_status(acc.status),acc.password ,acc.registration_date,'system',acc.last_record_update, lan.lut_name ,cast(acc.affiliate_id as char) 
from optionfair_trading.account acc join tbl_migration_map  mig on acc.id=mig.c_partner_account_id join optionfair_trading.lut lan on acc.site_language=lan.id and lan.lut_type_id=14 
join (select email from optionfair_trading.account group by email having count(email) >1) abc on acc.email=abc.email;

select row_count() tbl_ecr_1; 

INSERT INTO tbl_ecr_log (c_ecr_id,c_external_id ,c_email_id,c_partner_id,c_registration_status ,c_ecr_status,c_passwd_hash ,c_signup_time ,c_created_by,c_created_time,c_language,c_affiliate_id) select mig.c_ecr_id, mig.c_external_id, lower(acc.email),mig.c_partner_id,'ecr_regn_completed',fun_mig_get_ecr_status(acc.status),acc.password,acc.registration_date,'system',acc.last_record_update,lan.lut_name,cast(acc.affiliate_id as char) 
from optionfair_trading.account_history acc join tbl_migration_map  mig on acc.account_id=mig.c_partner_account_id join optionfair_trading.lut lan on acc.site_language=lan.id and lan.lut_type_id=14 
group by email having count(email)=1;

select row_count() tbl_ecr_log; 

INSERT INTO tbl_ecr_log (c_ecr_id,c_external_id ,c_email_id,c_partner_id,c_registration_status ,c_ecr_status,c_passwd_hash ,c_signup_time ,c_created_by,c_created_time,c_language,c_affiliate_id) 
select mig.c_ecr_id, mig.c_external_id, lower(concat(concat(acc.account_id,'_'),acc.email)),mig.c_partner_id,'ecr_regn_completed',fun_mig_get_ecr_status(acc.status),acc.password ,acc.registration_date,'system',acc.last_record_update, lan.lut_name ,cast(acc.affiliate_id as char) 
from optionfair_trading.account_history acc join tbl_migration_map  mig on acc.account_id=mig.c_partner_account_id join optionfair_trading.lut lan on acc.site_language=lan.id and lan.lut_type_id=14 
join (select email from optionfair_trading.account_history group by email having count(email) >1) abc on acc.email=abc.email;

select row_count() tbl_ecr_log_1; 

INSERT INTO tbl_ecr_profile_list (c_ecr_id,c_title ,c_fname,c_lname,c_created_time ,c_dob,c_address1 ,c_address2 ,c_city,c_state,c_country_code,c_zip,c_phone_isd_code,c_phone_number,c_mobile_isd_code,c_mobile_number,c_agent_name,c_partner_id,c_label_id) select mig.c_ecr_id, fun_mig_get_title_id(acc.title_id),acc.first_name,acc.last_name,acc.registration_date,acc.birth_date,cast(acc.street_number as char),acc.street,acc.city,acc.state,acc.country_id,acc.zip_postal_code,acc.phone_prefix_country_id,CAST(acc.phone_number AS UNSIGNED),acc.phone_2_prefix_country_id,CAST(acc.phone_number_2 AS UNSIGNED),'system', 'optionfair', fun_mig_get_label_id(acc.brand_id) from optionfair_trading.account_history acc join tbl_migration_map  mig on acc.account_id=mig.c_partner_account_id ;

select row_count() tbl_ecr_profile_list_acchist; 


INSERT INTO tbl_ecr_profile_list (c_ecr_id,c_title ,c_fname,c_lname,c_created_time ,c_dob,c_address1 ,c_address2 ,c_city,c_state,c_country_code,c_zip,c_phone_isd_code,c_phone_number,c_mobile_isd_code,c_mobile_number,c_agent_name,c_partner_id,c_label_id) select mig.c_ecr_id, fun_mig_get_title_id(acc.title_id),acc.first_name,acc.last_name,acc.registration_date,acc.birth_date,cast(acc.street_number as char),acc.street,acc.city,acc.state,acc.country_id,acc.zip_postal_code,acc.phone_prefix_country_id,CAST(acc.phone_number AS UNSIGNED),acc.phone_2_prefix_country_id,CAST(acc.phone_number_2 AS UNSIGNED),'system', 'optionfair', fun_mig_get_label_id(acc.brand_id) from optionfair_trading.account acc join tbl_migration_map  mig on acc.id=mig.c_partner_account_id ;


select row_count() tbl_ecr_profile_list_acc; 


INSERT INTO tbl_ecr_profile (c_ecr_id,c_title ,c_fname,c_lname,c_created_time ,c_dob,c_address1 ,c_address2 ,c_city,c_state,c_country_code,c_zip,c_phone_isd_code,c_phone_number,c_mobile_isd_code,c_mobile_number,c_agent_name,c_partner_id,c_label_id,c_profile_list_id) select * from (select mig.c_ecr_id, fun_mig_get_title_id(acc.title_id),acc.first_name,acc.last_name,acc.registration_date,acc.birth_date,cast(acc.street_number as char),acc.street,acc.city,acc.state,acc.country_id,acc.zip_postal_code,acc.phone_prefix_country_id,CAST(acc.phone_number AS UNSIGNED),acc.phone_2_prefix_country_id,CAST(acc.phone_number_2 AS UNSIGNED),'system', 'optionfair', fun_mig_get_label_id(acc.brand_id),plist.c_id from optionfair_trading.account acc join tbl_migration_map mig on acc.id=mig.c_partner_account_id join tbl_ecr_profile_list plist on plist.c_ecr_id=mig.c_ecr_id order by plist.c_ecr_id, plist.c_id desc) abc Group by c_ecr_id;

select row_count() tbl_ecr_profile; 

INSERT INTO tbl_ecr_screen_name ( c_ecr_id, c_screen_name,c_liquidity_pool,c_comments,c_updated_by,c_updated_time,c_screen_name_upper) SELECT mig.c_ecr_id,LOWER(acc.user_name),'riverplay',NULL,'SYSTEM',CURRENT_TIMESTAMP,UPPER(acc.user_name) FROM tbl_migration_map mig join optionfair_trading.account acc ON mig.c_partner_account_id = acc.id;

select row_count() tbl_ecr_screen_name; 

INSERT INTO tbl_ecr_screen_name_log ( c_ecr_id, c_screen_name,c_liquidity_pool,c_comments,c_created_by,c_created_time,c_screen_name_upper) SELECT mig.c_ecr_id,LOWER(acc.user_name),'riverplay',NULL,'SYSTEM',acc.registration_date,UPPER(acc.user_name) FROM tbl_migration_map mig join optionfair_trading.account_history acc ON mig.c_partner_account_id = acc.account_id;

select row_count() tbl_ecr_screen_name_log; 


INSERT INTO tbl_ecr_auth (c_ecr_id,c_partner_id,c_label_id,c_product_id,c_channel,c_sub_channel, c_os_browser_type,c_auth_type, c_auth_status,c_updated_by,c_updated_time,c_ip) SELECT mig.c_ecr_id, 'optionfair','optionfair', 'BINARY_TRADE', 'web','html','chrome','email','SUCCESS','SYSTEM', acc.registration_date, CONVERT(acc.registration_ip,CHAR(50)) FROM tbl_migration_map mig join optionfair_trading.account acc on mig.c_partner_account_id = acc.id;

select row_count() tbl_ecr_auth; 

INSERT INTO tbl_ecr_auth_log (c_ecr_id,c_partner_id,c_label_id,c_product_id,c_channel,c_sub_channel, c_os_browser_type,c_auth_type, c_auth_status,c_created_by,c_created_time,c_ip) SELECT mig.c_ecr_id, 'optionfair','optionfair', 'BINARY_TRADE', 'web','html','chrome','email','SUCCESS','SYSTEM',acc.registration_date, CONVERT(acc.registration_ip,CHAR(50)) FROM tbl_migration_map mig join optionfair_trading.account_history acc on mig.c_partner_account_id = acc.account_id;

select row_count() tbl_ecr_auth_log;

INSERT INTO tbl_ecr_conversion_info (c_ecr_id,c_partner_id,c_label_id,c_product_id,c_channel,c_sub_channel,c_os_browser_type,c_conversion_kind,c_conversion_time,c_created_by,c_created_time) SELECT mig.c_ecr_id,'optionfair','optionfair','BINARY_TRADE','web','html','chrome','CS',acc.FTD_date, 'system', acc.registration_date FROM tbl_migration_map mig join optionfair_trading.account acc on mig.c_partner_account_id = acc.id;

select row_count() tbl_ecr_conversion_info;

delete from tbl_ecr_conversion_info where c_ecr_id in (select ecr.c_ecr_id from tbl_ecr ecr, tbl_migration_map mig where ecr.c_ecr_status <> 'real' and ecr.c_ecr_id = mig.c_ecr_id );
--348268

INSERT INTO tbl_ecr_secret_question (c_ecr_id,c_secret_question_1, c_secret_answer_1,c_secret_question_2,c_secret_answer_2, c_created_by, c_created_time,c_updated_by,c_updated_time) SELECT mig.c_ecr_id,acc.security_question,acc.security_answer,NULL,NULL,'system', acc.registration_date, 'system', acc.registration_date from tbl_migration_map mig join optionfair_trading.account acc on mig.c_partner_account_id = acc.id;

select row_count() tbl_ecr_secret_question;


INSERT INTO tbl_ecr_secret_question_log (c_master_id,c_ecr_id,c_secret_question_1, c_secret_answer_1,c_secret_question_2,c_secret_answer_2, c_created_by, c_created_time) SELECT ecr.c_id,mig.c_ecr_id,acc.security_question,acc.security_answer,NULL,NULL,'SYSTEM',acc.registration_date FROM tbl_migration_map mig join optionfair_trading.account_history acc on mig.c_partner_account_id = acc.account_id 
join tbl_ecr_secret_question ecr on mig.c_ecr_id = ecr.c_ecr_id;


select row_count() tbl_ecr_secret_question_log;


INSERT INTO tbl_ecr_category (c_ecr_id,c_category,c_change_source,c_creation_date,c_created_by,c_last_modified_date) SELECT mig.c_ecr_id,fun_mig_get_category_data(acc.status, acc.is_black_list, acc.is_blocked, acc.can_login),'system', acc.registration_date,'system', acc.registration_date from optionfair_trading.account acc join tbl_migration_map mig on acc.id=mig.c_partner_account_id ;
 
select row_count() tbl_ecr_category;



INSERT INTO tbl_ecr_category_log (c_master_id,c_ecr_id,c_category,c_change_source,c_modified_by,c_last_modified_date) SELECT ecr.c_id,mig.c_ecr_id,fun_mig_get_category_data(acc.status, acc.is_black_list, acc.is_blocked, acc.can_login),'system', 'system', acc.registration_date from optionfair_trading.account_history acc join tbl_migration_map mig on acc.account_id=mig.c_partner_account_id 
join tbl_ecr_category ecr on mig.c_ecr_id = ecr.c_ecr_id;


select row_count() tbl_ecr_category_log;

INSERT INTO tbl_ecr_flags(c_ecr_id,c_withdrawl_allowed,c_test_user,c_created_by,c_created_time,c_comments) select mig.c_ecr_id, 0, 1, 'system',now(),'Migration' where mig.c_account_type_id in (10,132);

INSERT INTO tbl_ecr_sales_status (c_ecr_id,c_sales_status_id,c_sales_status,c_updated_by,c_updated_time,c_created_by,c_created_time,c_sales_rep) SELECT mig.c_ecr_id,acc.sale_status,NULL,'system', acc.registration_date,'system', acc.registration_date,acc.sales_rep FROM tbl_migration_map mig join optionfair_trading.account acc on mig.c_partner_account_id = acc.id;

select row_count() tbl_ecr_sales_status;


INSERT INTO tbl_ecr_signup_info ( c_ecr_id, c_partner_id, c_label_id, c_product_id, c_channel, c_sub_channel, c_os_browser_type, c_created_by , c_created_time)
SELECT c_ecr_id,'optionfair','optionfair','BINARY_TRADE','web','html','chrome','system', acc.registration_date
FROM tbl_migration_map mig join optionfair_trading.account acc on mig.c_partner_account_id = acc.id;

select row_count() tbl_ecr_signup_info;


INSERT INTO tbl_ecr_address_vrfn (c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_tracker_id,c_created_time)
SELECT mig.c_ecr_id,'vrfn_init','optionfair','optionfair', plist.c_profile_list_id, plist.c_created_time
FROM tbl_migration_map mig join tbl_ecr_profile plist on plist.c_ecr_id=mig.c_ecr_id;

select row_count() tbl_ecr_address_vrfn;

INSERT INTO tbl_ecr_address_vrfn_log (c_ecr_id, c_source, c_agent_name,c_vrfn_status,c_partner_id,c_label_id,c_processor_response_code,c_tracker_id,c_created_time,c_comments) SELECT c_ecr_id, c_source, c_agent_name,c_vrfn_status,c_partner_id,c_label_id,c_processor_response_code,c_tracker_id,c_created_time,c_comments FROM tbl_ecr_address_vrfn;

select row_count() tbl_ecr_address_vrfn_log;

INSERT INTO tbl_ecr_auth_label_info ( c_ecr_id, c_auth_label_id, c_updated_by, c_updated_time) 
SELECT mig.c_ecr_id,'optionfair','system', acc.registration_date
FROM tbl_migration_map mig join optionfair_trading.account acc ON mig.c_partner_account_id = acc.id;

select row_count() tbl_ecr_auth_label_info;

INSERT INTO tbl_ecr_auth_label_info_log (c_ecr_id,c_auth_label_id, c_last_auth_time, c_created_by,c_created_time)
SELECT c_ecr_id,c_auth_label_id, c_last_auth_time, c_updated_by,c_updated_time
FROM tbl_ecr_auth_label_info ;

select row_count() tbl_ecr_auth_label_info_log;

INSERT INTO tbl_ecr_identity_vrfn (c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_tracker_id,c_created_time, c_comments)
SELECT mig.c_ecr_id,CASE WHEN acc.status IN (500,510) THEN 'vrfn_verified' ELSE 'vrfn_init' END ,'optionfair','optionfair', plist.c_profile_list_id, acc.registration_date, 'Migration'
FROM  tbl_migration_map mig join tbl_ecr_profile plist on plist.c_ecr_id=mig.c_ecr_id
join optionfair_trading.account acc ON mig.c_partner_account_id = acc.id;

select row_count() tbl_ecr_identity_vrfn;

INSERT INTO  tbl_ecr_identity_vrfn_log
(c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_tracker_id ,c_created_time)
SELECT c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_tracker_id ,c_created_time
FROM tbl_ecr_identity_vrfn where c_partner_id='optionfair';

select row_count() tbl_ecr_identity_vrfn_log;

INSERT INTO tbl_ecr_age_vrfn(c_ecr_id, c_vrfn_status, c_partner_id, c_label_id, c_tracker_id, c_created_time,c_comments)
SELECT mig.c_ecr_id, 'vrfn_init','optionfair','optionfair', plist.c_profile_list_id, plist.c_created_time,'Migration'
FROM tbl_migration_map mig join tbl_ecr_profile plist on plist.c_ecr_id=mig.c_ecr_id;

select row_count() tbl_ecr_age_vrfn;

INSERT INTO tbl_ecr_age_vrfn_log(c_ecr_id, c_vrfn_status, c_partner_id, c_label_id, c_tracker_id, c_created_time,c_comments)
SELECT mig.c_ecr_id, 'vrfn_init','optionfair','optionfair', plist.c_profile_list_id, plist.c_created_time,'Migration'
FROM tbl_migration_map mig join tbl_ecr_profile plist on plist.c_ecr_id=mig.c_ecr_id;

select row_count() tbl_ecr_age_vrfn_log;

INSERT INTO tbl_ecr_labels (c_ecr_id,c_partner_id,c_label_id,c_type,c_created_by,c_created_time, c_updated_time,c_updated_by,c_comments) 
SELECT c_ecr_id,'optionfair','optionfair','signup','system',acc.registration_date,acc.registration_date, 'system','Migration'
FROM tbl_migration_map mig join optionfair_trading.account acc ON mig.c_partner_account_id = acc.id;

select row_count() tbl_ecr_labels;

INSERT INTO tbl_ecr_labels_log ( c_ecr_id,c_partner_id,c_label_id,c_type,c_comments,c_created_by,c_created_time)
SELECT c_ecr_id,c_partner_id,c_label_id,c_type,c_comments,c_created_by,c_created_time 
FROM tbl_ecr_labels where c_partner_id='optionfair';

select row_count() tbl_ecr_labels_log;


INSERT INTO tbl_ecr_mobile_details  (c_ecr_id,c_partner_id,c_isd_code,c_mobile_num,c_updated_by,c_updated_time)
SELECT mig.c_ecr_id,'optionfair',acc.phone_prefix_country_id,acc.phone_number,'system',acc.registration_date
FROM tbl_migration_map mig,optionfair_trading.account acc
where mig.c_partner_account_id = cast(acc.id as char(100));

select row_count() tbl_ecr_mobile_details;

INSERT INTO tbl_ecr_mobile_vrfn (c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_tracker_id,c_created_time,c_comments)
SELECT mig.c_ecr_id,fun_mig_get_mobile_vrfn_status(acc.phone_verified,acc.phone_2_verified),'optionfair','optionfair',plist.c_profile_list_id, acc.registration_date, 'Migration'
FROM  tbl_migration_map mig join tbl_ecr_profile plist on plist.c_ecr_id=mig.c_ecr_id join optionfair_trading.account acc on acc.id=mig.c_partner_account_id;

select row_count() tbl_ecr_mobile_vrfn;

INSERT INTO tbl_ecr_mobile_vrfn_log (c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_tracker_id, c_created_time,c_comments)
SELECT c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_tracker_id, c_created_time,c_comments
FROM tbl_ecr_mobile_vrfn;

select row_count() tbl_ecr_mobile_vrfn_log;

/*--aoa and aoa_log table*/
select fun_mig_ecr_aoa_data_map();


INSERT INTO tbl_ecr_account_verified_data(c_ecr_id,c_email_verified,c_allow_contact,c_terms_conditions_approved,c_created_time,c_created_by,c_update_time)
SELECT mig.c_ecr_id,acc.email_verified, acc.allow_contact,acc.terms_conditions_approved,acc.registration_date,'system',acc.registration_date
FROM  tbl_migration_map mig join optionfair_trading.account acc on acc.id=mig.c_partner_account_id;

select row_count() tbl_ecr_account_verified_data;

INSERT INTO tbl_ecr_linked_accounts_logs ( c_ecr_id, c_linked_ecr_id, c_linked_type, c_linked_data1, c_linked_data2, c_linked_data3, c_created_time)
SELECT mig1.c_ecr_id,rel_acc.dest_id,fun_mig_get_linked_account_type(rel_acc.relation_type),rel_data.account1_first_name, rel_data.account1_last_name, rel_data.account2_first_name, now()
FROM optionfair_trading.related_account rel_acc 
join tbl_migration_map mig1 on rel_acc.source_id = mig1.c_partner_account_id 
join optionfair_trading.related_account_data rel_data on rel_data.account1_id = rel_acc.source_id and rel_data.account2_id = rel_acc.dest_id and rel_data.relation_type = rel_acc.relation_type;

select row_count() tbl_ecr_linked_accounts_logs;

update tbl_ecr_linked_accounts_logs  set c_linked_ecr_id = (select c_ecr_id from tbl_migration_map mig where mig.c_partner_account_id=cast(c_linked_ecr_id as char(100)));
   
select row_count() tbl_ecr_linked_accounts_logs_update;

INSERT INTO tbl_ecr_linked_accounts (c_ecr_id,c_linked_ecr_id,c_linked_type,c_linked_data1,c_linked_data2,c_linked_data3,c_count, c_created_time, c_updated_time)
SELECT log_tbl.c_ecr_id, log_tbl.c_linked_ecr_id, log_tbl.c_linked_type, log_tbl.c_linked_data1, log_tbl.c_linked_data2, log_tbl.c_linked_data3, count(*), now(), now()
FROM tbl_ecr_linked_accounts_logs log_tbl
group by log_tbl.c_ecr_id, log_tbl.c_linked_ecr_id, log_tbl.c_linked_type;

select row_count() tbl_ecr_linked_accounts;

select now() endtime;