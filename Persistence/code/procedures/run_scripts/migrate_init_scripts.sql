USE ecr;

select now() starttime;

/*LOAD data infile '/tmp/account_balances.csv' into table ecr.tbl_migration_temp
fields terminated by ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS; 

select row_count() loadfilecnt;
select now() loadfile;
*/

INSERT INTO ecr.tbl_migration_temp(c_partner_account_id, c_acc_balance, c_bal_for_withdrawal, c_currency) select c_partner_account_id, c_acc_balance, c_bal_for_withdrawal, c_currency from optionfair_trading.tbl_migration_temp;

select row_count() tbl_migration_temp;
select now() tbl_migration_temp;

call proc_migrate_map_data_v1dot0();

select row_count() mapdata;
select now() mapdata;

use vendor

call proc_migrate_map_cashier_data_v1dot1(50000,20060404);

select now() cashiertime;

call proc_migrate_vendor_temp_data_v1dot1(99999,'20100401');

select now() vendortemp;

call proc_migrate_map_vendor_data_v1dot1(99999,'20100401');

select now() endtime;