USE vendor;

select now() starttime;

INSERT INTO tbl_vendor_ecr_id_mapping
(c_ecr_id,c_ecr_external_id,c_vendor_id,c_ecr_vendor_id,c_username,c_mail_id,c_updated_by,c_updated_time)
SELECT mig.c_ecr_id,mig.c_external_id,'tech_financials',CAST(acc.id AS CHAR),acc.user_name,acc.email,'system', acc.registration_date
FROM ecr.tbl_migration_map mig 
JOIN optionfair_trading.account acc ON acc.id = mig.c_partner_account_id;


select row_count() tbl_vendor_ecr_id_mapping;

INSERT INTO tbl_vendor_ecr_id_mapping_log
(c_id,c_ecr_id,c_ecr_external_id,c_vendor_id,c_ecr_vendor_id,c_username,c_mail_id,c_updated_by,c_updated_time)
SELECT c_id,c_ecr_id,c_ecr_external_id,c_vendor_id,c_ecr_vendor_id,c_username,c_mail_id,c_updated_by,c_updated_time
FROM tbl_vendor_ecr_id_mapping;


select row_count() tbl_vendor_ecr_id_mapping_log;

INSERT INTO tbl_vendor_fund_txn
(c_ecr_id,c_txn_id,c_fund_ref_id,c_fund_session_id,c_partner_id,c_label_id,c_product_id,c_sub_product_id,c_vendor_id,c_channel,c_sub_channel,c_txn_type,c_vendor_txn_status,c_fund_txn_status,c_sub_funds_order,c_txn_amount,c_txn_ccy,c_ecrid_ccy ,c_cash_bal_before,c_cash_bal_after,c_start_time,c_updated_by,c_updated_time,c_game_category, c_game_type, c_inhouse_amount,c_inhouse_ccy)
SELECT mig.c_ecr_id,mig.c_vendor_txn_id,mig.c_fund_txn_id,mig.c_fund_session_id,'optionfair','optionfair','BINARY_TRADE','tech_financials','tech_financials', 'web','html',CASE ledger.type WHEN 26 THEN 'BT_BUYIN' WHEN 27 THEN 'BT_LEAVE_TABLE' WHEN 148 THEN 'BT_CANCEL_BUY_IN' END, 'SUCCESS','SUCCESS','bonus_realcash',ABS(ledger.order_amount),currency.symbol,currency.symbol,IF (ledger.account_balance-ledger.order_amount < 0 ,0, ledger.account_balance - ledger.order_amount),IF (ledger.account_balance < 0 ,0, ledger.account_balance),ledger.ledger_time,'system', ledger.ledger_time, mst.c_game_cat_id, mst.c_game_type_id, ABS(ledger.order_amount * currency.Normalized_to_usd), 'USD'
FROM  tbl_migration_vendor_map mig 
JOIN  optionfair_trading.finaincial_ledger ledger ON mig.c_txn_id_at_vendor   = ledger.id
JOIN  optionfair_trading.account acc              ON mig.c_partner_account_id = acc.id
JOIN  optionfair_trading.currency currency        ON acc.currency_id          = currency.currency_id
join optionfair_trading.position pos on mig.c_position_id = pos.id and mig.c_partner_account_id = pos.account_id
join optionfair_trading.lut lut on lut.id = pos.position_type
join vendor.tbl_vendor_games_mapping_mst mst on mst.c_game_id = lower(lut.lut_name) and c_vendor_id = 'tech_financials' 
WHERE ledger.type IN (26,27,148);


select row_count() tbl_vendor_fund_txn;

 
INSERT INTO tbl_vendor_fund_txn_log
(c_ecr_id,c_txn_id,c_fund_ref_id,c_fund_session_id,c_partner_id,c_label_id,c_product_id,c_sub_product_id,c_vendor_id,c_channel,c_sub_channel,c_txn_type,c_vendor_txn_status,c_fund_txn_status,c_sub_funds_order,c_txn_amount,c_txn_ccy,c_ecrid_ccy ,c_cash_bal_before,c_cash_bal_after,c_created_by,c_created_time,c_game_category,c_game_type,c_inhouse_amount,c_inhouse_ccy)
SELECT c_ecr_id,c_txn_id,c_fund_ref_id,c_fund_session_id,c_partner_id,c_label_id,c_product_id,c_sub_product_id,c_vendor_id,c_channel,c_sub_channel,c_txn_type,c_vendor_txn_status,c_fund_txn_status,c_sub_funds_order,c_txn_amount,c_txn_ccy,c_ecrid_ccy ,c_cash_bal_before,c_cash_bal_after,c_updated_by,c_updated_time,c_game_category, c_game_type, c_inhouse_amount,c_inhouse_ccy
FROM  tbl_vendor_fund_txn;

select row_count() tbl_vendor_fund_txn_log;


INSERT INTO tbl_vendor_txn_mapping 
(c_ecr_id,c_vendor_id,c_txn_id,c_txn_id_at_vendor,c_session_id_at_vendor,c_fund_session_id,c_updated_by, c_updated_time, c_game_id)
SELECT mig.c_ecr_id, 'tech_financials', mig.c_vendor_txn_id, mig.c_txn_id_at_vendor, mig.c_position_id, mig.c_fund_session_id, 'system', pos.order_time, lower(lut.lut_name)
FROM tbl_migration_vendor_map mig join optionfair_trading.position pos on mig.c_position_id = pos.id and mig.c_partner_account_id = pos.account_id
join optionfair_trading.lut lut on lut.id = pos.position_type;

select row_count() tbl_vendor_txn_mapping;

INSERT INTO tbl_vendor_txn_mapping_log 
(c_ecr_id,c_vendor_id,c_txn_id,c_txn_id_at_vendor,c_session_id_at_vendor,c_fund_session_id,c_created_by, c_created_time, c_game_id)
SELECT c_ecr_id,c_vendor_id,c_txn_id,c_txn_id_at_vendor,c_session_id_at_vendor,c_fund_session_id,c_updated_by, c_updated_time, c_game_id
FROM tbl_vendor_txn_mapping;

select row_count() tbl_vendor_txn_mapping_log;


select now() endtime;