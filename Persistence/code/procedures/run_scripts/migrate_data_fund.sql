use fund

select now() starttime;

INSERT INTO tbl_real_fund (c_ecr_id,c_external_id,c_ecr_status,c_realcash_balance, c_rrp, c_crp, c_drp, c_wrp, c_ecr_crncy,c_cum_rounding_diff,c_updated_by,c_created_time,c_updated_time) 
SELECT mig.c_ecr_id ,mig.c_external_id ,ecr.fun_mig_get_ecr_status(acc.status) ,if(tmpmig.c_bal_for_withdrawal < 0, 0, tmpmig.c_bal_for_withdrawal), 0, 0, 0, 0, ifnull(ccy.symbol,'USD'),0,'SYSTEM' ,acc.registration_date,acc.registration_date
FROM ecr.tbl_migration_map mig join optionfair_trading.account acc on mig.c_partner_account_id = acc.id 
join ecr.tbl_migration_temp tmpmig on tmpmig.c_partner_account_id = acc.id and tmpmig.c_partner_account_id = mig.c_partner_account_id  
 left join optionfair_trading.currency ccy on acc.currency_id = ccy.currency_id;
 
 select row_count() tbl_real_fund;
 
INSERT INTO tbl_real_fund (c_ecr_id,c_external_id,c_ecr_status,c_realcash_balance, c_rrp, c_crp, c_drp, c_wrp, c_ecr_crncy,c_cum_rounding_diff,c_updated_by,c_created_time,c_updated_time) 
SELECT mig.c_ecr_id ,mig.c_external_id ,ecr.fun_mig_get_ecr_status(acc.status) ,0, 0, 0, 0, 0, ifnull(ccy.symbol,'USD'),0,'SYSTEM' ,acc.registration_date,acc.registration_date
FROM ecr.tbl_migration_map mig join optionfair_trading.account acc on mig.c_partner_account_id = acc.id 
 left join optionfair_trading.currency ccy on acc.currency_id = ccy.currency_id  
where mig.c_ecr_id not in (select c_ecr_id from tbl_real_fund);

select row_count() tbl_real_fund_update;

INSERT INTO tbl_real_fund_log (c_ecr_id,c_external_id,c_ecr_status,c_realcash_balance, c_ecr_crncy,c_cum_rounding_diff,c_created_by,c_created_time) 
SELECT c_ecr_id,c_external_id,c_ecr_status,c_realcash_balance, c_ecr_crncy,c_cum_rounding_diff,c_updated_by,c_updated_time FROM tbl_real_fund;

select row_count() tbl_real_fund_log;


INSERT INTO tbl_real_fund_session (c_ecr_id, c_session_id, c_session_status, c_init_txn_id, c_start_time,c_updated_by,c_updated_time, c_session_rounding_diff) 
SELECT mig.c_ecr_id, mig.c_fund_session_id, 1,mig.c_fund_txn_id, acc.registration_date,'system', acc.registration_date,0 
FROM vendor.tbl_migration_cashier_map mig join optionfair_trading.account acc on mig.c_partner_account_id = acc.id ;

select row_count() tbl_real_fund_session_bank;

INSERT INTO tbl_real_fund_session (c_ecr_id, c_session_id, c_session_status, c_init_txn_id, c_start_time,c_updated_by,c_updated_time, c_session_rounding_diff) 
SELECT mig.c_ecr_id, mig.c_fund_session_id, IF (COUNT(fin.position_id) = 1,  0,  1),mig.c_fund_txn_id,fin.ledger_time, 'system',fin.ledger_time, 0  FROM vendor.tbl_migration_vendor_map mig left join optionfair_trading.finaincial_ledger fin on fin.position_id =  mig.c_position_id and fin.id = mig.c_txn_id_at_vendor and fin.account_id = mig.c_partner_account_id where fin.type in (26,27) group by fin.position_id;

select row_count() tbl_real_fund_session_ledger;

INSERT INTO tbl_real_fund_session_log (c_ecr_id, c_session_id, c_session_status, c_init_txn_id, c_start_time,c_created_by, c_created_time,c_session_rounding_diff) 
SELECT c_ecr_id, c_session_id, c_session_status, c_init_txn_id, c_start_time,c_updated_by,c_updated_time, c_session_rounding_diff
FROM tbl_real_fund_session;

select row_count() tbl_real_fund_session_log;

INSERT INTO tbl_real_fund_txn(c_ecr_id, c_txn_id, c_session_id, c_partner_id,c_label_id,c_product_id,c_sub_product_id,c_channel,c_sub_channel,c_os_browser_type,c_initiator_ref_id,c_txn_type, c_op_type,c_agent_name,c_txn_status,c_sub_funds_order,c_start_time,c_updated_by,c_updated_time,c_prev_balance, c_current_balance, c_amount_in_ecr_ccy, c_ecr_ccy) 
SELECT mig.c_ecr_id, mig.c_fund_txn_id, mig.c_fund_session_id, 'optionfair', 'optionfair', 'BINARY_TRADE', 'tech_financials', 'web', 'html', 'chrome', mig.c_vendor_txn_id, fun_mig_get_bt_txn_type(fin.type, fin.order_amount), fun_mig_get_bt_op_type(fin.type, fin.order_amount), 'system', 'SUCCESS', '', fin.ledger_time, 'system', fin.ledger_time, IF(((fin.account_balance - fin.order_amount ) < 0 ) , 0 ,(fin.account_balance - fin.order_amount)), IF((fin.account_balance < 0 ) , 0 ,fin.account_balance), ABS(fin.order_amount), ccy.symbol
FROM vendor.tbl_migration_vendor_map mig join optionfair_trading.account acc on mig.c_partner_account_id = acc.id left join optionfair_trading.finaincial_ledger fin on fin.id = mig.c_txn_id_at_vendor and fin.account_id = mig.c_partner_account_id join optionfair_trading.currency ccy on acc.currency_id = ccy.currency_id;

select row_count() tbl_real_fund_txn_ledger;


INSERT INTO tbl_real_fund_txn_log(c_ecr_id, c_txn_id, c_session_id, c_partner_id,c_label_id,c_product_id,c_sub_product_id,c_channel,c_sub_channel,c_os_browser_type,c_initiator_ref_id,c_txn_type, c_op_type,c_agent_name,c_txn_status,c_sub_funds_order,c_created_by,c_created_time,c_prev_balance, c_current_balance, c_amount_in_ecr_ccy, c_ecr_ccy) 
SELECT c_ecr_id, c_txn_id, c_session_id, c_partner_id,c_label_id,c_product_id,c_sub_product_id,c_channel,c_sub_channel,c_os_browser_type,c_initiator_ref_id,c_txn_type, c_op_type,c_agent_name,c_txn_status,c_sub_funds_order,c_updated_by,c_updated_time,c_prev_balance, c_current_balance, c_amount_in_ecr_ccy, c_ecr_ccy
FROM tbl_real_fund_txn where c_partner_id='optionfair';

select row_count() tbl_real_fund_txn_log;


INSERT INTO tbl_realcash_sub_fund_txn(c_ecr_id,	c_sub_txn_id, c_fund_txn_id, c_txn_amount, c_txn_ccy, c_ecr_ccy, c_amount_in_house_ccy, c_amount_in_ecr_ccy, c_prev_balance, c_current_balance, c_txn_type, c_op_type, c_exchange_snapshot_id, c_session_id, c_start_time, c_updated_by,c_updated_time)
SELECT mig.c_ecr_id, mig.c_sub_txn_id, mig.c_fund_txn_id, ABS(fin.order_amount), ccy.symbol, ccy.symbol, ABS(fin.order_amount), ABS(fin.order_amount), IF(((fin.account_balance - fin.order_amount ) < 0 ) , 0 ,(fin.account_balance - fin.order_amount)), IF((fin.account_balance < 0 ) , 0 ,fin.account_balance), fun_mig_get_bt_txn_type(fin.type, fin.order_amount), fun_mig_get_bt_op_type(fin.type, fin.order_amount), 0, mig.c_fund_session_id, fin.ledger_time, 'system', fin.ledger_time
FROM vendor.tbl_migration_vendor_map mig join optionfair_trading.account acc on mig.c_partner_account_id = acc.id left join optionfair_trading.finaincial_ledger fin on fin.id = mig.c_txn_id_at_vendor and fin.account_id = mig.c_partner_account_id join optionfair_trading.currency ccy on acc.currency_id = ccy.currency_id;

select row_count() tbl_realcash_sub_fund_txn_ledger;

INSERT INTO tbl_fund_deposit_txn(c_txn_id, c_ecr_id, c_session_id, c_initiator_ref_id, c_amount, c_currency, c_start_time, c_updated_by,c_updated_time)
SELECT mig.c_fund_txn_id, mig.c_ecr_id, mig.c_fund_session_id, mig.c_cashier_txn_id, ban.amount, ccy.symbol, ban.request_time, 'system',ban.update_time
FROM vendor.tbl_migration_cashier_map mig join optionfair_trading.banking ban on ban.account_id = mig.c_partner_account_id and mig.c_txn_id_at_bank = ban.id join optionfair_trading.currency ccy on ban.currency_id = ccy.currency_id where ban.type = 102;

select row_count() tbl_fund_deposit_txn;

INSERT INTO tbl_fund_deposit_txn_log(c_txn_id, c_ecr_id, c_session_id, c_initiator_ref_id, c_amount, c_currency, c_created_time,c_created_by)
SELECT c_txn_id, c_ecr_id, c_session_id, c_initiator_ref_id, c_amount, c_currency, c_updated_time,c_updated_by
FROM tbl_fund_deposit_txn;

select row_count() tbl_fund_deposit_txn_log;

INSERT INTO tbl_fund_conversion_info(c_ecr_id, c_txn_id, c_partner_id, c_label_id, c_product_id, c_channel, c_sub_channel, c_os_browser_type, c_conversion_kind, c_created_by, c_created_time)
select * from (SELECT mig.c_ecr_id, mig.c_fund_txn_id, 'optionfair','optionfair1','BINARY_TRADE','web','html','chrome','DEPOSIT', 'system', ban.request_time
FROM vendor.tbl_migration_cashier_map mig join optionfair_trading.banking ban on ban.account_id = mig.c_partner_account_id and mig.c_txn_id_at_bank = ban.id where ban.type = 102 order by mig.c_ecr_id, ban.id asc) abc Group by c_ecr_id ;

select row_count() tbl_fund_conversion_info;

update tbl_fund_conversion_info set c_label_id='optionfair';

select row_count() tbl_fund_conversion_info_update;


select now() endtime;