USE cashier;

select now() starttime;

DELETE from tbl_processor_api_config where c_processor_name like 'direct_mg_%';

INSERT INTO tbl_processor_api_config(c_processor_name,c_api_name,c_api_url, c_status, c_created_time, c_update_time, c_created_by, c_updated_by)
select CONCAT(CONCAT(CONCAT(CONCAT(CONCAT('direct_mg_',bank.psp_id), '_'), ifnull(luck.lut_name,'')),'_'),ifnull(lower(fun_mig_get_bt_option_value(bank.psp_id, bank.card_type)),'')), 'payments', bpr.psp_merchant_params, 'inactive', now(), now(),'system','system'
from optionfair_trading.banking bank left join optionfair_trading.brand_psp_rel bpr on bank.psp_id= bpr.psp_id left join optionfair_trading.lut luck on bank.psp_id = luck.id  group by bank.psp_id,bank.card_type;

INSERT INTO tbl_processor_api_config(c_processor_name,c_api_name,c_api_url, c_status, c_created_time, c_update_time, c_created_by, c_updated_by)
select CONCAT(CONCAT(CONCAT(CONCAT(CONCAT('direct_mg_',bank.psp_id), '_'), ifnull(luck.lut_name,'')),'_'),ifnull(lower(fun_mig_get_bt_option_value(bank.psp_id, bank.card_type)),'')), 'transferOut', bpr.psp_merchant_params, 'inactive', now(), now(),'system','system'
from optionfair_trading.banking bank left join optionfair_trading.brand_psp_rel bpr on bank.psp_id= bpr.psp_id left join optionfair_trading.lut luck on bank.psp_id = luck.id  group by bank.psp_id,bank.card_type;

DELETE from tbl_processor_config where c_processor_name like 'direct_mg_%';

INSERT INTO tbl_processor_config (c_processor_name, c_option, c_mid, c_email_id, c_mpwd1, c_mpwd2, c_client_id, c_client_secret, c_status, c_created_time, c_update_time, c_created_by, c_updated_by, c_api_user, c_api_password, c_withdrawl_allowed) 
SELECT CONCAT(CONCAT(CONCAT(CONCAT(CONCAT('direct_mg_',bank.psp_id), '_'), ifnull(luck.lut_name,'')),'_'),ifnull(lower(fun_mig_get_bt_option_value(bank.psp_id, bank.card_type)),'')), upper(fun_mig_get_bt_option_value(bank.psp_id, bank.card_type)), bpr.merchant_id, '', '', '', '', bpr.merchant_key, 'inactive', now(), now(), 'system', 'system', '', '', 1
from optionfair_trading.banking bank left join optionfair_trading.brand_psp_rel bpr on bank.psp_id= bpr.psp_id left join optionfair_trading.lut luck on bank.psp_id = luck.id  group by bank.psp_id,bank.card_type;


INSERT INTO `tbl_instrument`
(c_ecr_id,c_instrument,c_option,c_first_part,c_last_part,c_status,c_deposit_attempted,c_deposit_success,c_payout_attempted,c_payout_success,c_currency,c_instrument_detail1,c_instrument_detail2,c_instrument_detail3,c_created_time,c_update_time,c_created_by,c_card_token)
SELECT mig.c_ecr_id,IFNULL(concat(concat(bank.card_num_start,'XXXX'),bank.card_num),'XXXXXX'),upper(fun_mig_get_bt_option_value(bank.psp_id, bank.card_type)),bank.card_num_start,bank.card_num,'active',if(bank.type = 102,1,0),if(bank.type = 102, CASE WHEN bank.status IN (97,98,100) THEN 1 WHEN bank.status in (99,101,108) THEN 0 END, 0),if(bank.type = 103,1,0), if(bank.type = 103, CASE WHEN bank.status IN (97,98,100) THEN 1 WHEN bank.status in (99,101,108) THEN 0 END, 0), currency.symbol,bank.cc_expiry,bank.psp_id, bank.card_type, bank.request_time,bank.update_time, bank.requested_by,bank.card_token
FROM ecr.tbl_migration_map mig join
(SELECT * FROM (SELECT  account_id,id,psp_id,card_num,card_num_start,cc_expiry,request_time,update_time,currency_id,requested_by,card_type,card_token, type, status FROM optionfair_trading.banking WHERE TYPE IN (102,103) ORDER BY account_id,id DESC) x GROUP BY account_id, card_num,card_num_start,card_type,psp_id) bank ON mig.c_partner_account_id = bank.account_id
JOIN optionfair_trading.currency currency ON bank.currency_id = currency.currency_id
LEFT JOIN optionfair_trading.lut lut      ON bank.card_type   = lut.id;

select row_count() tbl_instrument; 

INSERT INTO tbl_instrument_log 
(c_ecr_id,c_instrument,c_option,c_first_part,c_last_part,c_status,c_currency,c_instrument_detail1,c_created_time,c_created_by,c_card_token)
SELECT c_ecr_id,c_instrument,c_option,c_first_part,c_last_part,c_status,c_currency,c_instrument_detail1,c_created_time,c_created_by,c_card_token
FROM  tbl_instrument;

select row_count() tbl_instrument_log; 


INSERT INTO tbl_instrument_billing_address
(c_instrument_id,c_ecr_id,c_address1,c_city,c_state,c_country_code,c_zip,c_created_time,c_update_time,c_created_by)
SELECT instru.c_id,instru.c_ecr_id,bank.address,bank.city,bank.state_iso,bank.country_iso,bank.zip,bank.request_time,bank.update_time,bank.requested_by 
FROM ecr.tbl_migration_map mig 
JOIN  (SELECT * FROM (SELECT  account_id,id,address,city,state_iso,country_iso,zip,request_time,update_time,requested_by,psp_id,card_type,card_num,card_num_start FROM optionfair_trading.banking WHERE TYPE IN (102,103) ORDER BY account_id,id DESC) x GROUP BY account_id, card_num,card_num_start,card_type,psp_id) bank ON mig.c_partner_account_id = bank.account_id 
JOIN tbl_instrument instru ON mig.c_ecr_id = instru.c_ecr_id  and ifnull(instru.c_first_part,0) = ifnull(bank.card_num_start,0) COLLATE utf8_unicode_ci and instru.c_last_part = bank.card_num COLLATE utf8_unicode_ci and  instru.c_instrument_detail2 = bank.psp_id and instru.c_instrument_detail3 = bank.card_type;

select row_count() tbl_instrument_billing_address; 

INSERT INTO tbl_instrument_billing_address_log
(c_instrument_id,c_ecr_id,c_address1,c_city,c_state,c_country_code,c_zip,c_created_time,c_created_by)
SELECT c_instrument_id,c_ecr_id,c_address1,c_city,c_state,c_country_code,c_zip,c_created_time,c_created_by
FROM tbl_instrument_billing_address;


select row_count() tbl_instrument_billing_address_log; 


INSERT INTO tbl_deposit_withdrawl_flags
(c_ecr_id,c_deposit_allowed,c_withdrawl_allowed,c_created_by,c_created_time,c_updated_by, c_updated_time,c_comments)
SELECT mig.c_ecr_id,CASE WHEN acc.status IN (400,410,500,510) THEN 1 ELSE 0 END,CASE WHEN acc.status IN (400,410,500,510) THEN 1 ELSE 0 END,'system',acc.registration_date, 'system',acc.registration_date,"" 
FROM ecr.tbl_migration_map mig 
JOIN optionfair_trading.account acc ON mig.c_partner_account_id = acc.id;

select row_count() tbl_deposit_withdrawl_flags; 

update tbl_deposit_withdrawl_flags flag, tbl_migration_map mig set c_withdrawl_allowed = 0 where mig.c_ecr_id=flag.c_ecr_id and mig.c_account_type_id in (10,132);

INSERT INTO tbl_deposit_withdrawl_flags_log
(c_ecr_id,c_deposit_allowed,c_withdrawl_allowed,c_created_by,c_created_time,c_updated_by, c_updated_time, c_comments)
SELECT c_ecr_id,c_deposit_allowed,c_withdrawl_allowed,c_created_by,c_created_time,c_updated_by, c_updated_time,c_comments
FROM tbl_deposit_withdrawl_flags;

select row_count() tbl_deposit_withdrawl_flags_log; 

INSERT INTO `tbl_cashier_deposit`
(c_txn_id,c_ecr_id,c_partner_id,c_label_id,c_initial_amount,c_credited_amount,c_credited_currency,c_currency,c_confirmed_amount,c_credited_amount_in_ecr_ccy,c_ecr_currency,c_inhouse_amount, c_inhouse_currency,c_txn_status,c_txn_kind,c_processor_ref_id,c_processor_name, c_instrument_id, c_option,c_external_error_code,c_external_error_comments,c_comments,c_created_time,c_update_time,c_created_by, c_updated_by, c_refund_amount)
SELECT mig.c_cashier_txn_id,mig.c_ecr_id,'optionfair', 'optionfair',ABS(bank.amount),if(bank.status in (97,98,100),ABS(bank.amount),0), currency.symbol,currency.symbol,if(bank.status in (97,98,100),ABS(bank.amount),0),if(bank.status in (97,98,100),ABS(bank.amount),0),currency2.symbol,ABS(bank.amount) * currency.Normalized_to_usd,'USD',CASE WHEN bank.status IN (99) THEN 'txn_initiated' WHEN bank.status in (97,98,100) THEN 'txn_confirmed_success' WHEN bank.status IN (101,108) THEN 'txn_confirmed_failure' END,'deposit_in',bank.external_transaction_id, CONCAT(CONCAT(CONCAT(CONCAT(CONCAT('direct_mg_',bank.psp_id), '_'), ifnull(luck.lut_name,'')),'_'),ifnull(lower(fun_mig_get_bt_option_value(bank.psp_id, bank.card_type)),'')), instru.c_id, upper(fun_mig_get_bt_option_value(bank.psp_id, bank.card_type)) ,bank.external_auth_code,bank.psp_reason,concat(bank.id,concat('_',ifnull(bank.psp_response,''))),bank.request_time,bank.update_time,bank.requested_by,bou.username, ABS(bank.amount_for_withdrawl)
FROM vendor.tbl_migration_cashier_map mig 
JOIN optionfair_trading.banking bank       ON mig.c_txn_id_at_bank     = bank.id
JOIN optionfair_trading.account acc        ON bank.account_id          = acc.id 
JOIN optionfair_trading.currency currency  ON bank.currency_id         = currency.currency_id 
JOIN optionfair_trading.currency currency2 ON acc.currency_id          = currency2.currency_id
left join optionfair_trading.bo_user bou 	   ON bou.id = bank.sales_rep
left join optionfair_trading.lut luck on bank.psp_id = luck.id
left JOIN tbl_instrument instru ON mig.c_ecr_id = instru.c_ecr_id  and ifnull(instru.c_first_part,0) = ifnull(bank.card_num_start,0) COLLATE utf8_unicode_ci and instru.c_last_part = bank.card_num COLLATE utf8_unicode_ci and  instru.c_instrument_detail2 = bank.psp_id and instru.c_instrument_detail3 = bank.card_type
WHERE bank.type IN (102);

update cashier.tbl_cashier_deposit dep, optionfair_trading.banking  bank, vendor.tbl_migration_cashier_map mig  set dep.c_refund_amount= if(bank.amount<bank.amount_for_withdrawl,0,bank.amount-bank.amount_for_withdrawl) where mig.c_txn_id_at_bank     = bank.id and mig.c_partner_account_id = bank.account_id and  mig.c_ecr_id=dep.c_ecr_id and mig.c_cashier_txn_id = dep.c_txn_id;

update cashier.tbl_cashier_deposit  set c_refund_amount=if(c_initial_amount<c_refund_amount,0,c_initial_amount-c_refund_amount);


select ABS(c_initial_amount-c_refund_amount) from cashier.tbl_cashier_deposit where ecr_id=776971099886940000;

select count(*) from cashier.tbl_cashier_deposit dep, optionfair_trading.banking  bank, vendor.tbl_migration_cashier_map mig  where mig.c_txn_id_at_bank     = bank.id and mig.c_partner_account_id = bank.account_id and  mig.c_ecr_id=dep.c_ecr_id and mig.c_cashier_txn_id = dep.c_txn_id;



select row_count() tbl_cashier_deposit; 

INSERT INTO tbl_cashier_deposit_log
(c_txn_id,c_ecr_id,c_partner_id,c_label_id,c_initial_amount,c_confirmed_amount,c_credited_currency,c_currency,c_credited_amount,c_credited_amount_in_ecr_ccy,c_ecr_currency,c_inhouse_currency,c_txn_status,c_txn_kind,c_processor_ref_id,c_instrument_id, c_option,c_external_error_code,c_external_error_comments,c_comments,c_created_time,c_created_by,c_refund_amount)
SELECT c_txn_id,c_ecr_id,c_partner_id,c_label_id,c_initial_amount,c_confirmed_amount,c_credited_currency,c_currency,c_credited_amount,c_credited_amount_in_ecr_ccy,c_ecr_currency,c_inhouse_currency,c_txn_status,c_txn_kind,c_processor_ref_id,c_instrument_id,c_option,c_external_error_code,c_external_error_comments,c_comments,c_created_time,c_created_by,c_refund_amount
FROM tbl_cashier_deposit  where c_partner_id ='optionfair';

select row_count() tbl_cashier_deposit_log;

INSERT INTO tbl_cashier_deposit_summary(c_ecr_id, c_txn_count, c_start_time, c_end_time, c_amount, c_currency, c_created_time, c_inhouse_amount, c_inhouse_currency, c_amount_in_ecr_ccy, c_ecr_ccy) 
SELECT c_ecr_id,count(1),DATE_ADD(DATE_FORMAT(c_update_time, "%Y-%m-%d %H:00:00"), INTERVAL (IF(MINUTE(c_update_time) < 30, 0, 30)) MINUTE),DATE_ADD(DATE_ADD(c_update_time, INTERVAL (IF(MINUTE(c_update_time) < 30, 29, 59) - MINUTE(c_update_time)) MINUTE), INTERVAL(IF(MINUTE(c_update_time) < 30, 59, 59) - SECOND(c_update_time))SECOND), SUM(c_credited_amount), c_currency, c_update_time, SUM(c_inhouse_amount), 'USD' ,SUM(c_credited_amount) , MAX(c_ecr_currency)
FROM tbl_cashier_deposit
WHERE c_txn_status IN ('txn_confirmed_success','co_initiated') GROUP BY c_ecr_id,c_currency,UNIX_TIMESTAMP(c_update_time) DIV 1800;

select row_count() tbl_cashier_deposit_summary;

INSERT INTO tbl_cashier_deposit_summary_log(c_summary_id, c_ecr_id, c_txn_count, c_start_time, c_end_time, c_amount, c_currency, c_created_time, c_inhouse_amount, c_inhouse_currency, c_amount_in_ecr_ccy, c_ecr_ccy) 
 SELECT c_id, c_ecr_id, c_txn_count, c_start_time, c_end_time, c_amount, c_currency, c_created_time, c_inhouse_amount, c_inhouse_currency, c_amount_in_ecr_ccy, c_ecr_ccy
FROM tbl_cashier_deposit_summary;


select row_count() tbl_cashier_deposit_summary_log;


/* --TODO  bou requested_by  */
INSERT INTO tbl_cashier_cashout
(c_txn_id,c_ecr_id,c_partner_id,c_label_id,c_initial_amount,c_reversed_amount,c_paid_amount,c_paid_ccy,
c_currency,c_inhouse_amount,c_inhouse_currency,c_amount_in_ecr_ccy,c_ecr_ccy,c_txn_status,c_txn_kind,c_processor_name,c_processor_ref_id1,c_instrument_id, c_preferred_option,c_external_error_code,c_external_error_comments, c_processor_comments,c_created_time,c_update_time,c_created_by)
SELECT mig.c_cashier_txn_id,mig.c_ecr_id,'optionfair','optionfair',ABS(bank.amount),if(bank.status in (108),ABS(bank.amount),0),if(bank.status in (98),ABS(bank.amount),0),currency.symbol,currency.symbol,ABS(bank.amount) * currency.Normalized_to_usd,'USD',ABS(bank.amount),currency2.symbol,CASE WHEN bank.status IN (98) THEN 'co_success' WHEN bank.status in(99,100) THEN 'co_initiated' WHEN bank.status=108 THEN 'co_reversed' else 'co_initiated' END,'redeem_in',CONCAT(CONCAT(CONCAT(CONCAT(CONCAT('direct_mg_',bank.psp_id), '_'), ifnull(luck.lut_name,'')),'_'),ifnull(lower(fun_mig_get_bt_option_value(bank.psp_id, bank.card_type)),'')),bank.external_transaction_id,instru.c_id,upper(fun_mig_get_bt_option_value(bank.psp_id, bank.card_type)),bank.external_auth_code,bank.psp_reason,concat(bank.id,concat('_',ifnull(bank.psp_response,''))), bank.request_time,bank.update_time,bank.requested_by
FROM vendor.tbl_migration_cashier_map mig 
JOIN optionfair_trading.banking  bank      ON mig.c_txn_id_at_bank     = bank.id
JOIN optionfair_trading.account  acc       ON mig.c_partner_account_id = acc.id 
JOIN optionfair_trading.currency currency  ON bank.currency_id         = currency.currency_id 
JOIN optionfair_trading.currency currency2 ON acc.currency_id          = currency2.currency_id
left join optionfair_trading.lut luck on bank.psp_id = luck.id
left JOIN tbl_instrument instru ON mig.c_ecr_id = instru.c_ecr_id  and ifnull(instru.c_first_part,0) = ifnull(bank.card_num_start,0) COLLATE utf8_unicode_ci and instru.c_last_part = bank.card_num COLLATE utf8_unicode_ci and  instru.c_instrument_detail2 = bank.psp_id and instru.c_instrument_detail3 = bank.card_type
WHERE bank.type IN (103);

update tbl_cashier_cashout cash, optionfair_trading.banking  bank, vendor.tbl_migration_cashier_map mig   set cash.c_txn_status ='co_success' where cash.c_txn_status = 'co_initiated' and mig.c_txn_id_at_bank     = bank.id and mig.c_partner_account_id = bank.account_id and  mig.c_ecr_id=cash.c_ecr_id and mig.c_cashier_txn_id = cash.c_txn_id and bank.status=100;


select c_ecr_id from tbl_cashier_cashout cash, optionfair_trading.banking  bank, vendor.tbl_migration_cashier_map mig  where cash.c_txn_status = 'co_reversed' and mig.c_txn_id_at_bank     = bank.id and mig.c_partner_account_id = bank.account_id and  mig.c_ecr_id=cash.c_ecr_id and mig.c_cashier_txn_id = cash.c_txn_id and bank.status=100 and c_created_time<'2016-05-31 22:53:05';

select row_count() tbl_cashier_cashout; 

INSERT INTO tbl_cashier_cashout_log
(c_txn_id,c_ecr_id,c_partner_id,c_label_id,c_initial_amount,c_reversed_amount,c_paid_amount,c_paid_ccy,
c_currency,c_ecr_ccy,c_txn_status,c_txn_kind,c_processor_ref_id1, c_preferred_option,c_external_error_code,
c_external_error_comments, c_processor_comments,c_created_time,c_created_by)
SELECT c_txn_id,c_ecr_id,c_partner_id,c_label_id,c_initial_amount,c_reversed_amount,c_paid_amount,c_paid_ccy,
c_currency,c_ecr_ccy,c_txn_status,c_txn_kind,c_processor_ref_id1, c_preferred_option,c_external_error_code,
c_external_error_comments, c_processor_comments,c_created_time,c_created_by
FROM tbl_cashier_cashout where c_partner_id ='optionfair';

select row_count() tbl_cashier_cashout_log; 

INSERT INTO tbl_cashier_cashout_summary(c_ecr_id, c_txn_count, c_start_time, c_end_time, c_amount, c_currency, c_created_time, c_inhouse_amount, c_inhouse_currency, c_amount_verified,c_amount_posted, c_amount_reversed,c_amount_in_ecr_ccy,c_ecr_ccy) 
SELECT c_ecr_id,count(1),DATE_ADD(DATE_FORMAT(c_update_time, "%Y-%m-%d %H:00:00"), INTERVAL (IF(MINUTE(c_update_time) < 30, 0, 30)) MINUTE),DATE_ADD(DATE_ADD(c_update_time, INTERVAL (IF(MINUTE(c_update_time) < 30, 29, 59) - MINUTE(c_update_time)) MINUTE), INTERVAL(IF(MINUTE(c_update_time) < 30, 59, 59) - SECOND(c_update_time))SECOND), SUM(c_initial_amount), c_currency, c_update_time, SUM(c_inhouse_amount), 'USD' ,SUM(c_paid_amount) , SUM(c_paid_amount), SUM(c_reversed_amount) ,SUM(c_initial_amount),c_ecr_ccy
FROM tbl_cashier_cashout
WHERE c_txn_status IN ('txn_confirmed_success','co_initiated') GROUP BY c_ecr_id,c_currency,UNIX_TIMESTAMP(c_update_time) DIV 1800;

select row_count() tbl_cashier_cashout_summary; 

INSERT INTO tbl_cashier_cashout_summary_log(c_summary_id, c_ecr_id, c_txn_count, c_start_time, c_end_time, c_amount, c_currency, c_created_time, c_inhouse_amount, c_inhouse_currency, c_amount_verified, c_amount_posted, c_amount_reversed, c_amount_in_ecr_ccy, c_ecr_ccy) 
 SELECT c_id, c_ecr_id, c_txn_count, c_start_time, c_end_time, c_amount, c_currency, c_created_time, c_inhouse_amount, c_inhouse_currency, c_amount_verified, c_amount_posted, c_amount_reversed,c_amount_in_ecr_ccy,c_ecr_ccy
FROM tbl_cashier_cashout_summary;

select row_count() tbl_cashier_cashout_summary_log; 

INSERT INTO tbl_cashier_wallet_txn
(c_ecr_id,c_old_amount,c_amount,c_new_amount,c_currency,c_rounding_diff,c_exchange_snapshot_id,c_pl_flag,c_txn_type,
c_txn_kind,c_ref_txn_id,c_created_time,c_created_by)
SELECT mig.c_ecr_id,IF(((IFNULL(fin.account_balance,0) - bank.amount)<0),0,(IFNULL(fin.account_balance,0) - bank.amount)),ABS(bank.amount),IFNULL(fin.account_balance,0),currency.symbol,0,currency.Normalized_to_usd,1,CASE bank.type WHEN 102 THEN 'credit' WHEN 103 THEN 'debit' END,'wallet_credit',mig.c_cashier_txn_id,bank.request_time,bank.requested_by
FROM vendor.tbl_migration_cashier_map mig 
JOIN optionfair_trading.banking  bank     ON bank.account_id = mig.c_partner_account_id and mig.c_txn_id_at_bank = bank.id 
LEFT JOIN optionfair_trading.finaincial_ledger fin on fin.id =  bank.financail_ledger_id
JOIN optionfair_trading.currency currency ON bank.currency_id         = currency.currency_id
WHERE bank.type IN (102,103);


select row_count() tbl_cashier_wallet_txn; 


INSERT INTO tbl_cashier_wallet
(c_ecr_id,c_old_amount,c_amount,c_new_amount,c_currency,c_rounding_diff,c_exchange_snapshot_id,c_cummulative_rounding_diff,c_pl_flag, c_txn_type,c_txn_kind,c_last_txn_id,c_ref_txn_id,c_updated_by)
SELECT mig.c_ecr_id,0,if(tmpmig.c_bal_for_withdrawal < 0, 0, tmpmig.c_bal_for_withdrawal), 0, ifnull(currency.symbol,'USD'),0,1,0,1,wallet.c_txn_type,wallet.c_txn_kind,wallet.c_txn_id,cashout.c_txn_id,'system'
FROM ecr.tbl_migration_map mig 
JOIN ecr.tbl_migration_temp tmpmig on tmpmig.c_partner_account_id = mig.c_partner_account_id
JOIN optionfair_trading.account acc on acc.id = mig.c_partner_account_id
JOIN optionfair_trading.currency currency ON acc.currency_id   = currency.currency_id
JOIN ( SELECT  * FROM ( SELECT c_ecr_id,c_txn_type,c_txn_kind,c_txn_id FROM tbl_cashier_wallet_txn ORDER BY c_ecr_id,c_txn_id DESC) x GROUP BY c_ecr_id) wallet ON wallet.c_ecr_id = mig.c_ecr_id 
JOIN (SELECT * FROM  (SELECT c_ecr_id,c_txn_id FROM tbl_cashier_cashout ORDER BY c_ecr_id,c_txn_id DESC) x GROUP BY c_ecr_id) cashout ON cashout.c_ecr_id = mig.c_ecr_id;

select row_count() tbl_cashier_wallet; 

INSERT INTO tbl_wire_transaction
(c_ecr_id,c_instrument_id,c_full_name,c_IBAN,c_bank_account_number,c_bank_name,c_bank_address,c_bank_city,c_bank_country,c_BIC, c_SWIFT_code,c_message_to_customer, c_created_time,c_update_time,c_created_by,c_updated_by)
SELECT mig.c_ecr_id,instru.c_id,bank.beneficiary_name,'',bank.account_number,bank.bank_name,bank.bank_address,'','','',bank.swift,'',bank.request_time,bank.update_time,bank.requested_by,bank.requested_by
FROM vendor.tbl_migration_cashier_map mig
JOIN optionfair_trading.banking bank      ON mig.c_txn_id_at_bank = bank.id
JOIN optionfair_trading.currency currency ON bank.currency_id     = currency.currency_id
left JOIN tbl_instrument instru ON mig.c_ecr_id = instru.c_ecr_id  and ifnull(instru.c_first_part,0) = ifnull(bank.card_num_start,0) COLLATE utf8_unicode_ci and instru.c_last_part = bank.card_num COLLATE utf8_unicode_ci and  instru.c_instrument_detail2 = bank.psp_id and instru.c_instrument_detail3 = bank.card_type where bank.type=103 and bank.psp_id=93;


select row_count() tbl_wire_transaction; 


INSERT INTO tbl_wire_transaction_log
(c_ecr_id,c_instrument_id,c_full_name,c_IBAN,c_bank_account_number,c_bank_name,c_bank_address,c_bank_city,c_bank_country,c_BIC, c_SWIFT_code,c_message_to_customer, c_currency,c_amount,c_created_time,c_created_by)
SELECT c_ecr_id,c_instrument_id,c_full_name,c_IBAN,c_bank_account_number,c_bank_name,c_bank_address,c_bank_city,c_bank_country,c_BIC, c_SWIFT_code,c_message_to_customer, '','',c_update_time,c_updated_by
FROM tbl_wire_transaction;

select row_count() tbl_wire_transaction_log;

INSERT INTO tbl_card_token_details(c_ecr_id, c_instrument_id, c_card_token, c_processor_name, c_expiry)
select abc.c_ecr_id, abc.c_id, abc.token_string , abc.proc_name, abc.expiry 
from (SELECT mig.c_ecr_id, instru.c_id, bank.token_string, fun_mig_get_processor_name(bank.type) as proc_name, bank.expiry, bank.account_id, bank.num, bank.num_start,bank.type,bank.psp_id
FROM ecr.tbl_migration_map mig 
JOIN optionfair_trading.credit_card bank ON mig.c_partner_account_id = bank.account_id and bank.token_string is not null
left JOIN tbl_instrument instru ON mig.c_ecr_id = instru.c_ecr_id  and ifnull(instru.c_first_part,0) = ifnull(bank.num_start,0) COLLATE utf8_unicode_ci and instru.c_last_part = bank.num COLLATE utf8_unicode_ci and  instru.c_instrument_detail2 = bank.psp_id and instru.c_instrument_detail3 = bank.type  order by  bank.expiry%10000  desc) abc group by account_id,num,num_start,type,psp_id;


select row_count() tbl_card_token_details;

select count(*) from tbl_cashier_cashout_summary where c_created_time <'2016-05-31 22:53:05';

4375

delete from tbl_cashier_cashout_summary where c_created_time <'2016-05-31 22:53:05';

INSERT INTO tbl_cashier_cashout_summary(c_ecr_id, c_txn_count, c_start_time, c_end_time, c_amount, c_currency, c_created_time, c_inhouse_amount, c_inhouse_currency, c_amount_verified,c_amount_posted, c_amount_reversed,c_inhouse_amount_verified, c_inhouse_amount_posted, c_inhouse_amount_reversed,c_amount_in_ecr_ccy,c_ecr_ccy, c_amount_in_ecr_ccy_verified, c_amount_in_ecr_ccy_posted, c_amount_in_ecr_ccy_reversed) 
SELECT c_ecr_id,count(1),DATE_ADD(DATE_FORMAT(c_created_time, "%Y-%m-%d %H:00:00"), INTERVAL (IF(MINUTE(c_created_time) < 30, 0, 30)) MINUTE),DATE_ADD(DATE_ADD(c_created_time, INTERVAL (IF(MINUTE(c_created_time) < 30, 29, 59) - MINUTE(c_created_time)) MINUTE), INTERVAL(IF(MINUTE(c_created_time) < 30, 59, 59) - SECOND(c_created_time))SECOND), if(c_txn_status in ('co_success','co_initiated','co_verified','co_reversed'), SUM(c_initial_amount), 0), c_currency, c_created_time, SUM(c_inhouse_amount), 'USD' ,if(c_txn_status in('co_initiated','co_verified'),SUM(c_inhouse_amount),0) , if(c_txn_status in ('co_initiated','co_verified','co_success'),SUM(c_inhouse_amount),0), if(c_txn_status = 'co_reversed',SUM(c_reversed_amount),0),if(c_txn_status in('co_initiated','co_verified'),SUM(c_inhouse_amount),0) , if(c_txn_status in ('co_initiated','co_verified','co_success'),SUM(c_inhouse_amount),0), if(c_txn_status = 'co_reversed',SUM(c_inhouse_amount),0) ,if(c_txn_status in ('co_initiated','co_success','co_verified','co_reversed'), SUM(c_initial_amount), 0),c_ecr_ccy, if(c_txn_status in ('co_initiated','co_verified'),SUM(c_inhouse_amount),0),if(c_txn_status in ('co_initiated','co_verified','co_success'),SUM(c_inhouse_amount),0),if(c_txn_status in ('co_reversed'),SUM(c_reversed_amount),0)
FROM tbl_cashier_cashout_log
WHERE c_txn_status IN ('co_success','co_initiated','co_verified','co_reversed') and c_created_time <'2016-05-31 22:53:05' GROUP BY c_ecr_id,c_currency,UNIX_TIMESTAMP(c_created_time) DIV 1800; 

27822
29797
/*
FROM tbl_cashier_cashout
WHERE c_txn_status IN ('co_success','co_initiated','co_verified','co_reversed') and c_update_time <'2016-05-31 22:53:05' GROUP BY c_ecr_id,c_currency,UNIX_TIMESTAMP(c_update_time) DIV 1800; 
*/


select now() endtime;