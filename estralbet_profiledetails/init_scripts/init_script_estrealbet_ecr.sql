LOAD DATA LOCAL INFILE '/mnt/data/migration_data/tbl_ecr/tbl_ecr.csv' INTO TABLE tbl_ecr_mig CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES ;

LOAD DATA LOCAL INFILE '/mnt/data/migration_data/tbl_ecr_profile_list/tbl_ecr_profile_list.csv' INTO TABLE tbl_ecr_profile_list_mig CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES ;

LOAD DATA LOCAL INFILE '/mnt/data/migration_data/tbl_ecr_category/tbl_ecr_category.csv' INTO TABLE tbl_ecr_category_mig CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES ;

LOAD DATA LOCAL INFILE '/mnt/data/migration_data/tbl_ecr_subscriptions/tbl_ecr_subscriptions.csv' INTO TABLE tbl_ecr_subscriptions_mig CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES ;

LOAD DATA LOCAL INFILE '/mnt/data/migration_data/tbl_ecr_flags/tbl_ecr_flags.csv' INTO TABLE tbl_ecr_flags_mig CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES ;

LOAD DATA LOCAL INFILE '/mnt/data/migration_data/tbl_ecr_screen_name/tbl_ecr_screen_name.csv' INTO TABLE tbl_ecr_screen_name_mig CHARACTER SET 'latin1' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES ;