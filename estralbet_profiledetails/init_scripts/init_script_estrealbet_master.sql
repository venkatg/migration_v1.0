LOAD DATA LOCAL INFILE '/mnt/data/migration_data/tbl_ecr_profile_list/tbl_ecr_profile_list.csv' INTO TABLE tbl_ecr_profile_list_mig CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES ;

LOAD DATA LOCAL INFILE '/mnt/data/migration_data/tbl_ecr_subscriptions/tbl_ecr_subscriptions.csv' INTO TABLE tbl_ecr_subscriptions_mig CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES ;

LOAD DATA LOCAL INFILE '/mnt/data/migration_data/tbl_ecr_flags/tbl_ecr_flags.csv' INTO TABLE tbl_ecr_flags_mig CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES ;