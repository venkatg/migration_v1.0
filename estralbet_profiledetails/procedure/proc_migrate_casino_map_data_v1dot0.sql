use migestrelabet;
SELECT DATABASE();

delimiter #
DROP PROCEDURE IF EXISTS proc_migrate_map_data_v1dot0#

CREATE PROCEDURE proc_migrate_map_data_v1dot0(in datein date)
BEGIN

truncate migestrelabet.tbl_migration_map_mig;
		
set @i=1,@j=1;
INSERT INTO migestrelabet.tbl_migration_map_mig (c_ecr_id,c_external_id,c_partner_account_id,c_partner_id,c_status,c_created_time, c_created_by,c_updated_by) SELECT fun_mig_casino_gen_ecr_id(@i := @i+100,datein,10000,89999), mig.c_partner_account_id, mig.c_partner_account_id,"estrelabet",'Initial Data',now(),'system',null FROM migestrelabet.tbl_ecr_mig mig;
	
END #
delimiter ;