use migestrelabet;
SELECT DATABASE();

delimiter #
DROP PROCEDURE IF EXISTS proc_migrate_casino_map_missing_ids_data_v1dot0#

CREATE PROCEDURE proc_migrate_casino_map_missing_ids_data_v1dot0(in datein date)
BEGIN

		
set @i=1;
INSERT IGNORE INTO migestrelabet.tbl_migration_map_mig (c_ecr_id,c_external_id,c_partner_account_id,c_partner_id,c_status,c_created_time, c_created_by,c_updated_by) SELECT fun_mig_casino_gen_ecr_id(@i := @i+1,datein,10000000,89999999), mig.c_partner_account_id, mig.c_partner_account_id,"estrelabet",'Initial Data',now(),'system',null FROM migestrelabet.tbl_ecr_mig mig where mig.c_partner_account_id not in (select c_partner_account_id from tbl_migration_map_mig);

	
END #
delimiter ;