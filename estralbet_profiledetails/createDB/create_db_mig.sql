/****************************************************** clean up database & create migestrelabet schema START ******************************************************/
DROP SCHEMA IF EXISTS migestrelabet;
CREATE SCHEMA migestrelabet;
GRANT USAGE ON *.* TO 'migestrelabet'@'localhost';
DROP USER 'migestrelabet'@'localhost';
CREATE USER 'migestrelabet'@'localhost' IDENTIFIED BY 'migestrelabet';
GRANT ALL ON migestrelabet.* TO 'migestrelabet'@'localhost';
FLUSH PRIVILEGES;


ALTER DATABASE migestrelabet CHARACTER SET utf8 COLLATE utf8_bin;

USE  migestrelabet;
SELECT DATABASE();
/****************************************************** clean up database end ******************************************************/
