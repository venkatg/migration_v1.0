DROP table if exists tbl_real_fund_mig;
CREATE TABLE tbl_real_fund_mig (
c_partner_account_id      bigint(20) NOT NULL,
c_ecr_crncy                    varchar(3)  COLLATE utf8_bin,
c_realcash_balance              varchar(30), 
c_bonus_balance              varchar(30),
c_ecr_status	        varchar(30)  COLLATE utf8_bin,
c_updated_by                   varchar(100) COLLATE utf8_bin,
c_created_time  timestamp,
c_update_time timestamp,
KEY idx_partner_account_id (c_partner_account_id)
);

alter table tbl_migration_map_mig  add COLUMNS c_category varchar(100) ,add column c_email_triggered tinyint(1),add column c_sms_triggered tinyint(1),add column c_test_user tinyint(4),add column c_player_status varchar(100);

DROP table if exists tbl_migration_map_mig;
CREATE TABLE IF NOT EXISTS tbl_migration_map_mig(
	c_id bigint NOT NULL auto_increment unique,				
	c_partner_account_id bigint(20) NOT NULL,	
	c_partner_id varchar(30) not null,
	c_ecr_id bigint unique,		
	c_external_id bigint unique,
	c_master_id varchar(30)  unique,		
	c_status varchar(30) comment 'skipped/initiated/etc..', 
	c_created_time datetime not null,		
	c_update_time datetime not null default now(),		
	c_created_by varchar(30),		
	c_updated_by varchar(30),
    c_category varchar(100),
	c_email_triggered tinyint(1),
	c_sms_triggered tinyint(1),
	c_test_user tinyint(4),
	c_player_status varchar(100),
	unique (c_partner_account_id),
KEY idx_partner_account_id (c_partner_account_id),
KEY idx_c_ecr_id (c_ecr_id)	
);


DROP table if exists tbl_ecr_mig;
CREATE TABLE tbl_ecr_mig (
c_partner_account_id     	bigint(20) NOT NULL,
c_language   varchar(20) COLLATE utf8_bin,
c_email_id varchar(100) COLLATE utf8_bin,
c_affiliate_id varchar(100) COLLATE utf8_bin,
c_signup_time  timestamp,
c_ecr_status     varchar(20) COLLATE utf8_bin,
c_passwd_hash    varchar(250) COLLATE utf8_bin,
c_updated_by   varchar(20) COLLATE utf8_bin,
c_update_time  datetime not null default now(),
c_id int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (c_id),
KEY idx_partner_account_id (c_partner_account_id),
KEY idx_c_email_id (c_email_id)
);


DROP table if exists tbl_ecr_profile_list_mig;
CREATE TABLE tbl_ecr_profile_list_mig (
c_partner_account_id  	bigint(20) NOT NULL,
c_timezone  varchar(100)  COLLATE utf8_bin,
c_fname  varchar(50)  COLLATE utf8_bin,
c_mname  varchar(30)  COLLATE utf8_bin,
c_lname  varchar(50)  COLLATE utf8_bin,
c_national_id  varchar(100)  COLLATE utf8_bin,
c_sex  varchar(10)  COLLATE utf8_bin,
c_register_ip  varchar(100)  COLLATE utf8_bin,
c_phone_number   bigint(20)  COLLATE utf8_bin,
c_mobile_number  bigint(20)  COLLATE utf8_bin,
c_last_login_date  timestamp,
c_country_code   varchar(3)  COLLATE utf8_bin,
c_address1   varchar(100)  COLLATE utf8_bin,
c_city  varchar(50)  COLLATE utf8_bin,
c_zip  varchar(30)  COLLATE utf8_bin,
c_address2  varchar(100)  COLLATE utf8_bin,
c_state  varchar(50)  COLLATE utf8_bin,
c_dob   date COLLATE utf8_bin,
c_phone_isd_code   varchar(10)  COLLATE utf8_bin,
c_mobile_isd_code  varchar(10)  COLLATE utf8_bin,
c_agent_name  varchar(30)  COLLATE utf8_bin,
c_partner_id  varchar(30)  COLLATE utf8_bin,
c_label_id  varchar(30)  COLLATE utf8_bin,
c_comments   varchar(100)  COLLATE utf8_bin,
c_created_time  timestamp,
c_title  varchar(5)  COLLATE utf8_bin,
c_nationality  varchar(60)  COLLATE utf8_bin,
c_id int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (c_id),
KEY idx_partner_account_id (c_partner_account_id)
);

DROP table if exists tbl_ecr_screen_name_mig;
CREATE TABLE tbl_ecr_screen_name_mig (
c_partner_account_id  bigint(20) NOT NULL,
c_screen_name  varchar(100) COLLATE utf8_bin,
c_liquidity_pool  varchar(100) COLLATE utf8_bin,
c_id int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (c_id),
KEY idx_partner_account_id (c_partner_account_id),
KEY idx_c_screen_name (c_screen_name)
);



DROP table if exists tbl_ecr_category_mig;
CREATE TABLE tbl_ecr_category_mig (
c_partner_account_id  	bigint(20) NOT NULL,
c_category varchar(100)  COLLATE utf8_bin,
c_status varchar(100)  COLLATE utf8_bin,
c_closed_reason varchar(250)  COLLATE utf8_bin,
c_creation_date timestamp,
c_last_modified_date timestamp,
c_comments varchar(250)  COLLATE utf8_bin,
c_reason varchar(100)  COLLATE utf8_bin,
KEY idx_partner_account_id (c_partner_account_id),
KEY idx_c_category  (c_category)
);

DROP table if exists tbl_ecr_subscriptions_mig;
CREATE TABLE tbl_ecr_subscriptions_mig (
c_partner_account_id  bigint(20) NOT NULL,
c_email_triggered  VARCHAR(3) ,
c_directmail_subscribed  VARCHAR(3) ,
c_sms_triggered  VARCHAR(3),
c_telephone_subscribed  VARCHAR(3) ,
c_id int(11) NOT NULL AUTO_INCREMENT,
PRIMARY KEY (c_id),
KEY idx_partner_account_id (c_partner_account_id)
);



DROP table if exists tbl_ecr_flags_mig;
CREATE TABLE tbl_ecr_flags_mig (
c_partner_account_id     bigint(20) NOT NULL,
c_test_user		 tinyint,
c_allow_bet_sports tinyint,
c_allow_bet_casino tinyint,
c_withdrawl_allowed	   tinyint ,
c_deposit_allowed	   tinyint ,
c_test_user_2 tinyint,
KEY idx_partner_account_id (c_partner_account_id)
);


create table temp_dup_screen(c_partner_account_id bigint(20));
create table temp_dup_screen(c_partner_account_id bigint(20),emailid varchar(50));