1. create new schema(e.g. here migcosmolot ) as mentioned in create_db_migcosmolot.sql


2.copy the provided csv folders in to one location and run below sh file.
bash  generate_csv_to_sql_scripts.sh

it will generate load commands for all csv files into file (i.e. init_ecr_scripts.sql)

Please verify and copy this file data (init_ecr_scripts.sql) to directory cosmolot/init_scripts.

3) Create mig_cosmolot folder under /home/river/ and copy committed cosmolot folder into mig_cosmolot 
basedir="/home/river/mig_cosmolot"; if we want then change river to sankar etc.

4) execute below scripts first under cosmolot/execute_cosmolot_ecr

 execute_create_ecr_db.sh 
 execute_init_ecr.sh (dumping provided data into respective tables in migcos db and here we are generating ecr and externalIds too)
 execute_migdata_ecr (data duming into ecr db)

Once done. We need create migcosmolot schema and move all tables functions and proc created under migcosmolot into rest all db nodes.

    DB schemas
   =============
    vendor node : vendor & ecr & regulatory
    fund node : fund & cashier
    BI node : bireports & segment & kafka
    Rest all schemas in Master Node

5) cosmolot/execute_cosmolot_bi  and cosmolot/execute_cosmolot_fund etc.


 bash execute_migdata_bi.sh

Migration: 
Execute required db schemas level scripis inorder as specified 
1) execute_create_schema.sh   --- schema
	-- all tables in mig db
3)execute_migdata_schema.sh --- data duming into respective db



Note: cosmolot folder(commited one) need to be keep under specified location
e.g.  /home/sankar/mig_cosmolot

