
Use cashier;

INSERT ignore INTO cashier.tbl_deposit_withdrawl_flags
(c_ecr_id,c_deposit_allowed,c_withdrawl_allowed,c_created_by,c_created_time,c_updated_by, c_updated_time,c_comments)
SELECT mig.c_ecr_id,IFNULL(migcashier.c_deposit_allowed,1),IFNULL(migcashier.c_withdrawl_allowed,1),'system',now(), 'system', now(),"Migration" 
FROM migestrelabet.tbl_migration_map_mig mig LEFT JOIN migestrelabet.tbl_ecr_flags_mig migcashier ON  mig.c_partner_account_id=migcashier.c_partner_account_id;


select row_count() tbl_deposit_withdrawl_flags, now() complet_time;

INSERT INTO cashier.tbl_deposit_withdrawl_flags_log
(c_ecr_id,c_deposit_allowed,c_withdrawl_allowed,c_created_by,c_created_time,c_comments)
SELECT c_ecr_id,c_deposit_allowed,c_withdrawl_allowed,c_created_by,c_created_time,c_comments
FROM cashier.tbl_deposit_withdrawl_flags where c_ecr_id in (select c_ecr_id from migestrelabet.tbl_migration_map_mig);

select row_count() tbl_deposit_withdrawl_flags_log, now() complet_time;