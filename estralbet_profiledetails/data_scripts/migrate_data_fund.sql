select now() starttime;

INSERT INTO fund.tbl_real_fund (c_ecr_id,c_external_id,c_ecr_status,c_realcash_balance,c_ecr_crncy,c_updated_by,c_created_time,c_updated_time) 
SELECT mig.c_ecr_id ,mig.c_external_id ,ifnull(ecrcat.c_status,"play"),FLOOR(ifnull(migfund.c_realcash_balance,0) * 100),  ifnull(migfund.c_ecr_crncy,'BRL'),'migration_eb',ifnull(migfund.c_created_time,now()),ifnull(migfund.c_created_time,now())
FROM migestrelabet.tbl_migration_map_mig mig 
 left join migestrelabet.tbl_real_fund_mig migfund on mig.c_partner_account_id = migfund.c_partner_account_id LEFT JOIN migestrelabet.tbl_ecr_category_mig ecrcat  on ecrcat.c_partner_account_id = mig.c_partner_account_id;
 
select row_count() tbl_real_fund, now() complet_time;

INSERT INTO fund.tbl_real_fund_log (c_ecr_id,c_external_id,c_ecr_status,c_realcash_balance,c_ecr_crncy,c_created_by,c_created_time) 
SELECT c_ecr_id,c_external_id,c_ecr_status,c_realcash_balance, c_ecr_crncy,c_updated_by,c_created_time from fund.tbl_real_fund where c_ecr_id in (select c_ecr_id FROM migestrelabet.tbl_migration_map_mig);
 
select row_count() tbl_real_fund_log, now() complet_time;



INSERT INTO fund.tbl_fund_conversion_info(c_ecr_id, c_txn_id, c_partner_id, c_label_id, c_product_id, c_channel, c_sub_channel, c_os_browser_type, c_conversion_kind, c_created_by, c_created_time) 
SELECT mig.c_ecr_id, mig.c_ecr_id, mig.c_partner_id,mig.c_partner_id,'CASINO','web','html','chrome','DEPOSIT','SYSTEM', now() FROM migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_category_mig ecrcat  on ecrcat.c_partner_account_id = mig.c_partner_account_id and LOWER(ecrcat.c_status) = "real" ;

select row_count() tbl_fund_conversion_info, now() complet_time;

 
select now() endtime;
