use ecr;
SELECT now() starttime;

INSERT IGNORE INTO ecr.tbl_ecr_log (c_ecr_id,c_master_id,c_external_id ,c_email_id,c_partner_id,c_registration_status ,c_ecr_status,c_passwd_hash ,c_signup_time ,c_created_by,c_created_time,c_language,c_affiliate_id,c_tracker_id,c_jurisdiction) 
SELECT c_ecr_id,c_master_id,c_external_id ,c_email_id,c_partner_id,c_registration_status ,c_ecr_status,c_passwd_hash ,c_signup_time ,c_updated_by,c_updated_time,c_language,c_affiliate_id,c_tracker_id,c_jurisdiction
FROM ecr.tbl_ecr WHERE c_ecr_id in (SELECT c_ecr_id FROM migestrelabet.tbl_migration_map_mig);

SELECT row_count() tbl_ecr_log, now() complet_time;


INSERT INTO ecr.tbl_ecr_labels_log ( c_ecr_id,c_partner_id,c_label_id,c_type,c_comments,c_created_by,c_created_time)
SELECT plist.c_ecr_id,plist.c_partner_id,plist.c_partner_id,'conversion','SYSTEM','Migration',now()
FROM migestrelabet.tbl_migration_map_mig plist;
SELECT row_count() tbl_ecr_labels_log, now() complet_time;


INSERT INTO ecr.tbl_ecr_category_log (c_master_id,c_ecr_id,c_category,c_old_category,c_is_change_manual,c_change_source,c_modified_by,c_last_modified_date,c_comments,c_reason) 
SELECT mig.c_master_id,mig.c_ecr_id,IFNULL(migcat.c_category,"c4"),NULL,true,'migration','migration',mig.c_created_time,'Migration',"migration"
FROM migestrelabet.tbl_migration_map_mig mig LEFT JOIN migestrelabet.tbl_ecr_category_mig migcat on  mig.c_partner_account_id = migcat.c_partner_account_id; 

SELECT row_count() tbl_ecr_category_log, now() complet_time;

INSERT INTO ecr.tbl_ecr_address_vrfn_log (c_ecr_id, c_source, c_agent_name,c_vrfn_status,c_partner_id,c_label_id,c_processor_response_code,c_created_time,c_comments) 
SELECT c_ecr_id, c_source, c_agent_name,c_vrfn_status,c_partner_id,c_label_id,c_processor_response_code,c_created_time,c_comments 
FROM ecr.tbl_ecr_address_vrfn WHERE c_ecr_id in (SELECT c_ecr_id FROM migestrelabet.tbl_migration_map_mig);

SELECT row_count() tbl_ecr_address_vrfn_log, now() complet_time;

INSERT INTO  ecr.tbl_ecr_identity_vrfn_log
(c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_tracker_id ,c_created_time)
SELECT c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_tracker_id ,c_created_time
FROM ecr.tbl_ecr_identity_vrfn WHERE c_ecr_id in (SELECT c_ecr_id FROM migestrelabet.tbl_migration_map_mig);


SELECT row_count() tbl_ecr_identity_vrfn_log, now() complet_time;


INSERT INTO ecr.tbl_ecr_age_vrfn_log(c_ecr_id, c_vrfn_status, c_partner_id, c_label_id,  c_created_time,c_comments)
SELECT c_ecr_id, c_vrfn_status, c_partner_id, c_label_id,  c_created_time,c_comments
FROM ecr.tbl_ecr_age_vrfn WHERE c_ecr_id in (SELECT c_ecr_id FROM migestrelabet.tbl_migration_map_mig);

SELECT row_count() tbl_ecr_age_vrfn_log, now() complet_time;




INSERT INTO  ecr.tbl_ecr_payment_instruement_vrfn_log
(c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_created_time,c_comments)
SELECT c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_created_time,c_comments
FROM ecr.tbl_ecr_payment_instruement_vrfn WHERE c_ecr_id in (SELECT c_ecr_id FROM migestrelabet.tbl_migration_map_mig);

SELECT row_count() tbl_ecr_payment_instruement_vrfn_log, now() complet_time;



INSERT INTO ecr.tbl_ecr_mobile_vrfn_log (c_ecr_id,c_vrfn_status,c_partner_id,c_label_id, c_created_time,c_comments)
SELECT c_ecr_id,c_vrfn_status,c_partner_id,c_label_id, c_created_time,c_comments
FROM ecr.tbl_ecr_mobile_vrfn WHERE c_ecr_id in (SELECT c_ecr_id FROM migestrelabet.tbl_migration_map_mig);

SELECT row_count() tbl_ecr_mobile_vrfn_log, now() complet_time;



INSERT INTO ecr.tbl_ecr_banner_logs(c_ecr_id,c_affiliate_id,c_tracker_id,c_affiliate_name, c_created_by ,c_created_time)
SELECT c_ecr_id,c_affiliate_id,c_tracker_id,c_affiliate_name, c_created_by ,c_created_time
FROM ecr.tbl_ecr_banner WHERE c_ecr_id in (SELECT c_ecr_id FROM migestrelabet.tbl_migration_map_mig);

SELECT row_count() tbl_ecr_banner_logs, now() complet_time;





