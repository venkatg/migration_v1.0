use messaging;


INSERT INTO messaging.tbl_user_info(c_ecr_id,c_email_id,c_partner_id,c_label_id,c_product_id,c_country,c_country_code,c_state,c_city,c_zip,c_title,c_fname,c_mname,c_lname,c_nationality,c_screenname,c_sex,c_age,c_dob,c_isreal,c_isplay,c_issuspicious,c_isrg_closed,c_isfraud,c_isc1,c_isc2,c_isc3,c_address1,c_address2,c_phone_isd_code,c_phone_number,c_mobile_isd_code,c_mobile_number,c_updated_time,c_registered_time,c_language,c_currency,c_loyality,c_email_subscribed,c_mobile_subscribed,c_external_id,c_system_modified_mobile_subscription,c_system_modified_email_subscription,c_telephone_subscribed,c_social_subscribed,c_directmail_subscribed,c_push_subscribed,c_updated_by,c_created_by,c_created_time,c_closed,c_is_email_updated,c_system_modified_social_subscription,c_system_modified_telephone_subscription,c_system_modified_directmail_subscription,c_email_verified_status,c_mobile_verified_status,c_system_modified_push_subscription)
SELECT mig.c_ecr_id,(CASE WHEN migecr.c_email_id = '' OR migecr.c_email_id IS NULL THEN lower(concat(ifnull(migpro.c_mobile_number,migpro.c_partner_account_id),'@estrelabet.com')) ELSE lower(migecr.c_email_id) END),mig.c_partner_id,mig.c_partner_id,'CASINO','BRAZIL',UPPER(CASE WHEN migpro.c_country_code = '' OR migpro.c_country_code IS NULL THEN  'BR' ELSE migpro.c_country_code END),migpro.c_state,migpro.c_city,IF(ROUND(migpro.c_zip),ROUND(migpro.c_zip),migpro.c_zip),IF(migpro.c_sex is null OR migpro.c_sex ="" OR migpro.c_sex ="X","",IF(upper(migpro.c_sex)="F","Mis","Mr")) title,upper(migpro.c_fname),upper(migpro.c_mname),upper(migpro.c_lname),"Brazilian",IF(sc.c_screen_name is null OR sc.c_screen_name="",LOWER(c_email_id),LOWER(REPLACE(sc.c_screen_name,' ',''))) as c_screen_name,migpro.c_sex,IF(migpro.c_dob='0000-00-00',year(curdate())-year('2000-01-01'),year(curdate())-year(migpro.c_dob)),CASE WHEN migpro.c_dob='0000-00-00' THEN '2000-01-01' ELSE migpro.c_dob END ,if(ecrcat.c_status is not null and  ecrcat.c_status = "real" , 1,0),if(ecrcat.c_status is null or  ecrcat.c_status <> "real" , 1,0),0,0,if(ecrcat.c_category is not null and   ecrcat.c_category = "fraud" , 1,0),0,0,0,migpro.c_address1,migpro.c_address2,SUBSTRING(IFNULL(migpro.c_phone_number,""),1,2),SUBSTRING(IFNULL(migpro.c_phone_number,""),3),SUBSTRING(IFNULL(migpro.c_mobile_number,""),1,2),SUBSTRING(IFNULL(migpro.c_mobile_number,""),3),now(),migecr.c_signup_time,IFNULL(migecr.c_language,"pt-br"),"BRL",0,IF(subscr.c_email_triggered="",1,subscr.c_email_triggered),IF(subscr.c_sms_triggered is null OR subscr.c_sms_triggered="",1,subscr.c_sms_triggered),mig.c_external_id,0,0,IF(subscr.c_telephone_subscribed="",1,subscr.c_telephone_subscribed),0,IF(subscr.c_directmail_subscribed="",1,subscr.c_directmail_subscribed),0,"Migration","Migration",now(),0,1,0,0,0,"NOT_INITIATED","NOT_INITIATED",0 
FROM migestrelabet.tbl_migration_map_mig mig 
JOIN migestrelabet.tbl_ecr_mig migecr 
	ON mig.c_partner_account_id = migecr.c_partner_account_id 
LEFT JOIN migestrelabet.tbl_ecr_profile_list_mig migpro 
	ON mig.c_partner_account_id = migpro.c_partner_account_id
LEFT JOIN migestrelabet.tbl_ecr_screen_name_mig sc 
	ON mig.c_partner_account_id = sc.c_partner_account_id 
LEFT JOIN migestrelabet.tbl_ecr_category_mig ecrcat 
	ON mig.c_partner_account_id = ecrcat.c_partner_account_id 
LEFT JOIN migestrelabet.tbl_ecr_subscriptions_mig subscr 
	ON mig.c_partner_account_id = subscr.c_partner_account_id;
	
select row_count() tbl_user_info , now() complet_time;
	

INSERT IGNORE INTO messaging.tbl_user_subscription_history(c_ecr_id,c_email_subscribed,c_mobile_subscribed,c_telephone_subscribed,c_directmail_subscribed) SELECT mig.c_ecr_id,veri.c_email_triggered,veri.c_sms_triggered,c_telephone_subscribed,c_directmail_subscribed FROM migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_subscriptions_mig veri on mig.c_partner_account_id = veri.c_partner_account_id;

select row_count() tbl_user_subscription_history, now() complet_time;
#risk

insert ignore into risk.tbl_ecr_game_block_for_products(c_ecr_id,c_partner_id,c_label_id,c_product_id,c_is_blocked,c_created_time,c_created_by,c_updated_time,c_reason)select c_ecr_id,c_partner_id,c_partner_id,"casino",if(flag.c_allow_bet_casino,0,1),mig.c_created_time,"Migration",mig.c_created_time,"Migration Data" from migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_flags_mig flag on mig.c_partner_account_id = flag.c_partner_account_id ;

select row_count() tbl_ecr_game_block_for_products, now() complet_time;


insert ignore into risk.tbl_ecr_game_block_for_products(c_ecr_id,c_partner_id,c_label_id,c_product_id,c_is_blocked,c_created_time,c_created_by,c_updated_time,c_reason)select c_ecr_id,c_partner_id,c_partner_id,"sports_book",if(flag.c_allow_bet_sports,0,1),mig.c_created_time,"Migration",mig.c_created_time,"Migration Data" from migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_flags_mig flag on mig.c_partner_account_id = flag.c_partner_account_id ;

select row_count() tbl_ecr_game_block_for_products, now() complet_time;

insert ignore into risk.tbl_ecr_game_block_for_products_log(c_master_id,c_ecr_id,c_partner_id,c_label_id,c_product_id,c_is_blocked,c_created_time,c_created_by,c_reason)select CONCAT("MID",mig.c_ecr_id),c_ecr_id,c_partner_id,c_partner_id,"casino",if(flag.c_allow_bet_casino,0,1),mig.c_created_time,"Migration","Migration Data" from migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_flags_mig flag on mig.c_partner_account_id = flag.c_partner_account_id ;

select row_count() tbl_ecr_game_block_for_products_log, now() complet_time;

insert ignore into risk.tbl_ecr_game_block_for_products_log(c_master_id,c_ecr_id,c_partner_id,c_label_id,c_product_id,c_is_blocked,c_created_time,c_created_by,c_reason)select CONCAT("MID",mig.c_ecr_id),c_ecr_id,c_partner_id,c_partner_id,"sports_book",if(flag.c_allow_bet_sports,0,1),mig.c_created_time,"Migration","Migration Data" from migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_flags_mig flag on mig.c_partner_account_id = flag.c_partner_account_id ;

select row_count() tbl_ecr_game_block_for_products_log, now() complet_time;

