select now() starttime;

insert into bireports.tbl_ecr(c_ecr_id,c_external_id,c_partner_id,c_label_id,c_product_id,c_ecr_crncy,c_screen_name,c_ecr_status,
c_language,c_affiliate_id,c_sign_up_country,c_sign_up_ip,c_sign_up_channel,c_sign_up_sub_channel,c_sign_up_os_browser_type,c_sign_up_time,c_category,
c_deposit_allowed,c_withdrawl_allowed,c_test_user,c_liquidity_pool,c_updated_time,
c_ecr_conversion_date,c_rg_cool_off,c_rg_self_exclusion,c_rg_closed,c_rg_cool_off_updated_time,c_rg_closed_updated_time,
c_category_change_source,c_last_login_time)
select mig.c_ecr_id ,mig.c_external_id ,mig.c_partner_id, mig.c_partner_id, 'CASINO', 'BRL',  
IF(ecrscreen.c_screen_name is null OR ecrscreen.c_screen_name="",LOWER(c_email_id),ecrscreen.c_screen_name),
 mig.c_player_status , "pt-BR", migecr.c_affiliate_id, "BR",ecrmig.c_register_ip, 'web' ,'html' ,'chrome', migecr.c_signup_time, IFNULL(mig.c_category,"play_user") END,
ecrflag.c_deposit_allowed, ecrflag.c_withdrawl_allowed,ecrflag.c_test_user, mig.c_partner_id, now(), now(), 'notset', 'notset', 0, now(), now(),'SYSTEM',IF(ecrmig.c_last_login_date="0000-00-00 00:00:00",migecr.c_signup_time,ecrmig.c_last_login_date)
FROM migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_mig migecr on mig.c_partner_account_id=migecr.c_partner_account_id
JOIN migestrelabet.tbl_ecr_profile_list_mig ecrmig on mig.c_partner_account_id = ecrmig.c_partner_account_id  
JOIN migestrelabet.tbl_ecr_screen_name_mig ecrscreen on mig.c_partner_account_id = ecrscreen.c_partner_account_id
LEFT JOIN migestrelabet.tbl_ecr_flags_mig ecrflag on mig.c_partner_account_id = ecrflag.c_partner_account_id;


select row_count() tbl_ecr_bi, now() complet_time;

 insert ignore into bireports.tbl_ecr_info(c_ecr_id,c_title,c_first_name,c_last_name,c_sex,c_created_time,c_dob,c_city,c_state,c_country_code,c_address_vrfn_status,c_age_vrfn_status,c_identity_vrfn_status,c_mobile_vrfn_status,c_payment_instruement_vrfn_status,c_updated_time,c_email_id,c_mobile_number,c_channel_email_vrfn_status,c_channel_mobile_vrfn_status,c_user_type,c_kyc_level,c_source_of_income_vrfn_status)
select mig.c_ecr_id,IF(migpro.c_sex is null OR migpro.c_sex="","",IF(upper(migpro.c_sex)="M","Mr","Mis")) title,upper(migpro.c_fname),upper(migpro.c_lname),IF(migpro.c_sex is null OR migpro.c_sex="" OR migpro.c_sex="X" OR migpro.c_sex="O","O",migpro.c_sex),now(),CASE WHEN migpro.c_dob='0000-00-00' THEN '2000-01-01' ELSE migpro.c_dob END,migpro.c_city,migpro.c_state,UPPER(CASE WHEN migpro.c_country_code = '' OR migpro.c_country_code IS NULL THEN  'BR' ELSE migpro.c_country_code END),"vrfn_new","vrfn_new" ,"vrfn_new" ,"vrfn_new" ,"vrfn_new",now(),(CASE WHEN migecr.c_email_id = '' OR migecr.c_email_id IS NULL THEN lower(concat(ifnull(migpro.c_mobile_number,migpro.c_partner_account_id),'@estrelabet.com')) ELSE lower(migecr.c_email_id) END),migpro.c_mobile_number,0,0,"NON_PAY_AND_PLAY_USER","KYC_0","vrfn_new"  from migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_mig migecr 
	ON mig.c_partner_account_id = migecr.c_partner_account_id
JOIN migestrelabet.tbl_ecr_profile_list_mig migpro on mig.c_partner_account_id = migpro.c_partner_account_id;

select row_count() tbl_ecr_info_bi, now() complet_time;

insert into bireports.tbl_ecr_add_on_info(c_ecr_id,c_mobile_subscribed,c_last_login_country,c_updated_time,c_email_subscribed,c_telephone_subscribed,c_directmail_subscribed)
select mig.c_ecr_id,subscr.c_sms_triggered,"BR",now(),subscr.c_email_triggered,subscr.c_telephone_subscribed,subscr.c_directmail_subscribed
FROM migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_subscriptions_mig subscr  ON  mig.c_partner_account_id=subscr.c_partner_account_id;

select row_count() tbl_ecr_add_on_info, now() complet_time;


#SEGMENT: 


INSERT ignore INTO segment.tbl_segment_ecr_particular_details(c_ecr_id,c_external_id,c_email_id,c_partner_id,c_label_id,c_secondary_partners,c_product_id,
c_ecr_currency,c_screen_name,c_ecr_status,c_affiliate_id,
c_signup_time,c_signup_date,
c_category,c_category_updated_time,
c_last_login_time,c_last_login_date,c_updated_time,
c_address_vrfn_status,c_age_vrfn_status,c_identity_vrfn_status,c_mobile_vrfn_status,c_payment_instruement_vrfn_status,c_email_subscribed,c_mobile_subscribed,c_mobile_isd_code,c_mobile_number,c_dob,c_test_user,c_birthday,c_time_zone,c_sex,c_country_code,c_firstname,c_lastname,
c_phone_isd_code,c_phone_number, c_nationality, c_email_domain,c_reg_type, c_signup_ip_country_code, c_jurisdiction, c_state_code, c_signup_channel,c_login_channel,c_push_subscribed)
select mig.c_ecr_id ,
mig.c_external_id ,
ecrmig.c_email_id,
mig.c_partner_id,
 mig.c_partner_id,
 mig.c_partner_id,
 "CASINO",
"BRL",
IF(ecrscreen.c_screen_name is null OR ecrscreen.c_screen_name="",LOWER(ecrmig.c_email_id),ecrscreen.c_screen_name),
mig.c_player_status,
ecrmig.c_affiliate_id,
ecrmig.c_signup_time,
DATE(ecrmig.c_signup_time),
IFNULL(mig.c_category,"play_user"),
ecrmig.c_signup_time, 
IF(ecrProfile.c_last_login_date="0000-00-00 00:00:00",ecrmig.c_signup_time,ecrProfile.c_last_login_date),
DATE(IF(ecrProfile.c_last_login_date="0000-00-00 00:00:00",ecrmig.c_signup_time,ecrProfile.c_last_login_date)),
now(),
"vrfn_new",
"vrfn_new",
"vrfn_new",
"vrfn_new",
"vrfn_new",
IF(mig.c_email_triggered IS NULL OR mig.c_email_triggered="",1,mig.c_email_triggered),
IF(mig.c_sms_triggered IS NULL OR mig.c_sms_triggered="",1,mig.c_sms_triggered),
SUBSTRING(IFNULL(ecrProfile.c_mobile_number,""),1,2),
SUBSTRING(IFNULL(ecrProfile.c_mobile_number,""),3),
CASE WHEN ecrProfile.c_dob='0000-00-00' THEN '2000-01-01' ELSE ecrProfile.c_dob END,
IFNULL(mig.c_test_user,0),
DATE_FORMAT((CASE WHEN ecrProfile.c_dob='0000-00-00' THEN '2000-01-01' ELSE ecrProfile.c_dob END),'%m-%d'),
IFNULL(ecrProfile.c_timezone,"America/Sao_Paulo"),
IF(ecrProfile.c_sex is not null,upper(ecrProfile.c_sex),""),
UPPER(CASE WHEN ecrProfile.c_country_code = '' OR ecrProfile.c_country_code IS NULL THEN  'BR' ELSE ecrProfile.c_country_code END),
upper(ecrProfile.c_fname),
upper(ecrProfile.c_lname),
SUBSTRING(IFNULL(ecrProfile.c_phone_number,""),1,2),
SUBSTRING(IFNULL(ecrProfile.c_phone_number,""),3),
"Brazilian",
SUBSTRING(ecrmig.c_email_id,LOCATE('@',ecrmig.c_email_id)),
'EMAIL',
"BR",
"curacao",
ecrProfile.c_state,
'web',
"web",
1 
 from tbl_migration_map_mig mig JOIN tbl_ecr_mig ecrmig  on mig.c_partner_account_id = ecrmig.c_partner_account_id  
 JOIN migestrelabet.tbl_ecr_profile_list_mig ecrProfile on mig.c_partner_account_id = ecrProfile.c_partner_account_id
 LEFT JOIN migestrelabet.tbl_ecr_screen_name_mig ecrscreen on mig.c_partner_account_id = ecrscreen.c_partner_account_id;
 
select row_count() tbl_segment_ecr_particular_details, now() complet_time;


INSERT IGNORE INTO segment.tbl_segment_ecr_pocket_details (c_ecr_id,c_partner_id,c_tot_bonus_offered_count,c_tot_bonus_issued_count,c_tot_bonus_expired_count,c_tot_bonus_dropped_count,c_updated_time) 
select 
mig.c_ecr_id,mig.c_partner_id,0,0,0,0,now()
from migestrelabet.tbl_migration_map_mig mig;
	
select row_count() tbl_segment_ecr_pocket_details, now() complet_time;

INSERT IGNORE INTO segment.tbl_segment_ecr_casino_details
(c_ecr_id, c_partner_id, c_is_casino_blocked, c_updated_time)
select mig.c_ecr_id,mig.c_partner_id,if(flag.c_allow_bet_casino is null OR flag.c_allow_bet_casino=1,0,1), now()	
from migestrelabet.tbl_migration_map_mig mig JOIN	migestrelabet.tbl_ecr_flags_mig flag on mig.c_partner_account_id = flag.c_partner_account_id;

select row_count() tbl_segment_ecr_casino_details, now() complet_time;

INSERT IGNORE INTO segment.tbl_segment_ecr_sb_details
(c_ecr_id, c_partner_id, c_is_sb_blocked, c_updated_time)
select mig.c_ecr_id,mig.c_partner_id,if(flag.c_allow_bet_sports is null OR flag.c_allow_bet_sports=1,0,1), now()	
from migestrelabet.tbl_migration_map_mig mig JOIN	migestrelabet.tbl_ecr_flags_mig flag on mig.c_partner_account_id = flag.c_partner_account_id;	

select row_count() tbl_segment_ecr_sb_details, now() complet_time;

INSERT IGNORE INTO segment.tbl_segment_ecr_payment_details(c_ecr_id, c_partner_id, c_is_deposit_blocked, c_is_withdrawl_blocked)
select mig.c_ecr_id,mig.c_partner_id,IF(c_deposit_allowed is null or c_deposit_allowed=1,0,1),IF(c_withdrawl_allowed is null or c_withdrawl_allowed=1,0,1) from migestrelabet.tbl_migration_map_mig mig JOIN	migestrelabet.tbl_ecr_flags_mig flag on mig.c_partner_account_id = flag.c_partner_account_id;

select row_count() tbl_segment_ecr_payment_details, now() complet_time;