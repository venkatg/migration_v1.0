use ecr;
SELECT now() starttime;

INSERT INTO ecr.tbl_ecr_mig_partner_account_mapping(c_partner_account_id, c_partner_external_id, c_ecr_id,c_external_id,c_partner_id,c_created_by,c_created_time)
SELECT c_partner_account_id,c_partner_account_id,c_ecr_id,c_external_id,c_partner_id,'SYSTEM',now() FROM  migestrelabet.tbl_migration_map_mig;

SELECT row_count() tbl_ecr_mig_partner_account_mapping, now() complet_time;

INSERT  INTO ecr.tbl_ecr (c_ecr_id,c_master_id,c_external_id ,c_email_id,c_partner_id,c_registration_status ,c_ecr_status,c_passwd_hash ,c_signup_time ,c_updated_by,c_updated_time,c_language,c_affiliate_id,c_jurisdiction) 
select mig.c_ecr_id,CONCAT("MID",mig.c_ecr_id),  mig.c_external_id,(CASE WHEN migecr.c_email_id = '' OR migecr.c_email_id IS NULL THEN lower(concat(ifnull(profile.c_mobile_number,profile.c_partner_account_id),'@estrelabet.com')) ELSE lower(migecr.c_email_id) END)
,mig.c_partner_id,'ecr_regn_completed' ,IFNULL(catgory.c_status,'play'),ifnull(migecr.c_passwd_hash,'123123') ,IFNULL(migecr.c_signup_time,now()) ,'SYSTEM',now(),IFNULL(migecr.c_language,"pt-BR"),IF(migecr.c_affiliate_id is null OR migecr.c_affiliate_id="",null,migecr.c_affiliate_id),'curacao'
FROM migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_mig migecr on mig.c_partner_account_id=migecr.c_partner_account_id  LEFT JOIN migestrelabet.tbl_ecr_profile_list_mig profile on migecr.c_partner_account_id = profile.c_partner_account_id LEFT  JOIN migestrelabet.tbl_ecr_category_mig catgory on catgory.c_partner_account_id = mig.c_partner_account_id;

SELECT row_count() tbl_ecr, now() complet_time;


select count(mig.c_partner_account_id) from  migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_mig migecr on mig.c_partner_account_id=migecr.c_partner_account_id

INSERT INTO ecr.tbl_ecr_profile_list (c_ecr_id,c_title ,c_fname,c_mname,c_lname,c_sex,c_created_time ,c_dob,c_address1 ,c_address2 ,c_address_first3,c_city,c_state,c_country_code,c_zip,c_phone_isd_code,c_phone_number,c_mobile_isd_code,c_mobile_number,c_agent_name,c_partner_id,c_label_id,c_timezone,c_nationality,c_national_id)
SELECT mig.c_ecr_id,IF(migecr.c_sex is null or migecr.c_sex ="","",IF(upper(migecr.c_sex)="F","Mis","Mr")) title ,upper(migecr.c_fname),upper(migecr.c_mname),upper(migecr.c_lname),IF(migecr.c_sex is not null,upper(migecr.c_sex),"") ,IFNULL(migecr.c_created_time,now()) ,CASE WHEN migecr.c_dob='0000-00-00' THEN '2000-01-01' ELSE migecr.c_dob END,migecr.c_address1 ,migecr.c_address2 ,IF(migecr.c_address1 is not null,SUBSTRING(migecr.c_address1,1,3),SUBSTRING(IFNULL(migecr.c_address2,""),1,3)),migecr.c_city,migecr.c_state,UPPER(CASE WHEN migecr.c_country_code = '' OR migecr.c_country_code IS NULL THEN  'BR' ELSE migecr.c_country_code END),IF(ROUND(migecr.c_zip),ROUND(migecr.c_zip),migecr.c_zip),
SUBSTRING(IFNULL(migecr.c_phone_number,""),1,2),SUBSTRING(IFNULL(migecr.c_phone_number,""),3),SUBSTRING(IFNULL(migecr.c_mobile_number,""),1,2),SUBSTRING(IFNULL(migecr.c_mobile_number,""),3),'system',mig.c_partner_id,mig.c_partner_id,IFNULL(migecr.c_timezone,"America/Sao_Paulo"),"Brazilian",c_national_id
FROM migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_profile_list_mig migecr on mig.c_partner_account_id=migecr.c_partner_account_id;


SELECT row_count() tbl_ecr_profile_list, now() complet_time;

INSERT INTO ecr.tbl_ecr_profile (c_ecr_id,c_title ,c_fname,c_mname,c_lname,c_sex ,c_dob,c_address1 ,c_address2 ,c_address_first3,c_city,c_state,c_country_code,c_zip,c_phone_isd_code,c_phone_number,c_mobile_isd_code,c_mobile_number,c_agent_name,c_partner_id,c_label_id,c_profile_list_id,c_timezone,c_nationality,c_national_id,c_updated_time,c_signup_ip_country) 
select plist.c_ecr_id,c_title ,c_fname,c_mname,c_lname,c_sex ,c_dob,c_address1 ,c_address2 ,c_address_first3,c_city,c_state,c_country_code,c_zip,c_phone_isd_code,c_phone_number,c_mobile_isd_code,c_mobile_number,c_agent_name,plist.c_partner_id,plist.c_label_id,plist.c_id,c_timezone,c_nationality,c_national_id,now(),"BR" from ecr.tbl_ecr_profile_list plist JOIN migestrelabet.tbl_migration_map_mig mig
ON plist.c_ecr_id=mig.c_ecr_id;


SELECT row_count() tbl_ecr_profile, now() complet_time;



INSERT INTO ecr.tbl_ecr_screen_name ( c_ecr_id, c_screen_name,c_liquidity_pool,c_comments,c_updated_by,c_updated_time,c_screen_name_upper)
select migmap.c_ecr_id, IF(sc.c_screen_name is null OR sc.c_screen_name="",LOWER(c_email_id),LOWER(sc.c_screen_name)) as screen_name,migmap.c_partner_id,'estrelabet migration','SYSTEM',CURRENT_TIMESTAMP,IF(sc.c_screen_name is null OR sc.c_screen_name="",UPPER(c_email_id),UPPER(REPLACE(sc.c_screen_name,' ','')))
from migestrelabet.tbl_migration_map_mig migmap LEFT JOIN migestrelabet.tbl_ecr_screen_name_mig sc on sc.c_partner_account_id = migmap.c_partner_account_id join migestrelabet.tbl_ecr_mig ecr on migmap.c_partner_account_id = ecr.c_partner_account_id ;


SELECT row_count() tbl_ecr_screen_name, now() complet_time; 


INSERT INTO ecr.tbl_ecr_category (c_ecr_id,c_category,c_old_category,c_change_source,c_creation_date,c_created_by,c_last_modified_date,c_comments,c_reason) 
SELECT mig.c_ecr_id,IFNULL(migcat.c_category,"play_user"),NULL,'migration',mig.c_created_time,'Migration',now(),null,"migration"
FROM migestrelabet.tbl_migration_map_mig mig LEFT JOIN migestrelabet.tbl_ecr_category_mig migcat on  mig.c_partner_account_id = migcat.c_partner_account_id; 

SELECT row_count() tbl_ecr_category, now() complet_time;


INSERT ignore INTO ecr.tbl_ecr_labels (c_ecr_id,c_partner_id,c_label_id,c_type,c_created_by,c_created_time, c_updated_time,c_updated_by,c_comments) 
SELECT plist.c_ecr_id,plist.c_partner_id,plist.c_partner_id,'conversion','SYSTEM',c_created_time, now(), 'SYSTEM','Migration'
FROM migestrelabet.tbl_migration_map_mig plist JOIN migestrelabet.tbl_ecr_category_mig ecrcat on ecrcat.c_partner_account_id = plist.c_partner_account_id and LOWER(ecrcat.c_status) = "real";

SELECT row_count() tbl_ecr_labels, now() complet_time;

INSERT ignore INTO ecr.tbl_ecr_labels (c_ecr_id,c_partner_id,c_label_id,c_type,c_created_by,c_created_time, c_updated_time,c_updated_by,c_comments) 
SELECT plist.c_ecr_id,plist.c_partner_id,plist.c_partner_id,'signup','SYSTEM',c_created_time, now(), 'SYSTEM','Migration'
FROM migestrelabet.tbl_migration_map_mig plist LEFT JOIN migestrelabet.tbl_ecr_category_mig ecrcat on ecrcat.c_partner_account_id = plist.c_partner_account_id;


SELECT row_count() tbl_ecr_labels, now() complet_time;




INSERT INTO ecr.tbl_ecr_signup_info ( c_ecr_id, c_partner_id, c_label_id, c_product_id, c_channel, c_sub_channel, c_os_browser_type, c_created_by , c_created_time)
SELECT mig.c_ecr_id,mig.c_partner_id,mig.c_partner_id, "CASINO",'web','html','chrome','SYSTEM' , ecr.c_signup_time
FROM migestrelabet.tbl_migration_map_mig mig JOIN tbl_ecr_mig ecr on ecr.c_partner_account_id =  mig.c_partner_account_id;

SELECT row_count() tbl_ecr_signup_info, now() complet_time;

INSERT INTO ecr.tbl_ecr_address_vrfn (c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_created_time,c_comments)
SELECT mig.c_ecr_id,'vrfn_new',mig.c_partner_id,mig.c_partner_id, now(),'Migration'
FROM migestrelabet.tbl_migration_map_mig mig ;

SELECT row_count() tbl_ecr_address_vrfn, now() complet_time;


INSERT INTO ecr.tbl_ecr_identity_vrfn (c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_created_time, c_comments)
SELECT mig.c_ecr_id,'vrfn_new',mig.c_partner_id,mig.c_partner_id, now(),'Migration'
FROM migestrelabet.tbl_migration_map_mig mig ;

SELECT row_count() tbl_ecr_identity_vrfn, now() complet_time;


INSERT INTO ecr.tbl_ecr_age_vrfn(c_ecr_id, c_vrfn_status, c_partner_id, c_label_id, c_created_time,c_comments)
SELECT mig.c_ecr_id,'vrfn_new',mig.c_partner_id,mig.c_partner_id, now(),'Migration'
FROM migestrelabet.tbl_migration_map_mig mig ;

SELECT row_count() tbl_ecr_age_vrfn, now() complet_time;



INSERT INTO ecr.tbl_ecr_payment_instruement_vrfn (c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_created_time, c_comments)
SELECT mig.c_ecr_id,'vrfn_new',mig.c_partner_id,mig.c_partner_id, now(),'Migration'
FROM migestrelabet.tbl_migration_map_mig mig ;


SELECT row_count() tbl_ecr_payment_instruement_vrfn, now() complet_time;



INSERT INTO ecr.tbl_ecr_mobile_details  (c_ecr_id,c_partner_id,c_isd_code,c_mobile_num,c_updated_by,c_updated_time)
SELECT plist.c_ecr_id,plist.c_partner_id,plist.c_phone_isd_code,plist.c_phone_number,'SYSTEM',plist.c_created_time
FROM ecr.tbl_ecr_profile plist WHERE c_ecr_id in (SELECT c_ecr_id FROM migestrelabet.tbl_migration_map_mig);

SELECT row_count() tbl_ecr_mobile_details, now() complet_time;

INSERT INTO ecr.tbl_ecr_mobile_vrfn (c_ecr_id,c_vrfn_status,c_partner_id,c_label_id,c_created_time,c_comments)
SELECT mig.c_ecr_id,'vrfn_new',mig.c_partner_id,mig.c_partner_id, now(), 'Migration'
FROM migestrelabet.tbl_migration_map_mig mig;

SELECT row_count() tbl_ecr_mobile_vrfn, now() complet_time;


INSERT INTO ecr.tbl_ecr_banner(c_ecr_id,c_affiliate_id,c_created_by ,c_created_time,c_updated_time)
SELECT mig.c_ecr_id, IF(ecrmig.c_affiliate_id is null OR ecrmig.c_affiliate_id ="",null, ecrmig.c_affiliate_id), 'SYSTEM', now(),now()
FROM migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_mig ecrmig on mig.c_partner_account_id = ecrmig.c_partner_account_id;

SELECT row_count() tbl_ecr_banner, now() complet_time;


INSERT INTO ecr.tbl_ecr_flags(c_ecr_id,c_withdrawl_allowed,c_test_user, c_created_by ,c_created_time, c_updated_by, c_updated_time, c_comments)
select mig.c_ecr_id,IFNULL(c_withdrawl_allowed,1),IFNULL(c_test_user,0),"Migration",now(),"Migration",now(),"Migration"
FROM migestrelabet.tbl_migration_map_mig mig LEFT JOIN migestrelabet.tbl_ecr_flags_mig ecrflag  on mig.c_partner_account_id = ecrflag.c_partner_account_id ;

SELECT row_count() tbl_ecr_flags, now() complet_time;




insert into ecr.tbl_ecr_kyc_level(c_ecr_id,c_level,c_desc,c_created_time,c_created_by,c_updated_time,c_updated_by)
select mig.c_ecr_id,"KYC_0","intial insertion for Migration",now(),"Migration",now(),"Migration" from migestrelabet.tbl_migration_map_mig mig;

SELECT row_count() tbl_ecr_kyc_level, now() complet_time;


INSERT INTO ecr.tbl_ecr_conversion_info (c_ecr_id,c_partner_id,c_label_id,c_product_id,c_channel,c_sub_channel,c_os_browser_type,c_conversion_kind,c_conversion_time,c_created_by,c_created_time) 
SELECT mig.c_ecr_id,  mig.c_partner_id,mig.c_partner_id,'CASINO','web','html','chrome','DEPOSIT', now(),'Migration' , now() FROM migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_category_mig ecrcat  on ecrcat.c_partner_account_id = mig.c_partner_account_id and LOWER(ecrcat.c_status) = "real" ;

SELECT row_count() tbl_ecr_conversion_info, now() complet_time;


INSERT INTO ecr.tbl_ecr_auth (c_ecr_id,c_partner_id,c_label_id,c_product_id,c_channel,c_sub_channel, c_os_browser_type,c_auth_type, c_auth_status,c_updated_by,c_updated_time,c_ip,c_login_time) 
SELECT mig.c_ecr_id, mig.c_partner_id,mig.c_partner_id,'CASINO',IFNULL(auth.c_channel,'web'),'html','default',"REGISTRATION",'SUCCESS','SYSTEM',migecr.c_signup_time, pmig.c_register_ip,migecr.c_signup_time
FROM migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_mig migecr ON  mig.c_partner_account_id=migecr.c_partner_account_id JOIN migestrelabet.tbl_ecr_profile_list_mig pmig ON pmig.c_partner_account_id = mig.c_partner_account_id LEFT JOIN tbl_ecr_auth_mig auth on auth.c_partner_account_id = mig.c_partner_account_id;

SELECT row_count() tbl_ecr_auth1, now() complet_time;

-----------------
INSERT INTO ecr.tbl_ecr_auth (c_ecr_id,c_partner_id,c_label_id,c_product_id,c_channel,c_sub_channel, c_os_browser_type,c_auth_type, c_auth_status,c_updated_by,c_updated_time,c_ip,c_login_time) 
SELECT mig.c_ecr_id, mig.c_partner_id,mig.c_partner_id,'CASINO',IFNULL(auth.c_channel,'web'),'html','default',"email",'SUCCESS','SYSTEM',migecr.c_signup_time, pmig.c_register_ip,migecr.c_signup_time
FROM migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_mig migecr ON  mig.c_partner_account_id=migecr.c_partner_account_id JOIN migestrelabet.tbl_ecr_profile_list_mig pmig ON pmig.c_partner_account_id = mig.c_partner_account_id LEFT JOIN tbl_ecr_auth_mig auth on auth.c_partner_account_id = mig.c_partner_account_id;


SELECT row_count() tbl_ecr_auth2, now() complet_time;

INSERT INTO ecr.tbl_ecr_auth (c_ecr_id,c_partner_id,c_label_id,c_product_id,c_channel,c_sub_channel, c_os_browser_type,c_auth_type, c_auth_status,c_updated_by,c_updated_time,c_ip,c_login_time) 
SELECT mig.c_ecr_id, mig.c_partner_id,mig.c_partner_id,'CASINO',IFNULL(auth.c_channel,'web'),'html','default',"email",'SUCCESS','SYSTEM',migecr.c_last_login_date, pmig.c_register_ip,migecr.c_last_login_date
FROM migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_profile_list_mig migecr ON  mig.c_partner_account_id=migecr.c_partner_account_id JOIN migestrelabet.tbl_ecr_profile_list_mig pmig ON pmig.c_partner_account_id = mig.c_partner_account_id LEFT JOIN tbl_ecr_auth_mig auth on auth.c_partner_account_id = mig.c_partner_account_id where migecr.c_last_login_date <> "0000-00-00 00:00:00";

select now() Endtime;


insert ignore into ecr.tbl_ecr_cpf_validation_data(c_cpf,c_ecr_id,c_full_name,c_dob,c_cpf_status,c_status,c_is_deceased,c_is_pep,c_is_sanction,c_is_underaged,c_active,c_comment,c_created_time,c_updated_time,c_updated_by,c_transaction_result_type,c_masked_full_name,c_masked_dob,c_transaction_id)
select plist.c_national_id, mig.c_ecr_id,CONCAT(IFNULL(c_fname,"")," ",IFNULL(c_mname,"")," ",ifnull(c_lname,"")),plist.c_dob,"REGULAR",'registered',0,0,0,0,1,"Migration",now(),now(),"Migration","Success",CONCAT(IFNULL(c_fname,""),"****"),CONCAT("**/**/",YEAR(c_dob)),CONCAT("MIG",mig.c_partner_account_id)  from migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_profile_list_mig plist  on mig.c_partner_account_id  = plist.c_partner_account_id ;

insert ignore into ecr.tbl_ecr_cpf_validation_data_log(c_cpf,c_ecr_id,c_full_name,c_dob,c_cpf_status,c_status,c_is_deceased,c_is_pep,c_is_sanction,c_is_underaged,c_active,c_comment,c_updated_time,c_updated_by,c_transaction_result_type,c_masked_full_name,c_masked_dob,c_transaction_id)
select plist.c_national_id, mig.c_ecr_id,CONCAT(IFNULL(c_fname,"")," ",IFNULL(c_mname,"")," ",ifnull(c_lname,"")),plist.c_dob,"REGULAR",'registered',0,0,0,0,1,"Migration",now(),"Migration","Success",CONCAT(IFNULL(c_fname,""),"****"),CONCAT("**/**/",YEAR(c_dob)),CONCAT("MIG",mig.c_partner_account_id)  from migestrelabet.tbl_migration_map_mig mig JOIN migestrelabet.tbl_ecr_profile_list_mig plist  on mig.c_partner_account_id  = plist.c_partner_account_id ;