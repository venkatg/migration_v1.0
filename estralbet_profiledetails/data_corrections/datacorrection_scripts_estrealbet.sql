MESSAGING : 
update migestrelabet.tbl_ecr_category_mig set c_category = "real_user" where  c_category = "c5";
update migestrelabet.tbl_ecr_category_mig set c_category = "play_user" where  c_category = "c4";
update migestrelabet.tbl_ecr_category_mig set c_category = "suspended" where  c_category = "test_c1";
update tbl_ecr_profile_list_mig set c_national_id =REPLACE(REPLACE(c_national_id,"-",""),".","") where c_national_id is not null;

#FUND: 

update tbl_real_fund_mig set c_realcash_balance = REPLACE(c_realcash_balance,",",""),c_bonus_balance = REPLACE(c_bonus_balance,",","");
update migestrelabet.tbl_ecr_category_mig set c_category = "real_user" where  c_category = "c5";
update migestrelabet.tbl_ecr_category_mig set c_category = "play_user" where  c_category = "c4";
update migestrelabet.tbl_ecr_category_mig set c_category = "suspended" where  c_category = "test_c1";


#ECR : 

update migestrelabet.tbl_ecr_category_mig set c_category = "real_user" where  c_category = "c5";
update migestrelabet.tbl_ecr_category_mig set c_category = "play_user" where  c_category = "c4";
update migestrelabet.tbl_ecr_category_mig set c_category = "suspended" where  c_category = "test_c1";

#update migestrelabet.tbl_ecr_category_mig mig,ecr.tbl_category_config_mapping mapping set mig.c_category = mapping.c_category where  mig.c_category = mapping.c_category_name;
update tbl_ecr_profile_list_mig set c_national_id =REPLACE(REPLACE(c_national_id,"-",""),".","") where c_national_id is not null;


#update tbl_ecr_category_mig set c_category="play_user" where c_status = "play" and c_category = "";
#update tbl_ecr_category_mig set c_category="play_user",c_status = "play" where c_status = "" and c_category = "";
#update tbl_ecr_category_mig set c_closed_reason ="";
#update tbl_ecr_mig set c_email_id = REPLACE(c_email_id,"email.com","mailinator.com");

update tbl_ecr_screen_name_mig set c_screen_name = REPLACE(c_screen_name," ","");
update tbl_ecr_screen_name_mig set c_screen_name = LOWER(c_screen_name);

insert into temp_dup_screen select c_partner_account_id from migestrelabet.tbl_ecr_screen_name_mig group by c_screen_name having count(1)>1;
update tbl_ecr_screen_name_mig mig JOIN temp_dup_screen temp on mig.c_partner_account_id = temp.c_partner_account_id set c_screen_name = temp.c_partner_account_id;


update ecr.tbl_ecr_screen_name t1 JOIN tbl_ecr_screen_name_mig mig on t1.c_screen_name = mig.c_screen_name  
set t1.c_screen_name = CONCAT(t1.c_screen_name,"_beforemig") , t1.c_screen_name_upper = UPPER(CONCAT(t1.c_screen_name,"_beforemig"));
update ecr.tbl_ecr_screen_name t1 JOIN tbl_ecr_screen_name_mig mig on t1.c_screen_name_upper = UPPER(mig.c_screen_name)
 set t1.c_screen_name = CONCAT(t1.c_screen_name,"_beforemig") , t1.c_screen_name_upper = UPPER(CONCAT(t1.c_screen_name,"_beforemig"));
 

/*if requred run this deletes*/
#DELETE S1 FROM tbl_ecr_subscriptions_mig AS S1  
#INNER JOIN tbl_ecr_subscriptions_mig AS S2   
#WHERE S1.c_id < S2.c_id AND S1.c_partner_account_id = S2.c_partner_account_id;

update migestrelabet.tbl_ecr_mig set c_email_id = REPLACE(c_email_id," ","");
update migestrelabet.tbl_ecr_mig set c_email_id = LOWER(c_email_id);
update  ecr.tbl_ecr ecr JOIN  tbl_ecr_mig mig on mig.c_email_id = ecr.c_email_id set ecr.c_email_id = CONCAT("old_",ecr.c_email_id);

insert into temp_dup_email select c_partner_account_id,c_email_id from tbl_ecr_mig group by c_email_id having count(1)>1;

update tbl_ecr_mig mig JOIN temp_dup_email temp on mig.c_partner_account_id = temp.c_partner_account_id set c_email_id =CONCAT("rename_",c_email_id);

BIReports : 

update tbl_migration_map_mig mig LEFT JOIN tbl_ecr_category_mig cate on mig.c_partner_account_id = cate.c_partner_account_id set mig.c_category = IFNULL(cate.c_category,"play_user"), mig.c_player_status = IFNULL(cate.c_status,"play");

update tbl_migration_map_mig mig LEFT JOIN tbl_ecr_flags_mig flag on mig.c_partner_account_id = flag.c_partner_account_id set mig.c_test_user = IFNULL(flag.c_test_user,0);

update tbl_migration_map_mig mig LEFT JOIN tbl_ecr_subscriptions_mig subsc on mig.c_partner_account_id = subsc.c_partner_account_id set mig.c_sms_triggered = IFNULL(subsc.c_sms_triggered,1),mig.c_email_triggered =IFNULL(subsc.c_email_triggered,1);


use ecr;
update tbl_ecr  set c_language = "pt-BR" where c_language = "pt-br";

insert into tbl_ecr_screen_name_mig(c_partner_account_id,c_screen_name)values(2022055431524,"rafael130721");
insert into tbl_ecr_screen_name_mig(c_partner_account_id,c_screen_name)values(2022077167631,"14fernanda");