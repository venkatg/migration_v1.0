use ecr;
SELECT now() starttime;

truncate tbl_ecr_address_vrfn_log;
truncate tbl_ecr_age_vrfn_log;
truncate tbl_ecr_identity_vrfn_log;
truncate tbl_ecr_mobile_vrfn_log;
truncate tbl_ecr_payment_instruement_vrfn_log;
truncate tbl_ecr_labels_log;
truncate tbl_kyc_vrfn_documents_details_log;
truncate tbl_ecr_log         ;
truncate tbl_ecr_category_log;
truncate tbl_ecr_banner_logs ;
truncate tbl_ecr_cpf_validation_data_log;




delete t1 from ecr.tbl_ecr_mig_partner_account_mapping t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_address_vrfn t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;

delete t1 from ecr.tbl_ecr_age_vrfn t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_identity_vrfn t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_mobile_vrfn t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_payment_instruement_vrfn t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_auth t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_conversion_info t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_labels t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;

delete t1 from ecr.tbl_kyc_vrfn_documents_details t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_profile t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_profile_list t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_screen_name t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_signup_info t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_current_auth_details t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;

delete t1 from ecr.tbl_ecr t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;

delete t1 from ecr.tbl_ecr_category t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_mobile_details t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_banner t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;

delete t1 from ecr.tbl_ecr_flags t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_policies t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from ecr.tbl_ecr_kyc_level t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from tbl_ecr_cpf_validation_data t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;

SELECT now() endtime;

use fund ;

delete t1 from fund.tbl_real_fund t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from fund.tbl_real_fund_log t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from fund.tbl_fund_conversion_info t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from fund.tbl_fund_conversion_info_log t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from fund.tbl_real_fund_txn t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from fund.tbl_bonus_sub_fund_txn t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from fund.tbl_realcash_sub_fund_txn t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;



truncate tbl_real_fund_txn_log        ;


#truncate table tbl_casino_legacy_txns;
#truncate table tbl_sports_legacy_txns;

use bonus;

delete t1 from bonus.tbl_ecr_bonus_details  t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from bonus.tbl_ecr_bonus_details_inactive  t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from bonus.tbl_bonus_summary_details  t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from bonus.tbl_ecr_pre_offer_data  t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from bonus.tbl_bonus_pocket_txn  t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from bonus.tbl_bonus_user  t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
truncate tbl_ecr_bonus_details_log;
truncate tbl_bonus_summary_details_log;
truncate tbl_ecr_pre_offer_data_log;
truncate tbl_bonus_pocket_txn_log;

#messaging ; 

delete t1 from messaging.tbl_user_info t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from messaging.tbl_user_info_log  t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from messaging.tbl_user_subscription_history t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from messaging.tbl_user_subscription_history_log  t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from risk.tbl_ecr_game_block_for_products  t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from risk.tbl_ecr_game_block_for_products_log  t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;




cashier : 

delete t1 from cashier.tbl_deposit_withdrawl_flags_log t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;
delete t1 from cashier.tbl_deposit_withdrawl_flags  t1 INNER JOIN  migestrelabet.tbl_migration_map_mig t2 ON t1.c_ecr_id = t2.c_ecr_id;


segment : 

delete t1 from segment.tbl_segment_ecr_payment_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_casino_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_sb_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_pocket_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_particular_details t1 JOIN migestrelabet.tbl_migration_map_mig t2 on t1.c_ecr_id = t2.c_ecr_id;


delete t1 from segment.tbl_ecr t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from segment.tbl_ecr_conversion_info t1 JOIN tbl_migration_map_mig t2 on t1.c_ecr_id = t2.c_ecr_id;

delete t1 from segment.tbl_ecr_daily_segment_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;

delete t1 from segment.tbl_ecr_flags t1 JOIN migestrelabet.tbl_migration_map_mig t2 on t1.c_ecr_id = t2.c_ecr_id;


delete t1 from segment.tbl_ecr_life_time_segment_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;

delete t1 from segment.tbl_ecr_profile t1 JOIN migestrelabet.tbl_migration_map_mig t2 on t1.c_ecr_id = t2.c_ecr_id;

delete t1 from segment.tbl_ecr_rg_closed t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;

delete t1 from segment.tbl_ecr_tags_info t1 JOIN migestrelabet.tbl_migration_map_mig t2 on t1.c_ecr_id = t2.c_ecr_id;


delete t1 from segment.tbl_segment_campaign_activity_ecr_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_bingo_details t1 JOIN migestrelabet.tbl_migration_map_mig t2 on t1.c_ecr_id = t2.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_bt_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_casino_details t1 JOIN migestrelabet.tbl_migration_map_mig t2 on t1.c_ecr_id = t2.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_loyalty_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_particular_details t1 JOIN migestrelabet.tbl_migration_map_mig t2 on t1.c_ecr_id = t2.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_pocket_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_policy_details t1 JOIN migestrelabet.tbl_migration_map_mig t2 on t1.c_ecr_id = t2.c_ecr_id;

delete t1 from segment.tbl_segment_ecr_loyalty_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_third_party_acq_details t1 JOIN migestrelabet.tbl_migration_map_mig t2 on t1.c_ecr_id = t2.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_tag_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from segment.tbl_segment_ecr_social_media_account_details t1 JOIN migestrelabet.tbl_migration_map_mig t2 on t1.c_ecr_id = t2.c_ecr_id;


BIreports: 

delete t1 from bireports.tbl_ecr t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_add_on_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;


bireports:

delete t1 from bireports.tbl_active_ecr_list t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_cashier_ecr_daily_payment_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_cashier_ecr_payment_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_active_liability t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_auth t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_affliate_custom_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_banner t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_bonus_daily_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_bonus_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;

delete t1 from bireports.tbl_ecr_cashout_daily_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_cashout_daily_summary_dummy t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_conversion_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	

delete t1 from bireports.tbl_ecr_daily_auth_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_daily_game_play_data t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	

delete t1 from bireports.tbl_ecr_daily_pocket_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_daily_settled_game_play_sub_info_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_daily_settled_game_play_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	

delete t1 from bireports.tbl_ecr_daily_unsettled_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_deposit_daily_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	

delete t1 from bireports.tbl_ecr_deposit_daily_summary_affliate_dtls t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_employer_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_game_limits t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	
delete t1 from bireports.tbl_ecr_game_play_data t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;



delete t1 from bireports.tbl_ecr_hourly_auth_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_hourly_game_play_statistics t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_hourly_pocket_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	

delete t1 from bireports.tbl_ecr_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_labels t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	


delete t1 from bireports.tbl_ecr_lifetime_auth_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_loyalty_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	
delete t1 from bireports.tbl_ecr_pocket t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_preferences t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	



delete t1 from bireports.tbl_ecr_sub_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_timezone_cashout_daily_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	
delete t1 from bireports.tbl_ecr_timezone_daily_pocket_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_timezone_deposit_daily_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	

delete t1 from bireports.tbl_ecr_timezone_txn_type_wise_daily_game_play_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_timezone_txn_type_wise_daily_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	
delete t1 from bireports.tbl_ecr_txn_type_wise_daily_affliate_dtls t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_txn_type_wise_daily_game_play_affliate_dtls t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	


delete t1 from bireports.tbl_ecr_txn_type_wise_daily_game_play_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_txn_type_wise_daily_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	
delete t1 from bireports.tbl_ecr_txn_type_wise_hourly_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_txn_type_wise_life_time_game_play_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	

delete t1 from bireports.tbl_ecr_wise_daily_bi_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;
delete t1 from bireports.tbl_ecr_wise_monthly_bi_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;	
delete t1 from bireports.tbl_ecr_wise_weekly_bi_summary t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;




use casino /sports  : 

delete t1 from vendor.tbl_free_rounds_bonus_mapper t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_ecr_id=t1.c_ecr_id;


delete t1 from vendor.tbl_sports_book_bet_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_partner_account_id=t1.c_customer_id;
delete t1 from vendor.tbl_sports_book_bets_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_partner_account_id=t1.c_customer_id;
delete t1 from vendor.tbl_sports_book_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_partner_account_id=t1.c_customer_id;

delete t1 from vendor.tbl_sports_book_bet_details t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_customer_id=t1.c_customer_id;
delete t1 from vendor.tbl_sports_book_bets_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_customer_id=t1.c_customer_id;
delete t1 from vendor.tbl_sports_book_info t1 JOIN migestrelabet.tbl_migration_map_mig migmap  on migmap.c_customer_id=t1.c_customer_id;